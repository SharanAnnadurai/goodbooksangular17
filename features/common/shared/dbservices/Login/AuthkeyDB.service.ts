import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GBBaseDBService } from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { GBHttpService } from '../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

import { URLS } from './../../commonURL';

@Injectable({
  providedIn: 'root',
})
export class AuthkeyDBservice extends GBBaseDBService {
  constructor(private http: HttpClient,private GBHttpService:GBHttpService ) {
    super();
  }
  public AuthkeyDBservice(){
  
    // http://169.56.148.10:82/gb4/fws/Server.svc/AuthenticationKey
    //  baseuri http://169.56.148.10:82/gb4
    // url.authendication = '/fws/Server.svc/AuthenticationKey'
    return this.GBHttpService.httpgetwithoutheader(URLS.AUTHENTICATION);
  }

  public userAuthenticateService(authenticationDTO: any, getusername: any, headers: any) {
    /*return this.http.post(
      URLS.USERLOGIN + getusername,
      JSON.stringify(authenticationDTO),
      headers
    );*/
    return this.GBHttpService.httppostwithauthheader(
      URLS.USERLOGIN + getusername,
      JSON.stringify(authenticationDTO),
      headers
    );
  }
  
}
