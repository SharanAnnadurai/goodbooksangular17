
import { Injectable } from '@angular/core';
import { GBBaseDBService } from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { GBHttpService } from '../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

import { URLS } from './../../commonURL';

@Injectable({
  providedIn: 'root',
})
export class VersionDBservice extends GBBaseDBService {

  constructor(private gbhttp:GBHttpService) {
    super();
  }

  getversioninfo(UserName:any, Database:any) {
//**todo**  make this as url link 
//http://169.56.148.10:84/Version.svc/?UserCode=ADMIN&ConnectionName=GB4SC

    return this.gbhttp.versionhttpget("/vs/Version.svc/?UserCode=" + UserName + "&ConnectionName=" + Database);//gbapihttpget
  }

}