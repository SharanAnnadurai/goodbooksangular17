// import { TestBed, waitForAsync } from '@angular/core/testing';
// import { LoginComponent } from './login.component';
// import { RouterTestingModule } from '@angular/router/testing';

// describe('LoginComponent', () => {
//   beforeEach(waitForAsync(() => {
//     TestBed.configureTestingModule({
//       imports: [RouterTestingModule],
//       declarations: [LoginComponent],
//     }).compileComponents();
//   }));

//   it('should create the app', () => {
//     const fixture = TestBed.createComponent(LoginComponent);
//     const app = fixture.componentInstance;
//     expect(app).toBeTruthy();
//   });

//   it(`should have as title 'Goodbooks-Goodbooks'`, () => {
//     const fixture = TestBed.createComponent(LoginComponent);
//     const app = fixture.componentInstance;
//     expect(app.title).toEqual('Goodbooks-Goodbooks');
//   });

//   it('should render title', () => {
//     const fixture = TestBed.createComponent(LoginComponent);
//     fixture.detectChanges();
//     const compiled = fixture.nativeElement;
//     expect(compiled.querySelector('h1').textContent).toContain(
//       'Welcome to Goodbooks-Goodbooks!'
//     );
//   });
// });
import { LoginComponent } from './login.component';
import { FormBuilder } from '@angular/forms';
import {GBBasePageComponentNG, GBBasePageComponent} from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { GBDataPageServiceNG} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { GBBaseService } from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import { Store } from '@ngxs/store';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';


describe('NewComponentComponent', () => {
	let fixture: LoginComponent;
	let formBuilderMock: GBDataPageServiceNG;
	let store:Store;
	let dataService=new GBBaseService();
	let fb = new FormBuilder();
	let gbhttp:GBHttpService;
	let activerouten=new ActivatedRoute();
	let router: Router;
	beforeEach(() => {
		formBuilderMock = new GBDataPageServiceNG(store,dataService,fb,gbhttp,activerouten,router);
		fixture = new LoginComponent(formBuilderMock);
		// fixture.Login();
		// fixture.test();
	});

	describe('Test: ngOnInit', () => {
		it('should initialize loginForm', () => {
			const loginForm = {
				Server: 'GB4SC',
				UserName: 'ADMIN',
        		Password: 'admin'
			};
			expect(fixture.form.value).toEqual(loginForm);
		});
	});

});
