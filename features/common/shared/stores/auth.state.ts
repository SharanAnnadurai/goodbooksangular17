import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AddLoginDTO, GetLoginDTO, UpdateLogiDTO , AddVersionDTO, ThemeOption} from './auth.action';
import { ILoginDTO, IThemeDto } from './auth.model';

export class AuthstateModal {
    DLoginDTO!: ILoginDTO | null;
    Getdto!: any;
    DVersionDTO!:any;
    Dthemeoption!: IThemeDto | null;
}
@State<AuthstateModal>({
    name: 'AuthStore',
    defaults: {
        DLoginDTO: null,
        Getdto: null,
        DVersionDTO:[],
        Dthemeoption:null

    }
})
@Injectable()
export class AuthState {
    constructor() {
    }
    @Selector()
    static AddDTO(state: AuthstateModal) {
        return state.DLoginDTO;
    }

    @Selector()
    static GetDto(state: AuthstateModal) {
        return state.Getdto;
    }

    @Selector()
    static Getversion(state: AuthstateModal) {
        return state.DVersionDTO;
    }

    @Selector()
    static GetTheme(state: AuthstateModal) {
        return state.Dthemeoption;
    }

    @Action(AddLoginDTO)
    add({getState, setState}: StateContext<AuthstateModal>, {DLoginDTO}: AddLoginDTO) {
        const state = getState();
        setState({
            ...state,
            DLoginDTO: DLoginDTO
        });
    }
    @Action(UpdateLogiDTO)
    update({getState, setState}: StateContext<AuthstateModal>, {DLoginDTO}: UpdateLogiDTO) {
        const state = getState();
        setState({
            ...state,
            Getdto: DLoginDTO
        });
    }
    @Action(GetLoginDTO)
    get({getState, setState}: StateContext<AuthstateModal>, {DLoginDTO}: GetLoginDTO) {
        const state = getState();
        setState({
            ...state,
            Getdto: DLoginDTO
        });
    }
    
    @Action(AddVersionDTO)
    getversion(context: StateContext<AuthstateModal>, action: AddVersionDTO) {
        const state = context.getState();
        context.patchState({
            DVersionDTO: [...state.DVersionDTO, action.DVersionDTO]
        });
    }

    // @Action(ThemeOption)
    // addtheme(context: StateContext<AuthstateModal>, action: ThemeOption) {
    //     const state = context.getState();
    //     context.patchState({
    //         Dthemeoption: [...state.Dthemeoption, action.Dthemeoption]
    //     });
    // }

    @Action(ThemeOption)
    addtheme({getState, setState}: StateContext<AuthstateModal>, {Dthemeoption}: ThemeOption) {
        const state = getState();
        setState({
            ...state,
            Dthemeoption: Dthemeoption
        });
    }
}
