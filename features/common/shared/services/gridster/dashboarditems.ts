export interface DashboardItemModel {
  portletName: string;
  component: unknown;
}
const dashboardItems:any = [];

export class DashboardItems {
  static push(di: DashboardItemModel) {
    dashboardItems.push(di);
  }

  static getComponent(name: string) {
    console.log("Component Name:",name)
    return dashboardItems.filter((x:any) => x.portletName === name)[0].component;
  }
}
