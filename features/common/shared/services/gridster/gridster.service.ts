import { Injectable } from '@angular/core';
import { GridsterConfig, GridsterItem, GridsterItemComponentInterface, GridType} from 'angular-gridster2';

export interface IComponent {
  id: string;
  componentRef: string;
}

@Injectable({
  providedIn: 'root'
})
export class GridsterService {


  public withNonEditable: GridsterConfig = {
    draggable: {
      enabled: false
    },
    pushItems: false,
    resizable: {
      enabled: false
    },

  };
  public withEditable: GridsterConfig = {
    gridType: 'fit',
    margins: [5, 5],
    minCols: 8,
    maxCols: 10,
    minRows: 6,
    draggable: {
      enabled: true,
      stop: function (event, $element,widget) {
        console.log(widget);
      }.bind(this)
    },
    pushItems: true,
    resizable: {
      enabled: true
    }
  };

  public pages: GridsterItem[] = [];
  public components: IComponent[] = [];
  dropId: string;

  public changeSize()
  {
    if (this.withEditable.api && this.withEditable.api.optionsChanged) {
     this.withEditable.api.optionsChanged();
    }

  }
  constructor() {}

  getComponentRef(id: string): string {
    const comp = this.components.find(c => c.id === id);
    return comp ? comp.componentRef : null;
  }

}
