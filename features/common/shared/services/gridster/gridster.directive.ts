import { Directive , Input, OnChanges, 
  ViewContainerRef, ComponentFactoryResolver, ComponentRef, OnInit } from '@angular/core';



const components = {
};

@Directive({
  selector: '[appLayoutItem]'
})
export class GridsterDirective implements OnInit {

  @Input() componentRef: string;

  component: ComponentRef<any>;

  constructor(
    private container: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {

   const component = components[this.componentRef];

   if (component) {
    const factory = this.resolver.resolveComponentFactory<any>(component);
    this.component = this.container.createComponent(factory);
   }

  }

}
