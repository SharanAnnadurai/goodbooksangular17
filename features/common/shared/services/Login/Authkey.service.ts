import { HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router,RouterModule} from '@angular/router';
import { GBBaseService } from './../../../../../libs/gbdata/src/lib/gbdata.module';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service'

import {Store} from '@ngxs/store';
import { Md5 } from 'ts-md5';

import { AddLoginDTO} from './../../stores/auth.action';
import { AuthkeyDBservice } from './../../dbservices/Login/AuthkeyDB.service';
import { URLS } from './../../commonURL';
import { ILoginDetail } from './../../models/Login/LoginDetail';
import { ILoginDTO} from './../../models/Login/Logindto';

@Injectable({
  providedIn: 'root',
})
export class Authkeyservice extends GBBaseService {
  public loginDTO!: ILoginDTO;
  public authenticationDTO: any;
  isprod = this.env.gbsettings.isprod
  constructor(public AuthkeyDB: AuthkeyDBservice, public toaster:GbToasterService,public store: Store,private router: Router , @Inject('env') private env:any) {
    super();
   }
// uri  http://169.56.148.10:82/gb4\
  public Authkeyservice(userdetails: ILoginDetail,uri:any) {

let splituri = uri.split("/") 
let baseuri=splituri[2]
  console.log("baseuri",baseuri)

    return this.AuthkeyDB.AuthkeyDBservice().subscribe(authenticationDTO => {
      console.log("authenticationDTO",authenticationDTO)
      this.authenticationDTO = authenticationDTO;
      const password = userdetails.Password;
      const getusername = userdetails.UserName;
      const domainlink = baseuri
      const databasename = userdetails.Server;
      const url = URLS.USERLOGIN + getusername;
      this.authenticationDTO.UserCode = getusername;
      this.authenticationDTO.AppId = -1;
      this.authenticationDTO.DeviceId = -1;
      this.authenticationDTO.Method = 'POST';
      this.authenticationDTO.ModeOfWorking = 1;
      this.authenticationDTO.NounceCounter = '';
      this.authenticationDTO.Cnonce = this.generateNonce(25);
      this.authenticationDTO.BaseUri = domainlink;
      this.authenticationDTO.Uri = url;
      const ha1 = Md5.hashStr(getusername + this.authenticationDTO.Realm + password);
      const ha2 = Md5.hashStr(this.authenticationDTO.Method + this.authenticationDTO.Uri);
      const ha3 = Md5.hashStr(
        ha1 +
        this.authenticationDTO.Nonce +
        this.authenticationDTO.NounceCounter +
        this.authenticationDTO.Cnonce +
        this.authenticationDTO.Qop +
        ha2
      );
      this.authenticationDTO.Response = ha3;
      const headers = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'accept',
          ConnectionName: userdetails.Server,
        }),
      };
      return this.AuthkeyDB.userAuthenticateService(authenticationDTO, getusername, headers).subscribe(
        (res) => {
          console.log("userAuthenticateService",res);
          console.log("UserId",res.UserId);
          if (res.UserId != 0 && res.UserId != undefined) {
            try {
              const authDTO: any = res;
              const curdate = new Date();
              const offset = curdate.getTimezoneOffset();
              const dates = new Date();
              let formatstr = '' + dates.getDate();
              formatstr += dates.getMonth();
              formatstr += dates.getHours();
              formatstr += dates.getMinutes();
              formatstr += dates.getSeconds();

              this.loginDTO = {
                UserCode: getusername,
                DatabaseName: databasename,
                SessionId: 0,
                ValidToTime: authDTO.ValidToTime,
                ValidityOfSession: authDTO.ValidityOfSession,
                UserId: authDTO.UserId,
                UserPrimaryMailId: authDTO.UserPrimaryMailId,
                ServerId: authDTO.ServerId,
                RoleId: authDTO.RoleId,
                AppId: authDTO.AppId,
                DeviceId: authDTO.DeviceId,
                WorkOUId: authDTO.WorkOUId,
                WorkPeriodId: authDTO.WorkPeriodId,
                WorkPartyBranchId: authDTO.WorkPartyBranchId,
                WorkStoreId: authDTO.WorkStoreId,
                Realm: authDTO.Realm,
                ImgPath: '../images/user/',
                ModeOfOperation: authDTO.ModeOfOperation,
                MachineIP: '192.168.0.2',
                UserName: authDTO.UserName,
                TimeZone: offset,
                DatabaseOffset: authDTO.DatabaseOffset,
                DatabaseType: authDTO.DatabaseType,
                UserCriteriaConfigId: authDTO.UserCriteriaConfigId,
                ClientId: authDTO.ClientId,
                SourceType: authDTO.SourceType,
                UserCriteriaDTO: authDTO.UserCriteriaDTO,
                StartTime: formatstr,
                DateFormat: authDTO.DateFormat,
                CurrencyFormat: authDTO.CurrencyFormat,
                TimeFormat: authDTO.TimeFormat,
                QuantityFormat: authDTO.QuantityFormat,
                FromMailMenuId: -1,
                FromMailvalueId: -1,
                FEUri: url,
                IsAdmin: authDTO.IsAdmin,
                ServerConfigId: authDTO.ServerConfigId,
                ModeOfWorking: 1,
                BaseUri: domainlink,
                Geo: authDTO.Geo,
                LoginServerDate: authDTO.ServerDate,
                LoginEventLogId: authDTO.LoginEventLogId,
                ServerPopup: authDTO.ServerPopup,
                WorkFinanceBookId:-1,
                IsValidationRequired: 1,
                OuCode: authDTO.OuCode,
                OuName: authDTO.OuName,
                SelectlistOperationType:0,
                LastLoginUsedTime:authDTO.LastLoginUsedTime,
                ExpiryTime:660,
                GraceTime:15,
                ServerConfigOffset:0,
                ServerConfigMaxValue:0,
                TimeZoneId:91,
                TimeZoneDisplayName:"(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi",
                ServiceOffSet:330,
                FinalOffSetValue:660,
                FETIMEZONEOFFSET:-330,
                IsIpBasedCheckingRequired:1,
                IsForcePasswordChange:1,
                Delimiter:",",
                ReportBaseUri:"",
                FEVersion:4,
                
              };

              this.store.dispatch(new AddLoginDTO(this.loginDTO));
              // sessionStorage.setItem('loginDTO', JSON.stringify(this.loginDTO));
              // sessionStorage.setItem('DbName', databasename);
              // sessionStorage.setItem('username', JSON.stringify(getusername));
              this.router.navigate([""]);
              // this.router.navigate(["GoodBooks/Module"]);
            } catch (e) {
              return e;
            }
            return 'Value A';
          }
          else{
            this.toaster.showerror("Please check the Username & Password","ERROR");
          }
          return 'Value A';
        }
      );
    });
  }
  generateNonce = function(length: number) {
    const nonceChars = '0123456789abcdef';
    let result = '';
    for (let i = 0; i < length; i++) {
      result += nonceChars.charAt(
        Math.floor(Math.random() * nonceChars.length)
      );
    }

    return result;
  };
}
