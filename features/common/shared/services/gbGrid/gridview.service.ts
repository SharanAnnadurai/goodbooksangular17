import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GridData, GridProperties, GridSetting } from '../../models/gbgrid/gridview.model';
@Injectable({
  providedIn: 'root'
})
export class Gridviewservice {

  private grid_data = "/assets/data/grid-data.json";

  private grid_setting = "/assets/data/grid-setting.json";

  private grid_properties = "/assets/data/grid-properties.json";

  constructor(private http: HttpClient) { }
  gridData(): Observable<GridData[]> {
    return this.http.get<GridData[]>(this.grid_data);
  }
  gridSetting(): Observable<GridSetting[]> {
    return this.http.get<GridSetting[]>(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.http.get<GridProperties[]>(this.grid_properties);
  }
}
