import { Component, OnInit,EventEmitter, Input, Output, OnChanges, SimpleChanges  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting , Commonreportcol } from './../../../common/shared/models/gbgrid/gridview.model';
import {LayoutState} from './../../../layout/store/layout.state';
import {CommonReportService} from './../../service/commonreport.service';
import { ReportState } from './../../../../features/commonreport/datastore/commonreport.state';
import { DatePipe } from '@angular/common';
import { Store } from '@ngxs/store';
import { AuthState } from './../../../../features/common/shared/stores/auth.state';
import { ILoginDTO } from './../../../../features/common/shared/stores/auth.model';
@Component({
  selector: 'app-htmlview',
  templateUrl: './htmlview.component.html',
  styleUrls: ['./htmlview.component.scss']
})
export class HTMLViewComponent implements OnChanges {
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<ILoginDTO>;
  @Input() rowData!:any;
  @Input() columnData!: any[];
  @Select(ReportState.RowDataPassing) rowdatacommon$!: Observable<any>;
  rowDetail!:any;
  dateformat!: string;
  constructor(private route: ActivatedRoute ,public reportService: CommonReportService) { 
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['rowData']){
      if(typeof this.rowData == 'string'){
        this.rowData = [];
      }
      else{
        var datePipe = new DatePipe("en-US");
        this.rowDetail = JSON.stringify(this.rowData);
        let rowdata = this.rowData 
        let columns;
          this.loginDTOs$.subscribe(dto => {
            this.dateformat = dto.DateFormat
        })
        columns = Object.keys(rowdata[0])
        this.rowDetail = JSON.parse(this.rowDetail)
        for(let data of this.rowDetail){
          for(let item of columns){
            if(typeof data[item] == 'string' && data[item].includes('/Date(')){
              data[item] = datePipe.transform(data[item].replace("/Date(", "").replace(")/", ""), this.dateformat)
            }
          }
        }
        this.rowData = this.rowDetail
      }
    }
  }
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
}
