import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonReportService } from './../service/commonreport.service'
import { IReport,IColumnSetting } from './../model/report.model';
import { ReportState } from './../datastore/commonreport.state';
import { CriteriaConfigArray, ListofViewtypes, SelectedViewtype, RowDataPassing } from './../datastore/commonreport.action';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
// import { FilterState } from './../../gbfilter/store/gbfilter.state'
import { SharedService } from './../service/datapassing.service'
// import { DatePipe } from '@angular/common';
import { DashboardItems } from './../../common/shared/services/gridster/dashboarditems'
// const Reportreponse = require('./../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/commonreport/getreportdetail.brand.json');
// const col = require('./../../../apps/Goodbooks/Goodbooks/src/assets/data/brandlist.json')

// const Reportviewtypes = require('./../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/commonreport/ReportViewtypes.json');


@Component({
  selector: 'app-commonreport',
  templateUrl: './commonreport.component.html',
  styleUrls: ['./commonreport.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommonreportComponent implements OnInit {
  rows = []
  rowData = [];
  columnData :any[]= [];
  defaultColDef = [];
  viewtype!: string;
  sharedData!:any;
  Reportdetails!: IReport;
  datak! : any;
  dynamicreport!:any;

  constructor(public reportService: CommonReportService, public shareddataservice: SharedService, private store: Store, private router: Router, public activeroute: ActivatedRoute) { }
  // @Select(ReportState.Reportid) reportid$: Observable<any>;
  @Select(ReportState.CriteriaConfigArray) reportdetail$!: Observable<any>;
  @Select(ReportState.SelectedViewtype) selectedview$!: Observable<any>;
  @Select(ReportState.Configid) configid$!: Observable<any>;
  // @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(ReportState.Drilldownsettings) drilldownsettingdata$!: Observable<any>;
  @Select(ReportState.RowDataPassing) rowdatacommon$!: Observable<any>;

  ngOnInit(): void {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
    })
    this.viewtype = "CUSHTMLVIEW"
    this.defaultreportsettings();
    this.viewseletionsetting();
    this.rowcriteriasetting();
    this.filterdata();
    this.Drilldownsetting();
    this.defaultColDef = [
      {
        "resizable": true,
        "floatingFilter": true,
        "paginationAutoPageSize": true,
        "paginationPageSize": 13,
        "pagination": true,
        "undoRedoCellEditing": true,
        "undoRedoCellEditingLimit": 5,
        "enableCellChangeFlash": true,
        "enterMovesDownAfterEdit": true,
        "singleClickEdit": true,
        "animateRows": true,
        "rowSelection": "multiple",
        "enableRangeSelection": true,
        "paginateChildRows": true,
        "routingpath": "inventory/UOM",
        "routingparams": "Id",
        "routingfieldvalue": "Id"
      } as never
    ]
  }

  public defaultreportsettings() {
    // var datePipe = new DatePipe("en-US");
    this.activeroute.params.subscribe(res => {
      if (res) {
        let routeid = res['id'];
        this.shareddataservice.setDrillInfo(null)
        let idsplit = routeid.split(':');
        let id = idsplit[idsplit.length - 1];
        this.reportService.Reportdetailservice(id).subscribe(menudetails => {
          this.datak = menudetails;
          let reportDTO = menudetails;
          this.Reportdetails = menudetails;
          if (this.Reportdetails) {
            this.viewseletionsetting()
          }
          else {
          }
          this.reportService.Rowservice(this.datak, '').subscribe(res => {
            this.shareddataservice.setRowInfo(res.ReportDetail);
            // console.log("RowData:", this.rowData)
            this.shareddataservice.getRowInfo().subscribe(rowdata => {
              if (rowdata.length > 0) {
                this.rowData = rowdata;
              }
            })
          });
          this.store.dispatch(new CriteriaConfigArray(reportDTO))
          this.store.dispatch(new ListofViewtypes(reportDTO[0].ReportVsViewsArray))
          if (this.Reportdetails?.ReportVsViewsArray) {
            for (var data of this.Reportdetails.ReportVsViewsArray) {
              if (data.ReportViewId == this.Reportdetails.DefaultReportViewId) {
                let selectedview = ({ Id: data.ReportViewId, View: data.ReportViewType, ViewName: data.ReportViewName, TemplateLocation: data.TemplateLocation, SecondTemplateLocation: data.SecondTemplateLocation })
                this.store.dispatch(new SelectedViewtype(selectedview))
              }
            }
          }
        });
      }
    })
  }

  public viewseletionsetting() {
    let Reportmenureponse: IReport;
    this.reportdetail$.subscribe(res => {
      Reportmenureponse = res
    })
    let display: boolean;
    let finaldatacolsetting = [];
    this.selectedview$.subscribe(data => {
      if (data) {

        if (data.View == 0) {
          this.viewtype = "Grid";
          finaldatacolsetting = [];
          this.columnData = [];
          if (Reportmenureponse) {
            for (var coldata of Reportmenureponse.ReportVsFieldsArray) {
              if (data.Id == coldata.ReportViewId) {
                let celltye = ''
                if (coldata.ReportVsFieldsCalFormat == "18,4") {
                  celltye = "ag-right-aligned-cell"
                }
                else {
                  celltye = "ag-left-aligned-cell"
                }
                if (coldata.ReportVsFieldsIsDisplay != 1) {
                  display = false;
                  finaldatacolsetting.push({
                    headerName: coldata.ReportVsFieldsFieldTitle,
                    field: coldata.ReportVsFieldsFieldName,
                    hide: display,
                    width: coldata.ReportVsFieldsFieldWidth,
                    cellClass: celltye,
                    sortable: true
                    // const compt = DashboardItems.getComponent(data.SecondTemplateLocation);
                    // this.viewtype = "HTMLVIEW";
                    // this.dynamicreport = compt
                  })
                }
              }
            }
          }
          this.columnData = finaldatacolsetting;
        }
        else {
          console.log("SecondTemplateLocation:",data.SecondTemplateLocation)
          if(data.SecondTemplateLocation == 'STDHTMLVIEW'){
            this.viewtype = "STDHTMLVIEW";
          }
          else if (data.SecondTemplateLocation !="''" && data.SecondTemplateLocation !="") {
            const compt = DashboardItems.getComponent(data.SecondTemplateLocation);
            // this.store.dispatch(new RowDataPassing(this.rowData));
            this.viewtype = "CUSHTMLVIEW";
            this.dynamicreport = compt
            // this.columnData = col;
          }
          else {
            const otherview = Reportmenureponse.ReportVsViewsArray.find((c:any) => c.TemplateLocation == "")
            finaldatacolsetting = [];
            this.viewtype = "Grid";
            this.columnData = [];
            if (Reportmenureponse) {
              for (var coldata of Reportmenureponse.ReportVsFieldsArray) {
                if (otherview?.ReportViewId == coldata.ReportViewId) {
                  let celltye = ''
                  if (coldata.ReportVsFieldsCalFormat == "18,4") {
                    celltye = "ag-right-aligned-cell"
                  }
                  else {
                    celltye = "ag-left-aligned-cell"
                  }
                  if (coldata.ReportVsFieldsIsDisplay != 1) {
                    display = false;
                    finaldatacolsetting.push({
                      headerName: coldata.ReportVsFieldsFieldTitle,
                      field: coldata.ReportVsFieldsFieldName,
                      hide: display,
                      width: coldata.ReportVsFieldsFieldWidth,
                      cellClass: celltye,
                      sortable: true
                    })
                  }
                }
              }
            }
            this.columnData = finaldatacolsetting;
          }
          // console.log("Dynamic Component:", this.dynamicreport)
          // this.rowData = this.rows;
        }
      }
    })
  }




  public rowcriteriasetting() {
    let CriteriaConfigWithFieldValue:any;
    let CriteriaConfigId:any;

    this.configid$.subscribe(id => {
      if (id) {
        this.reportService.CriteriaConfigWithFieldValue(id).subscribe(res => {
          CriteriaConfigWithFieldValue = res[0]?.CriteriaConfigSectionArray[0]?.CriteriaConfigAttributeArray


          this.reportService.CriteriaConfigId(id).subscribe(resdata => {
            CriteriaConfigId = resdata.CriteriaConfigSectionArray[0]?.CriteriaConfigAttributeArray

            let data = {
              SectionCriteriaList: [
                {
                  "SectionId": 0,
                  "AttributesCriteriaList": [
                    {
                      "FieldName": CriteriaConfigWithFieldValue[0]?.CriteriaAttributeName,
                      "OperationType": CriteriaConfigWithFieldValue[0]?.CriteriaConfigAttributeOperationType,
                      "FieldValue": JSON.parse(CriteriaConfigWithFieldValue[0]?.CriteriaConfigAttributeCriteriaFieldValue),
                      "JoinType": CriteriaConfigWithFieldValue[0]?.CriteriaConfigAttributeCriteriaJoin,
                      "CriteriaAttributeName": CriteriaConfigId[0]?.CriteriaAttributeName,
                      "CriteriaAttributeValue": CriteriaConfigWithFieldValue[0]?.CriteriaConfigAttributeCriteriaFieldDisplayValue,
                      "IsHeader": 1,
                      "IsCompulsory": 1,
                      "CriteriaAttributeId": CriteriaConfigWithFieldValue[0]?.CriteriaAttributeId,
                      "CriteriaAttributeType": CriteriaConfigId[0]?.CriteriaAttributeType
                    }
                  ],
                  "OperationType": 0
                }
              ]
            }
            if(data){
              this.reportService.reportDatacriteria(data, '').subscribe((res:any) => {
                this.rowData = res.ReportDetail;
                this.store.dispatch(new RowDataPassing(this.rowData));
              });
            }
          });
        });

      }
    })
  }


  public filterdata() {
    // this.filtercriteria$.subscribe(data => {
    //   if (data) {
    //     console.log("Filter Data:", data);
    //     if (data.SectionCriteriaList[0].AttributesCriteriaList.length > 0) {
    //       this.reportService.reportDatacriteria(data, '').subscribe(res => {
    //         console.log("ABCDC",res)
    //         this.shareddataservice.setRowInfo(res.ReportDetail);
    //       });
    //     }
    //     else {
    //       let datas = null
    //       this.reportService.reportDatacriteria(datas, '').subscribe(res => {
    //         this.shareddataservice.setRowInfo(res.ReportDetail);
    //       });
    //     }
    //   }
    // })
  }


  public Drilldownsetting() {
    this.shareddataservice.getDrillInfo().subscribe(data => {
      console.log("dataaaaaaa.....",data)
      if (data != null) {
        console.log("data[0].length............",data)
          let selectedrowdata = data[0]
          let drillmenuid = data[1]
          let fieldname = data[2]
          this.reportService.drilldownsettingservice(drillmenuid, selectedrowdata, fieldname).subscribe(response => {
          })
      }
    })
  }


}