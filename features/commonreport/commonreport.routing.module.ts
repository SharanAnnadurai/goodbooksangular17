import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CommonreportComponent} from './../commonreport/screens/commonreport.component'
import {GbgridComponent} from './../gbgrid/components/gbgrid/gbgrid.component';
import {HTMLViewComponent} from './screens/html/htmlview.component';
import { CommonCustomHTMLComponent } from './screens/CommonCustomHTML/CommonCustomHTML.component';

const routes: Routes = [
    {
        path: 'commonreport/:id',
        component: CommonreportComponent
    },
    {
      path: 'GRID',
      component: GbgridComponent
    },
    {
      path: 'HTMLVIEW',
      component: HTMLViewComponent
    },
    {
      path: 'CustomHTML',
      component: CommonCustomHTMLComponent
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class CommonReportRoutingModule { }