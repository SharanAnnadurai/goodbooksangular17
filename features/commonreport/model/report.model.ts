export class IReport {
    MenuId!: number;
    MenuCode!: string;
    MenuName!: string;
    MenuType!: string;
    MenuNature!: number;
    MenuParentId!: number;
    ModuleId!: number;
    MenuReportId!: number;
    ReportName!: string;
    ReportUri!: string;
    ReportViewType!: number;
    WebFormId!: number;
    WebFormName!: string;
    WebFormURL!: string;
    WebFormTemplate!: string;
    WebServiceId!: number;
    WebServiceName!: string;
    WebServiceUriTemplate!: string;
    WebServiceMethodType!: number;
    MethodType!: string;
    BizTransactionClassId!: number;
    BizTransactionClassCode?: any;
    BizTransactionClassName?: any;
    DefaultReportFormatId!: number;
    ReportFile!: string;
    ReportFormatName!: string;
    PortletDataSourceFileName!: string;
    DefaultReportFormatRepPath!: string;
    HelpUrl!: string;
    GcmTypeId!: number;
    URIParameterValue!: string;
    EntityId!: number;
    EntityCode!: string;
    EntityName!: string;
    EntityPrimeryKeyMemberName?: any;
    DefaultReportViewId!: number;
    AnalysisQueryId!: number;
    UserVsMenuId!: number;
    ReportCriteriaArray!: ReportCriteriaArray[];
    ReportVsFieldsArray!: ReportVsFieldsArray[];
    CriteriaConfigArray!: CriteriaConfigArray[];
    ReportVsFormatsArray!: ReportVsFormatsArray[];
    WebServiceSettingArray!: WebServiceSettingArray[];
    ReportVsViewsArray!: ReportVsViewsArray[];
}

export class ReportCriteriaArray {
    MenuId!: number;
    MenuReportId!: number;
    WebServiceId!: number;
    WebServiceCriteriaSlNo!: number;
    WebServiceCriteriaId!: number;
    WebServiceCriteriaDisplayName!: string;
    WebServiceCriteriaFieldName!: string;
    WebServiceCriteriaPicklistFieldName!: string;
    WebServiceCriteriaDependPicklistName!: string;
    WebServiceCriteriaDependFieldName!: string;
    WebServiceCriteriaAttributeId!: number;
    CriteriaAttributeName!: string;
    IsCompulsory!: number;
    IsHeader!: number;
    CriteriaAttributeType!: number;
    CriteriaAttributeTypeName!: string;
    CriteriaAttributePicklistId!: number;
    CriteriaAttributePicklistCode!: string;
    CriteriaAttributePicklistDisplayFieldName!: string;
    CriteriaAttributePicklistDisplayTextBox!: string;
    CriteriaAttributePicklistSelectionFieldName!: string;
    CriteriaAttributePicklistURI!: string;
    CriteriaAttributeSearchFields!: string;
    CriteriaAttributeApplicableValues!: string;
    CriteriaAttributeVariableField!: string;
    CriteriaAttributeApplicableOperation!: string;
    CriteriaAttributeDefaultOperation!: number;
    CriteriaAttributeGcmTypeId!: number;
    CriteriaAttributeContextValueFieldName!: string;
    CriteriaAttributePicklistIsMultiSelect!: number;
}

export class ReportVsFieldsArray {
    MenuReportId!: number;
    ReportViewFieldsId!: number;
    ReportViewId!: number;
    ReportViewName!: string;
    ReportVsFieldsId!: number;
    ReportVsFieldsSlNo!: number;
    ReportVsFieldsFieldName!: string;
    ReportVsFieldsFieldTitle!: string;
    ReportVsFieldsFieldType!: number;
    ReportVsFieldsFieldWidth!: number;
    ReportVsFieldsIsDisplay!: number;
    ReportVsFieldsIsMergeColumn!: number;
    ReportVsFieldsIsGroupColumn!: number;
    ReportVsFieldsIsSubtotalColumn!: number;
    ReportVsFieldsIsGrandtotalColumn!: number;
    ReportVsFieldsCalFormat!: string;
    ReportVsFieldsDisplaySlNo!: number;
    CriteriaAttributeId!: number;
}

export class ConfigAttribute {
    CriteriaConfigAttributeId!: number;
    CriteriaConfigSectionId!: number;
    CriteriaConfigAttributeSlNo!: number;
    CriteriaAttributeId!: number;
    CriteriaAttributeName!: string;
    IsCompulsory!: number;
    IsHeader!: number;
    CriteriaAttributeType!: number;
    CriteriaConfigAttributeOperationType!: number;
    CriteriaConfigAttributeFieldValue!: string;
    CriteriaConfigAttributeFieldValueIn!: string;
    CriteriaConfigAttributeJoin!: number;
    CriteriaConfigAttributeFieldDisplayValue!: string;
    CriteriaConfigAttributeVariableField!: string;
    ApplicableOperation?: any;
    DefaultOperation!: number;
    CriteriaConfigAttributeFilterType!: number;
    IsChangeable!: number;
}

export class ConfigSection {
    CriteriaConfigSectionId!: number;
    CriteriaConfigSectionSlNo!: number;
    CriteriaConfigSectionJoin!: number;
    CriteriaConfigId!: number;
    ConfigAttribute!: ConfigAttribute[];
}

export class CriteriaConfigArray {
    CriteriaConfigId!: number;
    CriteriaConfigName!: string;
    CriteriaConfigVersion!: number;
    MenuId!: number;
    MenuCode!: string;
    MenuName!: string;
    PeriodFilter!: number;
    CriteriaOUId!: number;
    CriteriaConfigRecordsPerPage!: number;
    ConfigSection!: ConfigSection[];
    CriteriaConfigReportFormatId!: number;
    CriteriaConfigDefaultReportViewId!: number;
}

export class ReportVsFormatsArray {
    MenuReportId!: number;
    ReportVsForamtsId!: number;
    ReportVsForamtsSlNo!: number;
    ReportFormatId!: number;
    ReportFile!: string;
    ReportFormatName!: string;
    ReportFormatDataSourceFileName!: string;
    ReportFormatPurpose!: string;
    ReportFormatRepPath!: string;
    ReportFormatRemarks!: string;
    ReportVsForamtsIsVisible!: number;
}

export class WebServiceSettingDetailArray {
    WebServiceSettingDetailId!: number;
    ServiceSettingId!: number;
    WebServiceSettingDetailSlno!: number;
    CriteriaAttributeId!: number;
    CriteriaAttributeName!: string;
    CriteriaAttributeType!: number;
    CriteriaAttributeContextValueFieldName!: string;
    WebServiceSettingDetailIsCompulsory!: number;
    WebServiceSettingDetailIsHeader!: number;
    WebServiceSettingDetailIsVisible!: number;
    WebServiceSettingDetailIsChangeable!: number;
    WebServiceSettingDetailOperationType!: number;
    WebServiceSettingDetailCriteriaFieldValue!: string;
    WebServiceSettingDetailDisplayName!: string;
    WebServiceSettingDetailCriteriaJoin!: number;
    WebServiceCriteriaId!: number;
    WebServiceCriteriaFieldName!: string;
}

export class WebServiceSettingArray {
    WebServiceSettingId!: number;
    WebServiceId!: number;
    WebServiceName!: string;
    WebServiceSettingSettingType!: number;
    ReportId!: number;
    ReportName!: string;
    MenuId!: number;
    MenuCode!: string;
    MenuName!: string;
    WebServiceSettingCreatedById!: number;
    WebServiceSettingCreatedByName?: any;
    WebServiceSettingCreatedOn!: Date;
    WebServiceSettingModifiedById!: number;
    WebServiceSettingModifiedByName?: any;
    WebServiceSettingModifiedOn!: Date;
    WebServiceSettingDetailArray!: WebServiceSettingDetailArray[];
    WebServiceSettingSortOrder!: number;
    WebServiceSettingStatus!: number;
    WebServiceSettingVersion!: number;
    WebServiceSettingSourceType!: number;
}

export class ReportVsViewsArray {
    MenuReportId!: number;
    ReportViewId!: number;
    ReportViewName!: string;
    ReportId!: number;
    ReportName!: string;
    IsSystemView!: number;
    IsDefaultView!: number;
    ReportViewType!: number;
    SourceType!: number;
    TemplateId!: number;
    TemplateName!: string;
    TemplateLocation!: string;
    SecondTemplateLocation!:string;
    ColumnFreeze!: number;
    PivotFormat!: string;
    PageId!: number;
    PageCode!: string;
    PageName!: string;
    PageLayout!: string;
    PagePortletArray!: any[];
}

export class IColumnSetting {
    headerName!: any;
    field!: any;
    hide!: boolean;
    width!: any;
    cellClass!: string;
    sortable!: boolean;
};


