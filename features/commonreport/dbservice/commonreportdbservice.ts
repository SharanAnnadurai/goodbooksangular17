import { Injectable } from '@angular/core';
import { AuthState } from './../../common/shared/stores/auth.state';
import { Observable } from 'rxjs';
import { GBHttpService } from './../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { Select, Store } from '@ngxs/store';
// import { GbfilterDTO } from 'features/gbfilter/store/gbfilter.action';
import { Sourcecriteriadrilldown } from '../datastore/commonreport.action';
import { ReportState } from '../datastore/commonreport.state';

@Injectable({
    providedIn: 'root',
})
export class CommonReportdbservice {
    rowcriteria:any;
    constructor(public http: GBHttpService, public store: Store) { }
    @Select(AuthState.AddDTO) LOGINDTO$!: Observable<any>;
    @Select(ReportState.CriteriaConfigArray) rowcerteria$!: Observable<any>;

    public picklist(id: string): Observable<any> {
        let userid;
        this.LOGINDTO$.subscribe(dto => {
            let Logindto = dto;
            userid = dto.UserId;
        })
        const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=' + userid + '&MenuId=' + id;
        return this.http.httpget(url);
    }


    public reportData(pickobj:any, data:any) {
        let test = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [
                    ],
                    "OperationType": 1
                }
            ],

        }
        if(pickobj[0].WebServiceSettingArray.length > 0){
        let Webdataset = pickobj[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
        let criterset;
        if (pickobj[0].CriteriaConfigArray.length >= 1) {
            criterset = pickobj[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute
            for (let item of Webdataset) {
                for (let critem of criterset) {
                    if (critem.CriteriaAttributeId == item.CriteriaAttributeId) {
                        let dfielvalue = ""
                        if (item.WebServiceSettingDetailCriteriaFieldValue == "NONE" && critem.CriteriaConfigAttributeFieldValue != "NONE") {
                            dfielvalue = critem.CriteriaConfigAttributeFieldValue
                        }
                        else {
                            dfielvalue = critem.CriteriaConfigAttributeFieldValue
                            //dd = item.WebServiceSettingDetailCriteriaFieldValue

                        }
                        test.SectionCriteriaList[0].AttributesCriteriaList.push(
                            {
                                FieldName: item.WebServiceCriteriaFieldName,
                                OperationType: critem.CriteriaConfigAttributeOperationType,
                                FieldValue: JSON.parse(dfielvalue),
                                JoinType: critem.CriteriaConfigAttributeJoin,
                                CriteriaAttributeName: item.WebServiceSettingDetailDisplayName,
                                CriteriaAttributeValue: critem.CriteriaConfigAttributeFieldDisplayValue,
                                IsHeader: item.WebServiceSettingDetailIsHeader,
                                IsCompulsory: item.WebServiceSettingDetailIsCompulsory,
                                CriteriaAttributeId: item.CriteriaAttributeId,
                                CriteriaAttributeType: item.CriteriaAttributeType,
                                FilterType: "1"
                            } as never
                        )
                    }
                }
            }

            for( let webitems of Webdataset){
                if(pickobj[0].BizTransactionClassId != "-1" && webitems.WebServiceCriteriaFieldName == "BIZTransactionClassId"){
                    test.SectionCriteriaList[0].AttributesCriteriaList.push(
                        {
                            FieldName: webitems.WebServiceCriteriaFieldName,
                            OperationType: webitems.WebServiceSettingDetailOperationType,
                            FieldValue: pickobj[0].BizTransactionClassId,
                            JoinType: webitems.WebServiceSettingDetailCriteriaJoin,
                            CriteriaAttributeName: null,
                            CriteriaAttributeValue: "ABC ORGANIZATION",
                            IsHeader: webitems.WebServiceSettingDetailIsHeader,
                            IsCompulsory: webitems.WebServiceSettingDetailIsCompulsory,
                            CriteriaAttributeId: webitems.CriteriaAttributeId,
                            CriteriaAttributeType: webitems.CriteriaAttributeType,
                        } as never
                    )
                }
            }
        }
    }
        let criteria;
        let datas = pickobj[0].WebServiceUriTemplate.replace("183.82.250.223", "newserver")
        
        if (test.SectionCriteriaList[0].AttributesCriteriaList.length == 0) {
            criteria = {
                "ReportUri": pickobj[0].WebServiceUriTemplate,
                "Method": pickobj[0].MethodType,
                "ReportTitle": "",
                "CriteriaDTO": null,
                "MenuId": pickobj[0].MenuId,
                "WebserviceId": pickobj[0].WebServiceId,
                "PeriodFilter": 0,
                "RecordsPerPage": 0
            }
        }
        else {
            criteria = {
                "ReportUri": pickobj[0].WebServiceUriTemplate,
                "Method": pickobj[0].MethodType,
                "ReportTitle": "",
                "CriteriaDTO": test,
                "MenuId": pickobj[0].MenuId,
                "WebserviceId": pickobj[0].WebServiceId,
                "PeriodFilter": 0,
                "RecordsPerPage": 0
            }
        }
        this.store.dispatch(new Sourcecriteriadrilldown(criteria));
        this.rowcriteria = criteria
        let url;
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = '/cos/Common.svc/Report';
        // this.store.dispatch(new GbfilterDTO(test))
        return this.http.httppost(url, criteria);
    }


    public CriteriaConfigWithFieldValue(id:any) {
        let urls = '/fws/CriteriaConfig.svc/CriteriaConfigWithFieldValue/?CriteriaConfigId=' + id;
        return this.http.httpget(urls);
    }


    public CriteriaConfigId(id:any) {
        let urls = '/fws/CriteriaConfig.svc/?CriteriaConfigId=' + id;
        return this.http.httpget(urls);
    }

    public reportDatacriteria(criterias:any, datas:any):any {
        let finalcrteria:any;
        this.rowcerteria$.subscribe(data => {
            let criteria = {
                "ReportUri": data[0].WebServiceUriTemplate,
                "Method": data[0].MethodType,
                "ReportTitle": "",
                "CriteriaDTO": criterias,
                "MenuId": data[0].MenuId,
                "WebserviceId": data[0].WebServiceId,
                "PeriodFilter": 0,
                "RecordsPerPage": 0
            }
            finalcrteria = criteria
        })
        if(finalcrteria){
            let url;
            let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
            let str = fullurl.split('/gb4');
            let urlstr = str[str.length - 1];
            url = urlstr;
            this.store.dispatch(new Sourcecriteriadrilldown(finalcrteria));
            // this.store.dispatch(new GbfilterDTO(null)); 
            return this.http.httppost(url, finalcrteria);
        }

    }


    public drilldownsettingdb(drillmenuid:any, selectedrowdata:any,fieldname:any) : Observable<any> {
        return this.http.Drilldownsetting(drillmenuid, selectedrowdata,fieldname);
    }
}















///--------------------------------------------------------------07-11-2023---------- used code
// import { Injectable } from '@angular/core';
// import { AuthState } from './../../common/shared/stores/auth.state';
// import { Observable } from 'rxjs';
// import { GBHttpService } from '@goodbooks/gbcommon';
// import { Select, Store } from '@ngxs/store';
// import { GbfilterDTO } from 'features/gbfilter/store/gbfilter.action';
// import { Sourcecriteriadrilldown } from '../datastore/commonreport.action';
// import { ReportState } from '../datastore/commonreport.state';

// @Injectable({
//     providedIn: 'root',
// })
// export class CommonReportdbservice {
//     rowcriteria;
//     constructor(public http: GBHttpService, public store: Store) { }
//     @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
//     @Select(ReportState.CriteriaConfigArray) rowcerteria$: Observable<any>;

//     public picklist(id: string): Observable<any> {
//         let userid;
//         this.LOGINDTO$.subscribe(dto => {
//             let Logindto = dto;
//             userid = dto.UserId;
//         })
//         const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=' + userid + '&MenuId=' + id;
//         return this.http.httpget(url);
//     }


//     public reportData(pickobj, data) {
//         let test = {

//             "SectionCriteriaList": [
//                 {
//                     "SectionId": 0,
//                     "AttributesCriteriaList": [
//                     ],
//                     "OperationType": 1
//                 }
//             ],

//         }
//         let Webdataset = pickobj[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
//         console.log("Webdataset", Webdataset)
//         let criterset;
//         console.log("pickobj[0].CriteriaConfigArray.length", pickobj[0].CriteriaConfigArray.length)
//         if (pickobj[0].CriteriaConfigArray.length >= 1) {
//             criterset = pickobj[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute
//             console.log("criterset", criterset)
//             for (let item of Webdataset) {
//                 console.log("item", item)
//                 for (let critem of criterset) {
//                     console.log("critem", critem)
//                     if (critem.CriteriaAttributeId == item.CriteriaAttributeId) {
//                         let dd = ""
//                         if (item.WebServiceSettingDetailCriteriaFieldValue == "NONE" && critem.CriteriaConfigAttributeFieldValue != "NONE") {
//                             dd = critem.CriteriaConfigAttributeFieldValue
//                         }
//                         else {
//                             dd = item.WebServiceSettingDetailCriteriaFieldValue
//                         }
//                         console.log("WebServiceCriteriaFieldName", item.WebServiceCriteriaFieldName)
//                         test.SectionCriteriaList[0].AttributesCriteriaList.push(
//                             {
//                                 FieldName: item.WebServiceCriteriaFieldName,
//                                 OperationType: critem.CriteriaConfigAttributeOperationType,
//                                 FieldValue: JSON.parse(dd),
//                                 JoinType: critem.CriteriaConfigAttributeJoin,
//                                 CriteriaAttributeName: item.WebServiceSettingDetailDisplayName,
//                                 CriteriaAttributeValue: critem.CriteriaConfigAttributeFieldDisplayValue,
//                                 IsHeader: item.WebServiceSettingDetailIsHeader,
//                                 IsCompulsory: item.WebServiceSettingDetailIsCompulsory,
//                                 CriteriaAttributeId: item.CriteriaAttributeId,
//                                 CriteriaAttributeType: item.CriteriaAttributeType,
//                                 FilterType: "1"
//                             }
//                         )
//                     }
//                 }
//             }
//         }
//         let criteria;
//         let datas = pickobj[0].WebServiceUriTemplate.replace("183.82.250.223", "newserver")
//         console.log("TEST", test)
//         if (test.SectionCriteriaList[0].AttributesCriteriaList.length == 0) {
//             console.log("null", test.SectionCriteriaList[0].AttributesCriteriaList.length)
//             criteria = {
//                 "ReportUri": pickobj[0].WebServiceUriTemplate,
//                 "Method": pickobj[0].MethodType,
//                 "ReportTitle": "",
//                 "CriteriaDTO": null,
//                 "MenuId": pickobj[0].MenuId,
//                 "WebserviceId": pickobj[0].WebServiceId,
//                 "PeriodFilter": 0,
//                 "RecordsPerPage": 0
//             }
//         }
//         else {
//             console.log("test", test.SectionCriteriaList[0].AttributesCriteriaList.length)
//             criteria = {
//                 "ReportUri": pickobj[0].WebServiceUriTemplate,
//                 "Method": pickobj[0].MethodType,
//                 "ReportTitle": "",
//                 "CriteriaDTO": test,
//                 "MenuId": pickobj[0].MenuId,
//                 "WebserviceId": pickobj[0].WebServiceId,
//                 "PeriodFilter": 0,
//                 "RecordsPerPage": 0
//             }
//         }
//         this.store.dispatch(new Sourcecriteriadrilldown(criteria));
//         this.rowcriteria = criteria
//         let url;
//         let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
//         let str = fullurl.split('/gb4');
//         let urlstr = str[str.length - 1];
//         url = '/cos/Common.svc/Report';
//         // this.store.dispatch(new GbfilterDTO(test));
//         return this.http.httppost(url, criteria);
//     }


//     public CriteriaConfigWithFieldValue(id) {
//         let urls = '/fws/CriteriaConfig.svc/CriteriaConfigWithFieldValue/?CriteriaConfigId=' + id;
//         return this.http.httpget(urls);
//     }


//     public CriteriaConfigId(id) {
//         let urls = '/fws/CriteriaConfig.svc/?CriteriaConfigId=' + id;
//         return this.http.httpget(urls);
//     }

//     public reportDatacriteria(criterias, datas) {
//         console.log("KAKAKKAKAKAKAKAKAKA",criterias)
//         let finalcrteria;
//         this.rowcerteria$.subscribe(data => {
//             let criteria = {
//                 "ReportUri": data[0].WebServiceUriTemplate,
//                 "Method": data[0].MethodType,
//                 "ReportTitle": "",
//                 "CriteriaDTO": criterias,
//                 "MenuId": data[0].MenuId,
//                 "WebserviceId": data[0].WebServiceId,
//                 "PeriodFilter": 0,
//                 "RecordsPerPage": 0
//             }
//             console.log("criterias", criterias)
//             finalcrteria = criteria
//         })
//         if(finalcrteria){
//             let url;
//             let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
//             let str = fullurl.split('/gb4');
//             let urlstr = str[str.length - 1];
//             url = urlstr;
//             this.store.dispatch(new GbfilterDTO(null)); 
//             return this.http.httppost(url, finalcrteria);
            

//         }

//     }


//     // public drilldownsettingdb(sourcecriteria, drillmenuid, selectedrowdata) : Observable<any> {
//     //     return this.http.Drilldownsetting(sourcecriteria, drillmenuid, selectedrowdata);
//     // }
// }