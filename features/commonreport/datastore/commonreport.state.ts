import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import {
    Reportid,CriteriaConfigArray,ListofViewtypes , SelectedViewtype , Configid, RowDataPassing, Sourcecriteriadrilldown,Drilldownsetting
} from './commonreport.action';

export class ReportStateModel {
  id!: string | null;
  criteriaconfigarray : any;
  listofviewtypes: any;
  selectedviewtype: any
  selectedconfigid : any;
  rowdata:any
  sourcecriteriadrilldown:any
  drilldownsettings:any
}

@State<ReportStateModel>({
  name: 'Report',
  defaults: {
    id: null,
    criteriaconfigarray:null,
    listofviewtypes: null,
    selectedviewtype:null,
    selectedconfigid: null,
    rowdata:null,
    sourcecriteriadrilldown:null,
    drilldownsettings:null
  },
})
@Injectable()
export class ReportState {

  @Selector() static Reportid(state: ReportStateModel) {
    return state.id;
  }

  @Selector() static CriteriaConfigArray(state: ReportStateModel) {
    return state.criteriaconfigarray;
  }

  @Selector() static ListofViewtypes(state: ReportStateModel) {
    return state.listofviewtypes;
  }

  @Selector() static SelectedViewtype(state: ReportStateModel) {
    return state.selectedviewtype;
  }

  @Selector() static Configid(state: ReportStateModel) {
    return state.selectedconfigid;
  }

  @Selector() static RowDataPassing(state: ReportStateModel) {
    return state.rowdata;
  }

  @Selector() static Sourcecriteriadrilldown(state: ReportStateModel) {
    return state.sourcecriteriadrilldown;
  }

  @Selector() static Drilldownsettings(state: ReportStateModel) {
    return state.drilldownsettings;
  }

  @Action(Reportid)
  Changeid(context: StateContext<ReportStateModel>, newid: Reportid) {
    const state = context.getState();
    context.setState({ ...state, id: newid.reportid });
  }

  @Action(CriteriaConfigArray)
  CriteriaConfigArrayValue(context: StateContext<ReportStateModel>, newvalue: CriteriaConfigArray) {
    const state = context.getState();
    context.setState({ ...state, criteriaconfigarray: newvalue.criteriaconfig });
  }

  @Action(ListofViewtypes)
  ListTypesValue(context: StateContext<ReportStateModel>, listvalues: ListofViewtypes) {
    const state = context.getState();
    context.setState({ ...state, listofviewtypes: listvalues.viewtypes });
  }

  @Action(SelectedViewtype)
  SelectedView(context: StateContext<ReportStateModel>, viewtype: SelectedViewtype) {
    const state = context.getState();
    context.setState({ ...state, selectedviewtype: viewtype.selectedview });
  }

  @Action(Configid)
  SelectedConfigid(context: StateContext<ReportStateModel>, configid: Configid) {
    const state = context.getState();
    context.setState({ ...state, selectedconfigid: configid.selectedconfigid });
  }

  @Action(RowDataPassing)
  RowDataPassing(context: StateContext<ReportStateModel>, Rowdatapassing: RowDataPassing) {
    const state = context.getState();
    context.setState({ ...state, rowdata: Rowdatapassing.rowdatapassing });
  }

  @Action(Sourcecriteriadrilldown)
  Sourcecriteriadrilldown(context: StateContext<ReportStateModel>, Sourcecriteriadrilldown: Sourcecriteriadrilldown) {
    const state = context.getState();
    context.setState({ ...state, sourcecriteriadrilldown: Sourcecriteriadrilldown.sourcecriteriadrilldown });
  }

  @Action(Drilldownsetting)
  Drilldownsetting(context: StateContext<ReportStateModel>, Drilldownsettings: Drilldownsetting) {
    const state = context.getState();
    context.setState({ ...state, drilldownsettings: Drilldownsettings.drilldownsetting });
  }

}
