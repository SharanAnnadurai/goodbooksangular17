export class  GridData{
    'Athlete' : string;
    'Age' : number;
    'Country' : string;
    'Year' : number;
    
}
export class GridSetting{
    'headerName' : string;
    'field' : string;
    'sortable' : boolean;
    'filter' : boolean;
    'hide' : boolean;
    'checkboxSelection' : boolean;
    'resizable' : boolean;
    'editable' : boolean;
    'pinned' : string;
    'cellEditor' : string;
    'cellEditorParams' : string;
}
export class GridProperties{
    'paginationPageSize' : number;
    'pagination' : boolean;
    'undoRedoCellEditing' : boolean;
    'undoRedoCellEditingLimit' : number;
    'enableCellChangeFlash' : boolean;
    'enterMovesDown' : boolean;
    'enterMovesDownAfterEdit' : boolean;
    'rowSelection' : string;
    'enableRangeSelection' : boolean;
    'paginateChildRows' : boolean;
    'routingpath': string;
    'routingparams': string;
    'routingfieldvalue': string;
}