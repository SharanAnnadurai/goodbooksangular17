import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { GridProperties, GridSetting } from './../../../../features/common/shared/models/gbgrid/gridview.model';
import { ButtonVisibility, Title, Path } from './../../../layout/store/layout.actions';
import { Select, Store } from '@ngxs/store';
import { GridApi } from 'ag-grid-community';
import jsPDF from 'jspdf';
import { DatePipe } from '@angular/common';
import autoTable from 'jspdf-autotable';
import { RowDataPassing, Drilldownsetting } from './../../../../features/commonreport/datastore/commonreport.action';
import { ReportState } from './../../../../features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { GBHttpService } from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { AuthState } from './../../../../features/common/shared/stores/auth.state';
import { ILoginDTO } from './../../../../features/common/shared/stores/auth.model';
@Component({
  selector: 'goodbooks-gbgrid',
  templateUrl: './gbgrid.component.html',
  styleUrls: ['./gbgrid.component.scss']
})
export class GbgridComponent implements OnChanges {
  @Input() rowData!:any[];
  @Input() columnData!: any[];
  @Input() defaultColDef!: GridProperties[];
  @Output() Griddata!:any;
  @Output() selectedlist: EventEmitter<string> = new EventEmitter<string>();
  @Select(AuthState.AddDTO) loginDTOs$!: Observable<ILoginDTO>;
  @Select(ReportState.RowDataPassing) rowdatacommon$!: Observable<any>;
  rowDetail!:any;
  private gridApi!: GridApi;
  private gridColumnApi:any;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$!: Observable<any>;
  screenHeight!: number;
  screenWidth!: number;
  dateformat!: string;
  constructor(public lib: GBHttpService, public router: Router, public route: ActivatedRoute, public store: Store) {
   }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['rowData']){
      if(typeof this.rowData == 'string'){
        this.rowData = [];
      }
      else{
        var datePipe = new DatePipe("en-US");
        this.rowDetail = JSON.stringify(this.rowData);
        let rowdata = this.rowData 
        let columns;
          this.loginDTOs$.subscribe(dto => {
            this.dateformat = dto.DateFormat
        })
        if(rowdata != null){
          columns = Object.keys(rowdata[0])
          this.rowDetail = JSON.parse(this.rowDetail)
          for(let data of this.rowDetail){
            for(let item of columns){
              if(typeof data[item] == 'string' && data[item].includes('/Date(')){
                data[item] = datePipe.transform(data[item].replace("/Date(", "").replace(")/", ""), this.dateformat)
              }
            }
          }
          this.rowData = this.rowDetail
        }
        
      }
    }
  }
  
  public cc = [];
  public rr = [];

  get data() {
    let allRowData: any[] = [];
    this.gridApi.forEachNode(node => allRowData.push(node.data));
    this.Griddata = allRowData;
    return this.Griddata
  }
  public onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  public quicksearch(event: any) { 
    this.gridApi.setQuickFilter(event);
  }

  setStyle(name:any,value:any) {
    document.documentElement.style.setProperty(name, value);
  }
  
  public onRowClicked(event: any) {
    const url = this.defaultColDef[0].routingpath;
    const params = event[0].data.AccountScheduleId;
    console.log("onRowClicked", params, url, event)
    this.router.navigate([url, event[params]], { relativeTo: this.route });
  } 
  public onCell(event: any) {
    const url = this.defaultColDef[0].routingpath;
    let selectedRowNodesWithSort : any[] = [];
    this.gridApi.forEachNodeAfterFilterAndSort((node) => {
      selectedRowNodesWithSort.push(node.data);
    });
    this.store.dispatch(new RowDataPassing(selectedRowNodesWithSort));
    const params = event.data.UOMId;
    const selectedindexvalue = event.rowIndex
    const queryParams: any = {};
    queryParams.Id = JSON.stringify(params);
    queryParams.RowIndexselected = JSON.stringify(selectedindexvalue);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.store.dispatch(new ButtonVisibility("Forms"))
    this.router.navigate([url], navigationExtras);

    // this.sourcerowcriteria$.subscribe(data => {
    //   let selectedrowdata = event.data
    //   let sourcecriteria = data.CriteriaDTO
    //   let drillmenuid = -1399979647
    //   let drilldownsettingdata = [selectedrowdata,sourcecriteria,drillmenuid]
    //   this.store.dispatch(new Drilldownsetting(drilldownsettingdata));
    //   this.lib.Drilldownsetting(sourcecriteria, drillmenuid, selectedrowdata)
    // })
  }



  public captureScreen() {
    var doc = new jsPDF('p', 'pt');

    this.columnData.forEach((element:any) => {
      var temp:any[] = element.headerName;
      this.cc.push(temp as never);
    });

    this.rowData.forEach((element:any) => {
      var temp:any[] = [element.Code, element.Name];
      this.rr.push(temp as never);
    });
    autoTable(doc, {
      columns: this.cc,
      body: this.rr,
    })
    doc.save("table.pdf");
  }
}
