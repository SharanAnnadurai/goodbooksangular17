import { Directive, OnInit, ElementRef, OnChanges, Input, SimpleChanges } from "@angular/core";

@Directive({
  selector: '[getThemeList]',
})
export class GetThemeListDirective implements OnInit {
  private _el:HTMLSelectElement;

  constructor(el: ElementRef) {
    this._el = el.nativeElement;
  }

  ngOnInit(): void {
    let firstValue = "";
    let themes:any;
    const compStyle = document.defaultView ? document.defaultView.getComputedStyle(this._el): null;
    if (compStyle) {
      themes = compStyle.getPropertyValue("--themes");
    }
    this._el.innerHTML = "";
    const array=["Default", "Blue", "Pink", "Red", "Aqua", "Yellow", "Teal","Green", "Orange"]
    let count=0;
    themes.trim().split(' ').forEach((value:any) => {
      var element:HTMLOptionElement = document.createElement('option');
      element.text = array[count];
      element.value = array[count]; 
      this._el.add(element);
      if (firstValue === '') firstValue = array[count];
      count=count+1
    });
    this._el.value = firstValue;
    let oc = new Event('change');
    this._el.dispatchEvent(oc);
  }
}

@Directive({
  selector: '[getThemeMode]',
})
export class GetThemeModeListDirective implements OnChanges {
  @Input('getThemeMode')
  public selectedTheme!: string;

  private _el:HTMLSelectElement;

  constructor(el: ElementRef) {
    this._el = el.nativeElement;
  }
  ngOnChanges(changes: SimpleChanges): void {
    const compStyle = document.defaultView ? document.defaultView.getComputedStyle(this._el): null;
    let modes
    if (compStyle !== null) {
      modes = compStyle.getPropertyValue("--" + this.selectedTheme);
      // Rest of your code using 'modes'
    }
    // const modes = compStyle.getPropertyValue("--" + this.selectedTheme);
    this._el.innerHTML = "";
    let firstValue = "";
    if (modes) {
      modes.trim().split(' ').forEach((value) => {
        var element:HTMLOptionElement = document.createElement('option');
        element.text = value;
        element.value = value;
        this._el.add(element);
        if (firstValue === "") firstValue = value;
      });
    }
    this._el.value = firstValue;
    let oc = new Event('change');
    this._el.dispatchEvent(oc);
  }

}
