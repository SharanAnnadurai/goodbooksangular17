import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-matform',
  templateUrl: './matform.component.html',
  styleUrls: ['./matform.component.scss']
})
export class MatformComponent implements OnInit {

  checked = true;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  color: 'primary' | 'accent' | 'warn' | '' = '';
  disabled = false;

  @ViewChild(MatAccordion) accordion!: MatAccordion;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog() {
    this.dialog.open(MatDialogComponent);
  }

}

@Component({
  selector: 'app-matform',
  template: `
    <h1 mat-dialog-title>Dialog with elements</h1>
    <div mat-dialog-content>This dialog showcases the title, close, content and actions elements.</div>
    <div mat-dialog-actions>
      <button mat-button mat-dialog-close>Close</button>
    </div>
  `,
  styleUrls: ['./matform.component.scss']
})
export class MatDialogComponent {
}
