import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, ViewEncapsulation } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder ,FormArray, FormGroup } from '@angular/forms';
import { GBDataPageServiceNG } from './../../../../libs/uicore/src/lib/services/gbpage.service';
import { ReportCriteriaArray, RootObject } from './../../model/IFilter';
import { GBBaseFormGroup } from './../../../../libs/uicore/src/lib//classes/GBFormGroup';
import { GbFilterService } from './../../services/gbfilter/gbfilter.service';
import { Select, Store } from '@ngxs/store';
import { GbfilterDTO,GbReportformatselection} from './../../store/gbfilter.action';
import { ReportState } from './../../../commonreport/datastore/commonreport.state'
import { Observable } from 'rxjs';
import { LayoutState } from './../../../../features/layout/store/layout.state';
// import { Entitylookupdependpicklist } from 'features/entitylookup/store/entitylookup.action'; 
import { DateAdapter } from '@angular/material/core';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { MatDialog } from '@angular/material/dialog';
// const criteriajsondata = require('./../../crietriajson.json');

@Component({
  selector: 'app-gbfilter',
  templateUrl: './gbfilter.component.html',
  styleUrls: ['./gbfilter.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG },
    { provide: 'DataService', useClass: GbFilterService }
  ]
})



export class GbFilterComponent {
  @Select(LayoutState.Title) view$!: Observable<any>;
  @Select(ReportState.Reportid) reportid$!: Observable<string>;
  Advancefilter: boolean = false;
  public isactive=2;
  public Fromdate!:any;
  public Todate!:any;
  public WorkPeriodFromDate:any= "/Date(1617215400000)/";
  public Reportformat :any[]= []
  public WorkPeriodToDate: any= "/Date(1680201000000)/";
  public Datevalue!:any
  public filterdata!:any;
  public configdata!:any;
  readioSelected: any='CurrentYear';
  public inivisbledataarray!:any;
  public id!: string;
  public testidcode!:any;
  public testid!:any;
  public Biztransactionclass!:any;
  public PeriodFromArray = ['PeriodFromDate','PeriodFrom'];
  public PeriodToArray = ['PeriodToDate','PeriodTo']
  // public store : Store;
  form: FormGroup;
  criteria!: FormGroup;
  title = 'Filter';
  usercolor!: any;
  heading!:string;
  constructor(public dialog: MatDialog,private dateAdapter: DateAdapter<Date>, public fb: FormBuilder, public dataservice: GbFilterService, public store: Store, public ref: ChangeDetectorRef) {
    this.form = this.fb.group({
      AttributesCriteriaList: this.fb.array([])
    }) as FormGroup;
    this.dateAdapter.setLocale('en-GB');
  }

  ngOnInit(): void {
    var datePipe = new DatePipe("en-US");
    var temp1 = this.WorkPeriodFromDate;
    var temp2 = this.WorkPeriodToDate;
    temp1 = datePipe.transform(
      JSON.parse(temp1.replace('/Date(', '').replace(')/', '')),
      'dd-mm-yyyy'
    );
    temp2 = datePipe.transform(
      JSON.parse(temp2.replace('/Date(', '').replace(')/', '')),
      'dd-mm-yyyy'
    );
    var lastDayOfMonth = new Date(temp2.split("-")[2], temp2.split("-")[1]+3, 0);
    var FirstDayOfMonth = new Date(temp1.split("-")[2], temp1.split("-")[1] + 3, 1);
    this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
    this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    this.view$.subscribe(data => {
      if (data){
        this.heading = data
      }
    })
    this.reportid$.subscribe(id => {
      if (id) {
        (this.dataservice).getfilterservice(id).subscribe((res: RootObject[]) => {
          this.filterdata = res;
          this.configdata = res;
          // let critersetting = res[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute
          let reportid = this.configdata[0].MenuReportId;
          let menuid = this.configdata[0].MenuId
          this.dataservice.reportformatservices(reportid, menuid).subscribe((res:any) => {
            for (let data of res) {
              if(data.ReportFormatId == this.configdata[0].DefaultReportFormatId){
                this.store.dispatch(new GbReportformatselection(data.ReportFormatId));             // should hide on test
              }
              let finaldata:any = data
              this.Reportformat.push(finaldata)
            }
          });
          this.filterchange();
        });
      }
    })
  }

  private filterchange() : void{
    let web = this.configdata[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
          this.inivisbledataarray = web
          let cri = this.configdata[0].ReportCriteriaArray;
          var finalizedArray = []
          let FilterPushdata = []
          for (let webdata of web) {
            if(this.configdata[0].BizTransactionClassId != -1 && webdata.WebServiceCriteriaFieldName == 'BIZTransactionClassId'){
              this.Biztransactionclass = webdata
            }
            if (webdata.WebServiceSettingDetailIsVisible == 0) {
              for (let cridata of cri) {
                if (webdata.CriteriaAttributeId == cridata.WebServiceCriteriaAttributeId) {
                  let dependicklistvalue = [];
                  if (cridata.WebServiceCriteriaDependFieldName != 'NONE') {
                    let DependPicklistlength = [
                      cridata.WebServiceCriteriaDependFieldName.split(','),
                    ];
                    if (DependPicklistlength[0].length > 1) {
                      let DependPicklistFieldName = [
                        cridata.WebServiceCriteriaDependPicklistName.split(','),
                      ];
                      // let WebServiceCriteriaDependFieldName =[cridata.WebServiceCriteriaDependFieldName.split(','),]
                      let FieldName = [];
                      for (let DependPicklist of DependPicklistlength[0]) {
                        FieldName.push({ DependPicklist: DependPicklist });
                      }

                      let Picklist = [];
                      for (let PicklistName of DependPicklistFieldName[0]) {
                        Picklist.push({ PicklistName: PicklistName });
                      }
                      let Filterdata = [];
                      Filterdata.push({
                        FieldName,
                        Picklist,
                      });
                      for (var i = 0, l = Filterdata[0].FieldName.length; i < l; i++) {
                        FilterPushdata.push({
                          a: Filterdata[0].FieldName[i].DependPicklist,
                          b: Filterdata[0].Picklist[i].PicklistName
                        })
                      }
                      for (let datavalue of FilterPushdata) {
                        for (let webvalue of web) {
                          if (datavalue.a == webvalue.WebServiceCriteriaFieldName) {
                            let depvalue;
                            if(this.configdata[0].CriteriaConfigArray[0]){
                              for(let cridat of this.configdata[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute){
                                if(cridat.CriteriaAttributeName == datavalue.a){
                                  depvalue = cridat.CriteriaConfigAttributeFieldValue
                                }
                              }
                            }
                            if(datavalue.a == "BIZTransactionClassId"){
                              depvalue = this.configdata[0].BizTransactionClassId
                            }
                            // else{
                            // 
                            // }
                            if(depvalue != 'NONE' && depvalue  != undefined ){
                              depvalue =  Number(depvalue)
                              dependicklistvalue.push({
                                DependPicklistId: datavalue.a,
                                DependPicklistName: datavalue.b,
                                Operationtpyevalue: webvalue.WebServiceSettingDetailOperationType,
                                values: depvalue
                              });
                            }
                            else{
                              dependicklistvalue.push({
                                DependPicklistId: datavalue.a,
                                DependPicklistName: datavalue.b,
                                Operationtpyevalue: webvalue.WebServiceSettingDetailOperationType,
                                values: 'NONE'
                              });
                            }
                          }
                        }
                      }
                    }
                    else{
                      let flag = 0
                      if(this.configdata[0].CriteriaConfigArray[0]){
                        for(let cridat of this.configdata[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute){
                          if(cridat.CriteriaAttributeName == cridata.WebServiceCriteriaDependFieldName){
                            flag = 1;
                            dependicklistvalue.push({
                              DependPicklistId: cridata.WebServiceCriteriaDependFieldName,
                              DependPicklistName: cridata.WebServiceCriteriaDependPicklistName,
                              Operationtpyevalue: cridata.CriteriaAttributeDefaultOperation,
                              values: cridat.CriteriaConfigAttributeFieldValue,
                            });
                          }
                        }
                      }
                      if(cridata.WebServiceCriteriaDependFieldName == "BIZTransactionClassId"){
                        flag = 1;
                        dependicklistvalue.push({
                          DependPicklistId: cridata.WebServiceCriteriaDependFieldName,
                          DependPicklistName: cridata.WebServiceCriteriaDependPicklistName,
                          Operationtpyevalue: cridata.CriteriaAttributeDefaultOperation,
                          values: this.configdata[0].BizTransactionClassId,
                        });
                      }
                      if(flag == 0){
                        dependicklistvalue.push({
                          DependPicklistId: cridata.WebServiceCriteriaDependFieldName,
                          DependPicklistName: cridata.WebServiceCriteriaDependPicklistName,
                          Operationtpyevalue: cridata.CriteriaAttributeDefaultOperation,
                          values: 'NONE',
                        });
                      }
                      // for(let webdatasss of web){
                      //   if (cridata.WebServiceCriteriaDependFieldName == webdatasss.WebServiceCriteriaFieldName) {
                      //     dependicklistvalue.push({
                      //       DependPicklistId: cridata.WebServiceCriteriaDependFieldName,
                      //       DependPicklistName: cridata.WebServiceCriteriaDependPicklistName,
                      //       Operationtpyevalue: cridata.CriteriaAttributeDefaultOperation,
                      //       values: webdatasss.WebServiceSettingDetailCriteriaFieldValue,
                      //     });
                      //     // console.log("dependicklistvalue:",this.form)
                      //   }
                      // }
                    }
                  }
                  // if(cridata.WebServiceCriteriaDependFieldName != "NONE"){
                  //   dependicklistvalue.push({
                  //     DependPicklistName : cridata['WebServiceCriteriaDependFieldName'],
                  //     Operationtpyevalue:cridata['CriteriaAttributeDefaultOperation']
                  //   })
                  // }
                  let defaultvalue = ''
                  if (webdata.WebServiceSettingDetailCriteriaFieldValue != "NONE") {
                    defaultvalue = webdata.WebServiceSettingDetailCriteriaFieldValue
                  }
                  finalizedArray.push({
                    IFieldName: webdata['WebServiceCriteriaFieldName'],
                    IOperationType: webdata['WebServiceSettingDetailOperationType'],
                    IFieldValue: webdata['CriteriaAttributeContextValueFieldName'],
                    IJoinType: webdata[''],
                    ISNo : cridata['WebServiceCriteriaSlNo'],
                    Ilabel: cridata['WebServiceCriteriaDisplayName'],
                    IIsHeader: webdata['WebServiceSettingDetailIsHeader'],
                    IIsCompulsory: webdata['WebServiceSettingDetailIsCompulsory'],
                    ICriteriaAttributeId: webdata['CriteriaAttributeId'],
                    ICriteriaAttributeType: webdata['CriteriaAttributeType'],
                    selecttype: cridata['CriteriaAttributeTypeName'],
                    picklisturl: cridata['CriteriaAttributePicklistURI'],
                    Idisplaytext: cridata['CriteriaAttributePicklistDisplayFieldName'],
                    Isearchfields: cridata['CriteriaAttributeSearchFields'],
                    IPicklistTitle:cridata['CriteriaAttributePicklistTitle'],
                    IPicklistSelection:cridata['CriteriaAttributePicklistSelectionFieldName'],
                    listvalues: cridata['CriteriaAttributeApplicableValues'].split(','),
                    Idependpicklist: dependicklistvalue,
                    Idefaultvalue: defaultvalue
                  })
                }
              }
            }
          }
          finalizedArray.sort((a, b) => (a.ISNo > b.ISNo) ? 1 : -1)
          this.filterdata = finalizedArray;
          if(this.configdata[0].CriteriaConfigArray.length >= 1){
            for(let onedata of this.filterdata){
              if(onedata.Idefaultvalue == "" || onedata.selecttype == "LIST"){
                  for(let cridat of  this.configdata[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute){
                      if(cridat.CriteriaAttributeId == onedata.ICriteriaAttributeId){
                        // onedata.Idefaultvalue = cridat.CriteriaConfigAttributeFieldValue
                        if(onedata.selecttype == "PICKLIST"){
                          onedata.Idefaultvalue = cridat.CriteriaConfigAttributeFieldValue
                        }
                        if(onedata.selecttype == "LIST"){
                            onedata.Idefaultvalue = cridat.CriteriaConfigAttributeFieldValue
                        }
                      }
                  }
              }
              // else{
              //     console.log(onedata)
              // }
          }
          }
          this.form.get('AttributesCriteriaList')?.reset();
          for (var data of this.filterdata) {
            if (data) {
              if (data.selecttype == "DATE") {
                this.Datevalue = "DATE"
              }
              if(data.Ilabel == "FromDate"){
                var datePipe = new DatePipe("en-US");
                this.Fromdate  = datePipe.transform( data.Idefaultvalue,'yyyy-MM-dd');
              }
              else if(data.Ilabel == "ToDate"){
                var datePipe = new DatePipe("en-US");
                this.Todate  = datePipe.transform( data.Idefaultvalue,'yyyy-MM-dd');
              }
              var selectyperemove = data.selecttype
              const add = this.form.get('AttributesCriteriaList') as FormArray;
              add.push(this.fb.group({
                FieldName: [data.IFieldName],
                OperationType: [data.IOperationType],
                FieldValue: [data.IFieldValue],
                JoinType: [],
                CriteriaAttributeName: [data.Ilabel],
                CriteriaAttributeValue: [],
                IsHeader: [data.IIsHeader],
                IsCompulsory: [data.IIsCompulsory],
                CriteriaAttributeId: [data.ICriteriaAttributeId],
                CriteriaAttributeType: [data.ICriteriaAttributeType],
                listvalueremove: [data.listvalues],
                dependpicklist: [data.Idependpicklist],
                defaultvalue: [data.Idefaultvalue],
                picklisturlremove: [data.picklisturl],
                displaytext:[data.Idisplaytext],
                searchfieldsremove: [data.Isearchfields],
                picklisttitle: [data.IPicklistTitle],
                picklistselection: [data.IPicklistSelection],
                selectyperemove
              }))
            }
          }
          // this.store.dispatch(new Entitylookupdependpicklist(this.form.value));             // should hide on test
  }

  public btnfilter() {
    // document.getElementById("myModal").style.display = "block";
    const myModal = document.getElementById("myModal");
    if (myModal !== null) {
      myModal.style.display = "block";
    }
  }
// 
  public Getselectedid(SelectedPicklistData:any): void {
    const attributesCriteriaListsvalues = this.form.get('AttributesCriteriaList');

    let seldata = JSON.parse(SelectedPicklistData)
    if(attributesCriteriaListsvalues){
      for(let data of attributesCriteriaListsvalues.value){
        if(data.FieldName == seldata[0].Picklist){
        }
      }
    }

  }

  public apply() {
    // console.log("Apply")
    let checkpasssed;
    let fullvalue = [];
    const attributesCriteriaListsvalues = this.form.get('AttributesCriteriaList');
    if(attributesCriteriaListsvalues){
      for (let datas of attributesCriteriaListsvalues.value){
        if(datas.selectyperemove == "PICKLIST"){
          if(datas.IsCompulsory == 0 && (datas.CriteriaAttributeValue.lookupdata == null || datas.CriteriaAttributeValue.lookupdata == '')){
            checkpasssed = "Showerror";
            let error = "Please fill "+datas.CriteriaAttributeName;
            fullvalue.push(error);
          }
        }
        else{
          if(datas.IsCompulsory == 0 && (datas.CriteriaAttributeValue == null || datas.CriteriaAttributeValue == '')){
            checkpasssed == "Showerror"
            let error = "Please fill "+datas.CriteriaAttributeName;
            fullvalue.push(error);
          }
        }
      }
    }



    if(checkpasssed != "Showerror"){
      // document.getElementById("myModal").style.display = "none";
      const myModal = document.getElementById("myModal");

      if (myModal !== null) {
        myModal.style.display = "none";
      }

      let checkvalue = "";
      this.criteria = this.fb.group({
        AttributesCriteriaList: this.fb.array([])
      }) as FormGroup;
  
      let criteriavalue;
      const attributesCriteriaListsvalue = this.form.get('AttributesCriteriaList');
      if(attributesCriteriaListsvalue){
        for (let data of attributesCriteriaListsvalue.value) {
          if (data.CriteriaAttributeValue != null && data.CriteriaAttributeValue != undefined) {
            let CriteriaAttributeValue :any;
            criteriavalue = data.CriteriaAttributeValue
            const json = this.criteria.get('AttributesCriteriaList') as FormArray;
            if (data.selectyperemove == "PICKLIST" && data.CriteriaAttributeValue.lookupdata != undefined) {
              // console.log("vvvv",data.CriteriaAttributeValue.lookupdata.length)
              let value = '';
              // let xx = [data.CriteriaAttributeValue.lookupdata]
              if (data.CriteriaAttributeValue.lookupdata.length > 1) {
                let final = []
                // let Value = data.CriteriaAttributeValue.lookupdata
                // let Keys = Object.keys(Value)
                let ids = "";
                let codes ="";
                // console.log("PICKLIST DATA:",data.CriteriaAttributeValue.lookupdata)
                for(let item of data.CriteriaAttributeValue.lookupdata){
                  ids = ids + item.Id + ","
                  codes = codes + item.Code + ","
                }
                ids = ids.slice(0, -1);
                codes = codes.slice(0, -1);
                final.push({ "Id": ids, "Code": codes })
                CriteriaAttributeValue = final[0];
                // console.log("PICKLISTSELECTEDVALUE",CriteriaAttributeValue)
              }
              else if(data.CriteriaAttributeValue.lookupdata.length > 0){
                value = data.CriteriaAttributeValue.lookupdata;                                     //for testing (data.CriteriaAttributeValue) // for deveops (data.CriteriaAttributeValue.lookupdata )
                CriteriaAttributeValue = value[0];
                // console.log("PICKLISTSELECTEDVALUE2",CriteriaAttributeValue)
              }
              else{
                // console.log("PICKLISTSELECTEDVALUE2",CriteriaAttributeValue)
                data.CriteriaAttributeValue = null;
              }
            }
            else if (data.selectyperemove == "LIST") {
              // console.log("LIST",data.CriteriaAttributeValue.length)
              if(typeof data.CriteriaAttributeValue == 'string'){
                let valuelist = data.CriteriaAttributeValue.split(',')
                let finallistvalue = [];
                if(valuelist.length>1){
                  finallistvalue.push({ "Id": valuelist[1], "Code": valuelist[0]})
                  CriteriaAttributeValue = finallistvalue[0];
                }
              }
            }
            else if (data.selectyperemove == "DATE") {
              var datePipe = new DatePipe("en-US");
              data.CriteriaAttributeValue = datePipe.transform(data.CriteriaAttributeValue, 'dd/MMM/yyyy');
              var timestampdate = Date.parse(data.CriteriaAttributeValue);
              let finaltimestampdate = timestampdate/1000;
    
              let finallistdate = [];
              finallistdate.push({ "Id": finaltimestampdate, "Code": data.CriteriaAttributeValue})
              CriteriaAttributeValue = finallistdate[0];
              // console.log("DateSELECTEDVALUE",CriteriaAttributeValue)
            }
            else{
              data.CriteriaAttributeValue = null;
            }
  
            if(data.CriteriaAttributeValue != null){
              json.push(this.fb.group({
                FieldName: [data.FieldName],
                OperationType: [data.OperationType],
                FieldValue: [CriteriaAttributeValue.Id],
                JoinType: [0],
                CriteriaAttributeName: [data.WebServiceCriteriaDisplayName],
                CriteriaAttributeValue: [CriteriaAttributeValue.Code],
                IsHeader: [data.IsHeader],
                IsCompulsory: [data.IsCompulsory],
                CriteriaAttributeId: [data.CriteriaAttributeId],
                CriteriaAttributeType: [data.CriteriaAttributeType],
              }))
            }
            checkvalue = "Valuepassed"
          }
          else {
            // console.log("checkvalue", checkvalue)
          }
        }
      }

  
      if (criteriavalue != null) {
        const json = this.criteria.get('AttributesCriteriaList') as FormArray;
        for (let values of this.inivisbledataarray) {
          if (values.WebServiceSettingDetailIsVisible == 1 && values.WebServiceSettingDetailCriteriaFieldValue != "NONE") {
            json.push(this.fb.group({
              FieldName: [values.WebServiceCriteriaFieldName],
              OperationType: [values.WebServiceSettingDetailOperationType],
              FieldValue: [values.WebServiceSettingDetailCriteriaFieldValue],
              JoinType: "2",
              CriteriaAttributeName: [values.WebServiceSettingDetailDisplayName],
              CriteriaAttributeValue: [values.WebServiceSettingDetailCriteriaFieldValue],
              IsHeader: [values.WebServiceSettingDetailIsHeader],
              IsCompulsory: [values.WebServiceSettingDetailIsCompulsory],
              CriteriaAttributeId: [values.CriteriaAttributeId],
              CriteriaAttributeType: [values.CriteriaAttributeType],
            }))
          }
        }
      }
      if(this.configdata[0].BizTransactionClassId != -1){
        const json = this.criteria.get('AttributesCriteriaList') as FormArray;
        json.push(this.fb.group({
          FieldName: this.Biztransactionclass.WebServiceCriteriaFieldName,
          OperationType: this.Biztransactionclass.WebServiceSettingDetailOperationType,
          FieldValue: this.configdata[0].BizTransactionClassId,
          JoinType: this.Biztransactionclass.WebServiceSettingDetailCriteriaJoin,
          CriteriaAttributeName: null,
          CriteriaAttributeValue: "ABC ORGANIZATION",
          IsHeader: this.Biztransactionclass.WebServiceSettingDetailIsHeader,
          IsCompulsory: this.Biztransactionclass.WebServiceSettingDetailIsCompulsory,
          CriteriaAttributeId: this.Biztransactionclass.CriteriaAttributeId,
          CriteriaAttributeType: this.Biztransactionclass.CriteriaAttributeType,
      }))
      }
      const attributesCriteriaLists = this.criteria.get('AttributesCriteriaList');
      if (attributesCriteriaLists){
      const AttributesCriteriaList = attributesCriteriaLists.value;
      let criteriadata = {
        SectionCriteriaList: [
          {
            "SectionId": 0,
            AttributesCriteriaList,
            "OperationType": 0
          }
        ]
      }
      
      if (checkvalue) {
        this.store.dispatch(new GbfilterDTO(criteriadata));             // should hide on test
      }
      }

 
      // const AttributesCriteriaList = this.criteria.get('AttributesCriteriaList').value;
  
      // this.testid = criteriajsondata.AttributesCriteriaListtest;
      // this.testidcode = criteriajsondata.AttributesCriteriaList
      return this.criteria.value;
    }
    else{
      // const dialogRef = this.dialog.open(DialogBoxComponent, {
      //   data: {
      //     message: fullvalue,
      //     heading: 'Info',
      //     // button:'Ok'
      //     // Add any other properties you want to pass
      //   }
      // });
      // alert(fullvalue)
    }
  }


  public SaveConfigration(nameddata:any) {

    this.criteria = this.fb.group({
      AttributesCriteriaList: this.fb.array([])
    }) as unknown as GBBaseFormGroup;

    const attributesCriteriaLists = this.criteria.get('AttributesCriteriaList');
    if (attributesCriteriaLists) {
      for (let data of attributesCriteriaLists.value) {
        if (data.CriteriaAttributeValue != null) {                                       //for testing (data.CriteriaAttributeValue == null) // for deveops (data.CriteriaAttributeValue != null)
          const json = this.criteria.get('AttributesCriteriaList') as FormArray;
          if (data.selectyperemove == "PICKLIST") {
            let value = '';
            if (data.CriteriaAttributeValue.lookupdata.length > 1) {
              let final = []
              final.push({ "Id": data.CriteriaAttributeValue.lookupdata[0].Id + "," + data.CriteriaAttributeValue.lookupdata[1].Id, "Code": data.CriteriaAttributeValue.lookupdata[0].Code + "," + data.CriteriaAttributeValue.lookupdata[1].Code })
              //for testing (data.CriteriaAttributeValue) // for deveops (data.CriteriaAttributeValue.lookupdata )            //for testing (data.CriteriaAttributeValue) // for deveops (data.CriteriaAttributeValue.lookupdata )
              data.CriteriaAttributeValue = final[0];
            }
            else {
              value = data.CriteriaAttributeValue.lookupdata;                                     //for testing (data.CriteriaAttributeValue) // for deveops (data.CriteriaAttributeValue.lookupdata )
              data.CriteriaAttributeValue = value[0];
            }
          }
          if (data.selectyperemove == "LIST") {
            data.CriteriaAttributeValue = data.CriteriaAttributeValue;
          }
          if (data.selectyperemove == "DATE") {
            var datePipe = new DatePipe("en-US");
            data.CriteriaAttributeValue = datePipe.transform(data.CriteriaAttributeValue, 'dd/MMM/yyyy');
          }
          json.push(this.fb.group({
            FieldName: [data.FieldName],
            OperationType: [data.OperationType],
            fieldvalueC: [data.FieldValue],
            FieldValue: [data.CriteriaAttributeValue.Id],
            JoinType: [0],
            CriteriaAttributeName: [data.WebServiceCriteriaDisplayName],
            CriteriaAttributeValue: [data.CriteriaAttributeValue.Code],
            IsHeader: [data.IsHeader],
            IsCompulsory: [data.IsCompulsory],
            CriteriaAttributeId: [data.CriteriaAttributeId],
            CriteriaAttributeType: [data.CriteriaAttributeType],
          }))
        }
      }
    }
    const attributesCriteriaList = this.criteria.get('AttributesCriteriaList');
    let AttributesCriteriaList:any
    if (attributesCriteriaList) {
       AttributesCriteriaList = attributesCriteriaList.value;
    }   
    // const AttributesCriteriaList = this.criteria.get('AttributesCriteriaList').value;
    let test =
    {
      "CriteriaConfigId": 0,
      "CriteriaConfigCode": nameddata,
      "CriteriaConfigName": nameddata,
      "CriteriaConfigCriteriaServiceId": this.configdata[0].WebServiceId,
      "CriteriaConfigCriteriaConfigType": 0,
      "CriteriaConfigCriteriaUserId": -1499998941,
      "CriteriaConfigCriteriaMenuId": this.configdata[0].MenuId,
      "CriteriaConfigVersion": 0,
      "CriteriaConfigSectionArray": [
        {
          "CriteriaConfigSectionId": 0,
          "CriteriaConfigSectionSlNo": 1,
          "CriteriaConfigSectionSectionJoin": 0,
          "CriteriaConfigAttributeArray": [
            {
              "CriteriaConfigAttributeSlNo": 1,
              "CriteriaAttributeId": AttributesCriteriaList[0].CriteriaAttributeId,
              "CriteriaAttributeName": AttributesCriteriaList[0].FieldName,
              "CriteriaConfigAttributeOperationType": AttributesCriteriaList[0].OperationType,
              "CriteriaConfigAttributeFieldValue": AttributesCriteriaList[0].FieldValue,
              "CriteriaConfigAttributeJoin": AttributesCriteriaList[0].JoinType,
              "CriteriaConfigAttributeFieldDisplayName": AttributesCriteriaList[0].WebServiceCriteriaDisplayName,
              "CriteriaConfigAttributeFieldDisplayValue": AttributesCriteriaList[0].CriteriaAttributeValue,
              "CriteriaAttributeType": AttributesCriteriaList[0].CriteriaAttributeType,
              "CriteriaConfigAttributeFieldIsHeader": AttributesCriteriaList[0].IsHeader,
              "CriteriaConfigAttributeFieldIsMandatory": AttributesCriteriaList[0].IsCompulsory,
              "CriteriaConfigAttributeCriteriaFieldValue": AttributesCriteriaList[0].FieldValue,
              "CriteriaConfigAttributeCriteriaFieldDisplayValue": AttributesCriteriaList[0].CriteriaAttributeValue
            }
          ]
        }
      ],
      "CriteriaConfigPeriodFilter": 99,
      "CriteriaConfigRecordsPerPage": "0",
      "CriteriaConfigCriteriaOUId": AttributesCriteriaList[0].fieldvalueC,
      "ReportFormatId": this.configdata[0].DefaultReportFormatId
    }
    this.dataservice.configservice(test).subscribe(res => {
    });
  }



  public close() {
    // document.getElementById("myModal").style.display = "none";
    const dateform = document.getElementById("myModal");
    if (dateform !== null) {
      dateform.style.display = "none";
    }
  }

  public Date() {
    this.isactive=1;
    // document.getElementById("dateform").style.display = "block";
    // document.getElementById("filterform").style.display = "none";
    // document.getElementById("Configrationform").style.display = "none";
    // document.getElementById("Reportform").style.display = "none";
    const dateform = document.getElementById("dateform");
    if (dateform !== null) {
      dateform.style.display = "block";
    }
    const Reportform = document.getElementById("Reportform");
    if (Reportform !== null) {
      Reportform.style.display = "none";
    }
    const filterForm = document.getElementById("filterform");
    if (filterForm !== null) {
      filterForm.style.display = "none";
    }
    const Configrationform = document.getElementById("Configrationform");
    if (Configrationform !== null) {
      Configrationform.style.display = "none";
    }
  }

  public radiobtnclicked(data:any) {
    for (let data of this.filterdata) {
      if (data.IFieldName == "PeriodFromDate") {
        this.WorkPeriodFromDate = "/Date(1617215400000)/"
      }
      if (data.IFieldName == "PeriodToDate") {
        this.WorkPeriodToDate = "/Date(1680201000000)/"
      }
      // this.today = datePipe.transform(JSON.parse(ss.replace("/Date(","").replace(")/","")),'dd/MMM/yyyy');
    }
    var datePipe = new DatePipe("en-US");
    if (this.readioSelected == "DTD") {
      var d = new Date();
      this.Fromdate = datePipe.transform(d, 'yyyy-MM-dd');
      this.Todate = datePipe.transform(d, 'yyyy-MM-dd');
    } else if (this.readioSelected == "WTD") {
      var curr = new Date;
      var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      var lastday = new Date();
      this.Fromdate = datePipe.transform(firstday, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastday, 'yyyy-MM-dd')
    }
    else if (this.readioSelected == "PreviousWeek") {
      var currpreviousweek = new Date;
      var firstdayPW = new Date(currpreviousweek.setDate((currpreviousweek.getDate() - currpreviousweek.getDay() - 6)));
      //var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
      var currpreviousweeklastday = new Date;
      var lastdayPW = currpreviousweeklastday.setDate(currpreviousweeklastday.getDate() - currpreviousweeklastday.getDay());
      this.Fromdate = datePipe.transform(firstdayPW, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastdayPW, 'yyyy-MM-dd')
    } else if (this.readioSelected == "MTD") {
      var today = new Date();
      var t = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var temp = today.getDate() - t.getDate();
      var first = new Date(today.getFullYear(), today.getMonth(), 1);
      var last = new Date(today.getFullYear(), today.getMonth() + 1, temp);
      this.Fromdate = datePipe.transform(first, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(last, 'yyyy-MM-dd')
    } else if (this.readioSelected == "YTD") {
      var today = new Date();
      var tech = localStorage.getItem("userDetails");
      var temp1 = this.WorkPeriodFromDate;
            // temp1 = datePipe.transform(temp1, "yyyy-MM-dd");

      temp1 = datePipe.transform(
        JSON.parse(temp1.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      var t = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var temp = today.getDate() - t.getDate();
      //var last = new Date(today.getFullYear(), today.getMonth() + 1, temp);
      var last = new Date();
      var FirstDayOfMonth = new Date(temp1.split("-")[2], temp1.split("-")[1] + 3, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(last, 'yyyy-MM-dd')
    } else if (this.readioSelected == "CurrentYear") {
      var today = new Date();
      let t = localStorage.getItem("userDetails");
      var temp1 = this.WorkPeriodFromDate;
      var temp2 = this.WorkPeriodToDate;
      // temp1 = datePipe.transform(temp1, "yyyy-MM-dd");
      temp1 = datePipe.transform(
        JSON.parse(temp1.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      temp2 = datePipe.transform(
        JSON.parse(temp2.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      // temp2 = datePipe.transform(temp2, "yyyy-MM-dd");
      var lastDayOfMonth = new Date(temp2.split("-")[2], temp2.split("-")[1]+3, 0);
      var FirstDayOfMonth = new Date(temp1.split("-")[2], temp1.split("-")[1] + 3, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "CurrentMonth") {
      var today = new Date();
      var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var FirstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "Yesterday") {
      var today = new Date();
      var FirstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(today, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYearDTD") {
      var today = new Date();
      var da = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
      this.Fromdate = datePipe.transform(da, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(today, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYearWTD") {
      var curr = new Date();
      var firstday = new Date(curr.getFullYear() - 1, curr.getMonth(), 1);
      firstday = new Date(firstday.setDate(firstday.getDate() - firstday.getDay()));
      var lastday = new Date();
      var firstdays = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      this.Fromdate = datePipe.transform(firstday, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(firstdays, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYearMTD") {
      var today = new Date();
      var t = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var temp = today.getDate() - t.getDate();
      var first = new Date(today.getFullYear() - 1, today.getMonth(), 1);
      var last = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
      this.Fromdate = datePipe.transform(first, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(last, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYearYTD") {
      var today = new Date();
      var tech = localStorage.getItem("userDetails");
      var temp1 = this.WorkPeriodFromDate;
      temp1 = datePipe.transform(
        JSON.parse(temp1.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      var t = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var temp = today.getDate() - t.getDate();
      var last = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
      var FirstDayOfMonth = new Date(temp1.split("-")[2] -1, temp1.split("-")[1] + 3, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(last, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYear") {
      var today = new Date();
      let t = localStorage.getItem("userDetails");
      var temp1 = this.WorkPeriodFromDate;
      var temp2 = this.WorkPeriodToDate;
      temp1 = datePipe.transform(
        JSON.parse(temp1.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      temp2 = datePipe.transform(
        JSON.parse(temp2.replace('/Date(', '').replace(')/', '')),
        'dd-mm-yyyy'
      );
      var lastDayOfMonth = new Date(temp2.split("-")[2], temp2.split("-")[1] +3, 0);
      var FirstDayOfMonth = new Date(temp1.split("-")[2] -1, temp1.split("-")[1] + 3, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousMonth") {
      var today = new Date();
      var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 0);
      var FirstDayOfMonth = new Date(today.getFullYear(), today.getMonth() - 1, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "PreviousYearMonth") {
      var today = new Date();
      var lastDayOfMonth = new Date(today.getFullYear() - 1, today.getMonth() + 1, 0);
      var FirstDayOfMonth = new Date(today.getFullYear() - 1, today.getMonth(), 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "Others") {
      var today = new Date();
      var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      var FirstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    } else if (this.readioSelected == "All") {
      var today = new Date();
      var lastDayOfMonth = new Date(9999, 12, 0);
      var FirstDayOfMonth = new Date(1899, 0, 1);
      this.Fromdate = datePipe.transform(FirstDayOfMonth, 'yyyy-MM-dd')
      this.Todate = datePipe.transform(lastDayOfMonth, 'yyyy-MM-dd')
    }
  }

  public selectedreportformat(selectedata:any){
    this.store.dispatch(new GbReportformatselection(selectedata));             // should hide on test

  }

  public Filter() {
    this.isactive=2;
    // document.getElementById("dateform").style.display = "none";
    // document.getElementById("filterform").style.display = "block";
    // document.getElementById("Configrationform").style.display = "none";
    // document.getElementById("Reportform").style.display = "none";
    const dateform = document.getElementById("dateform");
    if (dateform !== null) {
      dateform.style.display = "none";
    }
    const Reportform = document.getElementById("Reportform");
    if (Reportform !== null) {
      Reportform.style.display = "none";
    }
    const filterForm = document.getElementById("filterform");
    if (filterForm !== null) {
      filterForm.style.display = "block";
    }
    const Configrationform = document.getElementById("Configrationform");
    if (Configrationform !== null) {
      Configrationform.style.display = "none";
    }
  }


  public Configration() {
    this.isactive=4;
    // document.getElementById("dateform").style.display = "none";
    // document.getElementById("Reportform").style.display = "none";
    // document.getElementById("filterform").style.display = "none";
    // document.getElementById("Configrationform").style.display = "block";
    const dateform = document.getElementById("dateform");
    if (dateform !== null) {
      dateform.style.display = "none";
    }
    const Reportform = document.getElementById("Reportform");
    if (Reportform !== null) {
      Reportform.style.display = "none";
    }
    const filterForm = document.getElementById("filterform");
    if (filterForm !== null) {
      filterForm.style.display = "none";
    }
    const Configrationform = document.getElementById("Configrationform");
    if (Configrationform !== null) {
      Configrationform.style.display = "block";
    }
  }

  public Report() {
    this.isactive=3
    // document.getElementById("dateform").style.display = "none";
    // document.getElementById("Reportform").style.display = "block";
    // document.getElementById("filterform").style.display = "none";
    // document.getElementById("Configrationform").style.display = "none";
    const dateform = document.getElementById("dateform");
    if (dateform !== null) {
      dateform.style.display = "none";
    }
    const Reportform = document.getElementById("Reportform");
    if (Reportform !== null) {
      Reportform.style.display = "block";
    }
    const filterForm = document.getElementById("filterform");
    if (filterForm !== null) {
      filterForm.style.display = "none";
    }
    const Configrationform = document.getElementById("Configrationform");
    if (Configrationform !== null) {
      Configrationform.style.display = "none";
    }
  }

  public changeCheckevent(event:any) {
    this.Advancefilter = event.target.checked;
  }

  public selecteddata(data:any) {
    this.id = JSON.stringify(data);
  }
}



  // thisConstructor() {
  //   this.form = this.gbps.fb.group({
  //     AttributesCriteriaList: this.gbps.fb.array([])
  //   }) as GBBaseFormGroup;
  //   // this.form = this.gbps.fb.group({
  //   //   Entitylookup: ['', Validators.required],
  //   //   combobox: ['', Validators.required],
  //   // }) as GBBaseFormGroup;
  //   // this.form = this.gbps.fb.FormControl()
  // }


      // let group={}    
      // this.filterdata.forEach(input_template=>{
      //   group[input_template.label]=new FormControl('');  
      // })
      // this.form = new GBBaseFormGroup(group);