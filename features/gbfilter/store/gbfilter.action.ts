import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Filter';

export class GbfilterDTO {
  static readonly type = '[' + storeSliceName + '] GbfilterDTO';
  constructor(public DFilter:any) {
  }
}

export class GbReportformatselection {
  static readonly type = '[' + storeSliceName + '] GbReportformatselection';
  constructor(public DReportformatselection:any) {
  }
}

