import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Inject, Injectable } from '@angular/core';
import { GbfilterDTO , GbReportformatselection} from './gbfilter.action';

export class FilterStateModel {
  filterdata:any;
  reportformatselection:any;
}

@State<FilterStateModel>({
  name: 'Filter',
  defaults: {
    filterdata: null,
    reportformatselection: null,
  },
})
@Injectable()
export class FilterState {
  @Selector() static Filter(state: FilterStateModel) {
    return state.filterdata;
  }

  @Selector() static Reportformatselection(state: FilterStateModel) {
    return state.reportformatselection;
  }


  @Action(GbfilterDTO)
  GbfilterDTO(context: StateContext<FilterStateModel>, Ifilter: GbfilterDTO) {
    const state = context.getState();
    context.setState({ ...state, filterdata: Ifilter.DFilter });
  }

  @Action(GbReportformatselection)
  GbReportformatselection(context: StateContext<FilterStateModel>, Ifilter: GbReportformatselection) {
    const state = context.getState();
    context.setState({ ...state, reportformatselection: Ifilter.DReportformatselection });
  }
}