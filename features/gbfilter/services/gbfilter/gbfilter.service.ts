import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { GbFilterdbservice } from './../../dbservices/gbfilter/gbfilterdb.service';
import { ReportCriteriaArray } from './../../model/IFilter';

@Injectable({
  providedIn: 'root',
})
export class GbFilterService {

  constructor(public Gbfilterdbservice: GbFilterdbservice) { }

  public getfilterservice(id:any) {
    return this.Gbfilterdbservice.FilterDbServices(id);
  }

  public configservice(criteria:any) {
    return this.Gbfilterdbservice.configration(criteria);
  }

  public reportformatservices(reportid :any,menuid:any){
    return this.Gbfilterdbservice.reportformatdbservice(reportid ,menuid);
  }

}
