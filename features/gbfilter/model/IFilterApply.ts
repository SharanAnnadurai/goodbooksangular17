export class AttributesCriteriaList {
    FieldName: string;
    OperationType: any;
    FieldValue: string;
    JoinType: any;
    CriteriaAttributeName: string;
    CriteriaAttributeValue: string;
    IsHeader: any;
    IsCompulsory: any;
    CriteriaAttributeId: any;
    CriteriaAttributeType: number;
}

export class SectionCriteriaList {
    SectionId: number;
    AttributesCriteriaList: AttributesCriteriaList[];
    OperationType: number;
}

export class RootObject {
    SectionCriteriaList: SectionCriteriaList[];
}