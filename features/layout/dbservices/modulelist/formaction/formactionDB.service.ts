import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

@Injectable({
    providedIn: 'root',
  })

  export class formactiondbservice {
    constructor(@Inject(GBHttpService) public http: GBHttpService) { }
    public formactionService(url:any,criteria:any) : Observable<any> {
      return this.http.httppost(url,criteria);
    }
  
  }
  