import { Injectable } from '@angular/core';
import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

@Injectable({
    providedIn: 'root',
})
export class Configdbservice {
    constructor(public http: GBHttpService) { }
    public Configdata(postdata:any, data:any) {

        let url;
        let fullurl = 'http://169.56.148.10:82/gb4/fws/CriteriaConfig.svc/SelectList/?FirstNumber=1&MaxResult=50';
        let str =  fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        return this.http.httppost(url, postdata);
    }
}
