import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { MatCommonModule } from '@angular/material/';
import { NgxsModule } from '@ngxs/store';
import {GbfilterModule} from './../gbfilter/gbfilter.module';
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatIconModule } from '@angular/material/icon'
import { MatFormFieldModule } from '@angular/material/form-field'

import {PreloadAllModules, RouterModule} from '@angular/router';
import { NavComponent } from './components/nav/nav.component';
import { AsideComponent } from './components/aside/aside.component';
import { LoHgComponent } from './layouts/lo-hg/lo-hg.component';
import { LoBcComponent } from './layouts/lo-bc/lo-bc.component';
import { LoGbComponent } from './layouts/lo-gb/lo-gb.component';
import {MatBadgeModule} from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule} from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import { MatToolbarModule} from '@angular/material/toolbar';
import {MatDialogModule} from '@angular/material/dialog'
import {MatDividerModule} from '@angular/material/divider';
import {MatSelectModule} from '@angular/material/select';
import { LayoutState } from './store/layout.state';
import { MatInputModule } from '@angular/material/input';

import { GbhelpComponent} from './components/gbappfooter/gbhelp/gbhelp.component';
import { GbsaveComponent } from './components/gbappfooter/gbsave/gbsave.component';
import { GbstatusComponent } from './components/gbappfooter/gbstatus/gbstatus.component';
import { GbhbmenuComponent} from './components/gbappheader/gbhbmenu//gbhbmenu.component';
import { GbclientComponent} from './components/gbappheader/gbclient/gbclient.component';
import { GblogoComponent } from './components/gbappheader/gblogo/gblogo.component';
import { GbmodulelistComponent } from './components/gbappheader/gbmodulelist/gbmodulelist.component';
import { GbnotificationComponent } from './components/gbappheader/gbnotification/gbnotification.component';
import {  GbsearchComponent } from './components/gbappheader/gbsearch/gbsearch.component';
import {  GbSettingsComponent } from './components/gbappheader/gbsettings/gbsettings.component';
import { GbThemeComponent } from './components/gbappheader/gbsettings/gbtheme/gbtheme.component';
import { GbAppMenuComponent } from './components/gbappheader/gbappmenu/gbappmenu.component';
import {  GbusersettingComponent } from './components/gbappheader/gbuser/gbusersetting/gbusersetting.component';
import { GbappHeaderComponent} from './components/gbappheader/gbappheader.component';
import { GBAppFooterComponent} from './components/gbappfooter/gbappfooter.component';
import { GbappsidebarComponent } from './components/gbappsidebar/gbappsidebar.component';
import { GbmaincontentComponent } from './components/gbmaincontent/gbmaincontent.component';
import { GbformheaderComponent } from './components/gbmaincontent/gbformheader/gbformheader.component';
import { GbformtoolbarComponent } from './components/gbmaincontent/gbformtoolbar/gbformtoolbar.component';
import { GbformactionsComponent } from './components/gbmaincontent/gbformheader/gbformactions/gbformactions.component';
import { GbformselectorComponent} from './components/gbmaincontent/gbformheader/gbformselector/gbformselector.component';
import { GbformstatusComponent} from './components/gbmaincontent/gbformheader/gbformstatus/gbformstatus.component';
import { GbformtitleComponent} from './components/gbmaincontent/gbformheader/gbformtitle/gbformtitle.component';
import { GbformdataactionsComponent } from './components/gbmaincontent/gbformtoolbar/gbformdataactions/gbformdataactions.component';
import { GbformdatarecentComponent } from './components/gbmaincontent/gbformtoolbar/gbformdatarecent/gbformdatarecent.component';
import { GbformdatatitleComponent } from './components/gbmaincontent/gbformtoolbar/gbformdatatitle/gbformdatatitle.component';
import { GbformdatatoolbarComponent} from './components/gbmaincontent/gbformtoolbar/gbformdatatoolbar/gbformdatatoolbar.component';
import {CommonReportModule} from './../commonreport/commonreport.module';
import { ThemesModule } from '../themes/themes.module';
import { GBMenutreeModule } from './components/gbappsidebar/gbmenutree/gbmenutree.module';
// import { SamplescreensModule } from 'features/erpmodules/samplescreens/samplescreens.module';

@NgModule({
  declarations: [
    NavComponent,
    AsideComponent,
    LoHgComponent,
    LoBcComponent,
    LoGbComponent,
    GbhelpComponent,
    GbsaveComponent,
    GbstatusComponent,
    GbhbmenuComponent,
    GbnotificationComponent,
    GbmodulelistComponent,
    GblogoComponent,
    GbsearchComponent,
    GbSettingsComponent,
    GbAppMenuComponent,
    GbThemeComponent,
    GBAppFooterComponent,
    GbappsidebarComponent,
    GbmaincontentComponent,
    GbformheaderComponent,
    GbformtoolbarComponent,
    GbformselectorComponent,
    GbformselectorComponent,
    GbformtitleComponent,
    GbformstatusComponent,
    GbformdataactionsComponent,
    GbformdatarecentComponent,
    GbformdatatitleComponent,
    GbformdatatoolbarComponent,
    GbformactionsComponent,
    GbclientComponent,
    GbusersettingComponent,
    GbappHeaderComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([]),
    NgxsModule.forFeature([LayoutState]),
    CommonReportModule,
    GBMenutreeModule,
    GbfilterModule,
    ThemesModule,
    MatSidenavModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatButtonModule,
    MatTabsModule,
    MatMenuModule,
    MatToolbarModule,
    MatSelectModule,
    MatBadgeModule,
    MatDividerModule,
    MatDialogModule,


    // SamplescreensModule
  ],
  providers: [],
  bootstrap: [],
  exports: [
    LoHgComponent,
    LoBcComponent,
    LoGbComponent,
    GbhelpComponent,
    GbsaveComponent,
    GbstatusComponent,
    GBAppFooterComponent
  ]
})
export class GBLayoutModule { }
