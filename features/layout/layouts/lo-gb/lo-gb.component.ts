import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { LayoutState } from '../../store/layout.state';
import { OverlayContainer } from '@angular/cdk/overlay';
// @Component({selector: 'temp', template: 'test'})
// export class temp {}
import { ThemeColor } from './../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/ThemeColor';
import { Selectcolors } from './../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/SelectedColor';
import { thememonsecondarycolor } from './../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/Theme-Mon-Secondary-Color';
// const themecolors = require('./../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/ThemeColor.json')
// const selectcolors = require('./../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/SelectedColor.json')
// const secondarycolors = require('./../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/Theme-Mon-Secondary-Color.json')
@Component({
  selector: 'app-lo-gb',
  templateUrl: './lo-gb.component.html',
  styleUrls: ['./lo-gb.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class LoGbComponent implements OnInit {
  themeColor: 'primary' | 'accent' | 'warn' = 'primary';
  @Select(LayoutState.CurrentTheme) currenttheme: any;
  // temp=temp;
  showFiller = false;
  screenHeight!: number;
  screenWidth!: number;
  // _leftsidebarOpen = true;
  // _leftsidebarPinned = true;
  // _rightsidebarOpen = true;
  // _rightsidebarPinned = true;

  constructor(private store: Store, private overlayContainer: OverlayContainer) {
    this.onResize();
  }

  // @ViewChild('rightsidebartemplate') public rightsidebartemplate: TemplateRef<any>;

  // @ViewChild('dynamicContainer', { read: ViewContainerRef }) private vcr: ViewContainerRef;
  // private componentRef: any;
  // createComponent() {
  //   console.log('clicked!');
  //   this.vcr.clear();
  //   this.componentRef = SideBarComponents.CurrentComponentRef(this.vcr);
  // }
  // destroyComponent() {
  //   this.componentRef.destroy();
  // }
  // currentRightSidebarComponent$(): ComponentRef<any> {
  //   return SideBarComponents.CurrentComponentRef(this.vcr);
  // }

  ngOnInit(): void {
    this.onThemeChange();
    this.currenttheme.subscribe( (theme:any) => {
        this.setStyle('--color', (ThemeColor as any)[theme]);
        this.setStyle('--selectcolor', (Selectcolors as any)[theme]);
        this.setStyle('--secondarycolor', (thememonsecondarycolor as any)[theme]);
    })
  }

  setStyle(name:any,value:any) {
    document.documentElement.style.setProperty(name, value);
  }

  private onResize(event?: Event) {
    this.screenHeight = window.innerHeight;
    this.screenWidth =  window.innerWidth;
    this.setStyle('--screenwidth', this.screenWidth - 30 + 'px');
    this.setStyle('--screenheight', this.screenHeight + 'px');
}
  
  onThemeChange() {
    let theme = '';
    this.themeClass$.subscribe(res => theme = res);
    const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
    const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('theme--'));
    if (themeClassesToRemove.length) {
       overlayContainerClasses.remove(...themeClassesToRemove);
    }
    theme.split(' ').forEach(x => {
      overlayContainerClasses.add(x);
    });
  }

  // @Select(LayoutState.MenuBarOpened) leftsidebarOpen$: Observable<boolean>;
  // @Select(LayoutState.MenuBarPinned) leftsidebarPinned$: Observable<boolean>;
  // @Select(LayoutState.ContextBarOpened) rightsidebarOpen$: Observable<boolean>;
  // @Select(LayoutState.ContextBarPinned) rightsidebarPinned$: Observable<boolean>;
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  // @Select(LayoutState.NeedBackdrop) needbackdrop$: Observable<string>;
  // toggleMenu(newValue: boolean) { if(newValue) this.store.dispatch(new OpenMenuBar); else this.store.dispatch(new CloseMenuBar);  }
  // toggleContext(newValue: boolean) { if(newValue) this.store.dispatch(new OpenContextBar); else this.store.dispatch(new CloseContextBar); }
  // toggleMenuPin(newValue: boolean) { if(newValue) this.store.dispatch(new PinMenuBar); else this.store.dispatch(new UnpinMenuBar); }
  // toggleContextPin(newValue: boolean) { if(newValue) this.store.dispatch(new PinContextBar); else this.store.dispatch(new UnpinContextBar);  }
  // changeTheme(newValue: string) { this.store.dispatch(new ChangeTheme(newValue)); }

}
