import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Layout';

export class ToggleMenuBar {
  static readonly type = '[' + storeSliceName + '] ToggleMenuBar';
  constructor() {  }
}
export class OpenMenuBar {
  static readonly type = '[' + storeSliceName + '] OpenMenuBar';
  constructor() {  }
}
export class CloseMenuBar {
  static readonly type = '[' + storeSliceName + '] CloseMenuBar';
  constructor() {  }
}
export class TogglePinMenuBar {
  static readonly type = '[' + storeSliceName + '] TogglePinMenuBar';
  constructor() {  }
}
export class PinMenuBar {
  static readonly type = '[' + storeSliceName + '] PinMenuBar';
  constructor() {  }
}
export class UnpinMenuBar {
  static readonly type = '[' + storeSliceName + '] UnpinMenuBar';
  constructor() {  }
}

export class ToggleContextBar {
  static readonly type = '[' + storeSliceName + '] ToggleContextBar';
  constructor() {  }
}
export class OpenContextBar {
  static readonly type = '[' + storeSliceName + '] OpenContextBar';
  constructor() {  }
}
export class CloseContextBar {
  static readonly type = '[' + storeSliceName + '] CloseContextBar';
  constructor() {  }
}
export class TogglePinContextBar {
  static readonly type = '[' + storeSliceName + '] TogglePinContextBar';
  constructor() {  }
}
export class PinContextBar {
  static readonly type = '[' + storeSliceName + '] PinContextBar';
  constructor() {  }
}
export class UnpinContextBar {
  static readonly type = '[' + storeSliceName + '] UnpinContextBar';
  constructor() {  }
}

export class ChangeTheme {
  static readonly type = '[' + storeSliceName + '] ChangeTheme';
  constructor(public themeName: string, public ref?: ChangeDetectorRef) {
  }
}

export class ChangeMode {
  static readonly type = '[' + storeSliceName + '] ChangeMode';
  constructor(public modeName: string, public ref?: ChangeDetectorRef) {
  }
}

export class ButtonVisibility {
  static readonly type = '[' + storeSliceName + '] ButtonVisibility';
  constructor(public visibilityname: string, public ref?: ChangeDetectorRef) {
  }
}


export class Title {
  static readonly type = '[' + storeSliceName + '] Title';
  constructor(public title: string, public ref?: ChangeDetectorRef) {
  }
}


export class Path {
  static readonly type = '[' + storeSliceName + '] Path';
  constructor(public path: string, public ref?: ChangeDetectorRef) {  }
}

export class Movenextpreviousid {
  static readonly type = '[' + storeSliceName + '] Movenextpreviousid';
  constructor(public movenextpreviousid: string, public ref?: ChangeDetectorRef) {  }
}

export class FormEditOption {
  static readonly type = '[' + storeSliceName + '] FormEditOption';
  constructor(public FormEdit:any) {
  }
}

export class FormEditabe {
  static readonly type = '[' + storeSliceName + '] FormEditabe';
  constructor() {  }
}
export class FormUnEditable {
  static readonly type = '[' + storeSliceName + '] FormUnEditable';
  constructor() {  }
}

export class VersionServiceURL {
  static readonly type = '[' + storeSliceName + '] VersionServiceURL';
  constructor(public versionurl: string, public ref?: ChangeDetectorRef) {  }
}

export class ResetForm {
  static readonly type = '[' + storeSliceName + '] ResetForm';
  constructor(public resetformvlaue: string, public ref?: ChangeDetectorRef) {  }
}

export class FormValidationvalue {
  static readonly type = '[' + storeSliceName + '] FormValidationvalue';
  constructor(public formvalidationvalues: any, public ref?: ChangeDetectorRef) {  }
}