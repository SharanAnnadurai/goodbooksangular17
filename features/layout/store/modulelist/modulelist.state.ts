
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { ModuleID , MenuOption } from './modulelist.action';
import { IMenuDto } from './../../models/Imodule.model';

export class modulestatemodel {  

    Dmoduleid: any;
    Dmenuoption!: IMenuDto | null;
}
@State<modulestatemodel>({
    name: 'moduleidstate',
    defaults: {

        Dmoduleid: null,
        Dmenuoption: null
    }
})
@Injectable()
export class modulestate {
    constructor() {
    }
    @Selector()
    static getmoduleid(state: modulestatemodel) {
        return state.Dmoduleid;
    }

    @Selector()

    static GetMenu(state: modulestatemodel) {
        return state.Dmenuoption;
    }

    @Action(ModuleID)
    getmoduleid({ getState, setState }: StateContext<modulestatemodel>, { moduleid }: ModuleID) {
        const state = getState();
        setState({
            ...state,
            Dmoduleid: moduleid
        });
    }

    @Action(MenuOption)
    addmenu({ getState, setState }: StateContext<modulestatemodel>, { Dmenuoption }: MenuOption) {
        const state = getState();
        setState({
            ...state,
            Dmenuoption: Dmenuoption
        });
    }

}











