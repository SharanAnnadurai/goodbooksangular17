import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Inject, Injectable } from '@angular/core';
import {
  ToggleMenuBar,
  OpenMenuBar,
  CloseMenuBar,
  TogglePinMenuBar,
  PinMenuBar,
  UnpinMenuBar,
  ToggleContextBar,
  OpenContextBar,
  CloseContextBar,
  TogglePinContextBar,
  PinContextBar,
  UnpinContextBar,
  ChangeTheme,
  ChangeMode,
  ButtonVisibility,
  Title,
  Path, 
  FormEditOption,
  FormEditabe,
  FormUnEditable,
  Movenextpreviousid,
  VersionServiceURL,
  ResetForm,
  FormValidationvalue
} from './layout.actions';

export class FormEditStateModel {
  formedit:any;
}

export class LayoutStateModel {
  MenuBarOpened!: boolean;
  MenuBarPinned!: boolean;
  FormEdit!: boolean;
  ContextBarOpened!: boolean;
  ContextBarPinned!: boolean;
  Path!: string;
  Theme!: string;
  Mode!: string;
  Visibilityname!: string;
  Title!: string | null;
  statemovenextpreviousid! : string | null;
  versionServiceURL! : string | null;
  resetform! : string | null
  formvalidations! : any
}

@State<LayoutStateModel>({
  name: 'Layout',
  defaults: {
    ContextBarOpened: false,
    ContextBarPinned: false,
    MenuBarOpened: false,
    MenuBarPinned: false,
    FormEdit:false,
    Theme: 'Default',
    Mode: 'light',
    Path: 'null',
    Visibilityname: 'null',
    Title:null,
    statemovenextpreviousid : null,
    versionServiceURL:null,
    resetform: null,
    formvalidations:null,
  },
})

@State<FormEditStateModel>({
  name: 'Form',
  defaults: {
    formedit: null,
  },
})

@Injectable()
export class LayoutState {
  @Selector() static Formeditabe(state: FormEditStateModel) {
    return state.formedit;
  }
  @Selector() static MenuBarOpened(state: LayoutStateModel) {
    return state.MenuBarOpened;
  }
  @Selector() static MenuBarPinned(state: LayoutStateModel) {
    return state.MenuBarPinned;
  }
  @Selector() static FormEdit(state: LayoutStateModel) {
    return state.FormEdit;
  }
  @Action(ToggleMenuBar)
  ToggleMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: !state.MenuBarOpened });
  }
  @Action(OpenMenuBar)
  OpenMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: true });
  }
  @Action(CloseMenuBar)
  CloseMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: false });
  }

  @Action(FormEditabe)
  FormEditabe(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, FormEdit: true });
  }
  @Action(FormUnEditable)
  FormUnEditable(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, FormEdit: false });
  }
  @Action(TogglePinMenuBar)
  TogglePinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: !state.MenuBarPinned });
  }
  @Action(PinMenuBar)
  PinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: true });
  }
  @Action(UnpinMenuBar)
  UnpinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: false });
  }

  @Selector() static ContextBarOpened(state: LayoutStateModel) {
    return state.ContextBarOpened;
  }
  @Selector() static ContextBarPinned(state: LayoutStateModel) {
    return state.ContextBarPinned;
  }
  @Action(ToggleContextBar)
  ToggleContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: !state.ContextBarOpened });
  }
  @Action(OpenContextBar)
  OpenContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: true });
  }
  @Action(CloseContextBar)
  CloseContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: false });
  }
  @Action(TogglePinContextBar)
  TogglePinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: !state.ContextBarPinned });
  }
  @Action(PinContextBar)
  PinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: true });
  }
  @Action(UnpinContextBar)
  UnpinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: false });
  }

  @Selector() static CurrentTheme(state: LayoutStateModel) {
    return state.Theme;
  }

  @Selector() static CurrentMode(state: LayoutStateModel) {
    return state.Mode;
  }

  @Selector() static ThemeClass(state: LayoutStateModel) {
    let retClass: String = 'theme--' + state.Theme + ' theme--' + state.Theme + '-' + state.Mode;
    return retClass;
  }

  @Selector() static Movenxtpreid(state: LayoutStateModel) {
    return state.statemovenextpreviousid;
  }

  @Selector() static Resetformvalue(state: LayoutStateModel) {
    return state.resetform;
  }

  @Selector() static FormValidationvalues(state: LayoutStateModel) {
    return state.formvalidations;
  }

  @Selector() static Versionservicurl(state: LayoutStateModel) {
    return state.versionServiceURL;
  }

  @Action(ChangeTheme)
  ChangeTheme(context: StateContext<LayoutStateModel>, newTheme: ChangeTheme) {
    const state = context.getState();
    context.setState({ ...state, Theme: newTheme.themeName });
    if (newTheme.ref != null) newTheme.ref.detectChanges();
  }

  @Action(VersionServiceURL)
  VersionServiceURL(context: StateContext<LayoutStateModel>, version: VersionServiceURL) {
    const state = context.getState();
    context.setState({ ...state, versionServiceURL:version.versionurl });
    if (version.ref != null) version.ref.detectChanges();
  }

  @Action(Movenextpreviousid)
  Movenextpreviousid(context: StateContext<LayoutStateModel>, moveid: Movenextpreviousid) {
    const state = context.getState();
    context.setState({ ...state, statemovenextpreviousid: moveid.movenextpreviousid });
  }

  @Action(ResetForm)
  ResetForm(context: StateContext<LayoutStateModel>, reset: ResetForm) {
    const state = context.getState();
    context.setState({ ...state, resetform: reset.resetformvlaue });
  }

  @Action(FormValidationvalue)
  FormValidationvalue(context: StateContext<LayoutStateModel>, validation: FormValidationvalue) {
    const state = context.getState();
    context.setState({ ...state, formvalidations: validation.formvalidationvalues });
  }

  @Action(ChangeMode)
  ChangeMode(context: StateContext<LayoutStateModel>, newMode: ChangeMode) {
    const state = context.getState();
    context.setState({ ...state, Mode: newMode.modeName });
    if (newMode.ref != null) newMode.ref.detectChanges();
  }

  @Selector() static NeedBackdrop(state: LayoutStateModel) {
    return !state.MenuBarPinned;
    return !state.ContextBarPinned || !state.MenuBarPinned;
  }


  @Selector() static Title(state: LayoutStateModel) {
    return state.Title;
  }

  @Action(Title)
  Title(context: StateContext<LayoutStateModel>, title: Title) {
    const state = context.getState();
    context.setState({ ...state, Title: title.title});
  }

  @Selector() static Visibilebuttons(state: LayoutStateModel) {
    return state.Visibilityname;
  }

  @Action(ButtonVisibility)
  Visibilityname(context: StateContext<LayoutStateModel>, visiblebuttons: ButtonVisibility) {
    const state = context.getState();
    context.setState({ ...state, Visibilityname: visiblebuttons.visibilityname});
  }

  @Selector() static Path(state: LayoutStateModel) {
    return state.Path;
  }

  @Action(Path)
  Path(context: StateContext<LayoutStateModel>, Path: Path) {
    const state = context.getState();
    context.setState({ ...state, Path: Path.path});
  }

  @Action(FormEditOption)
    FormEditOption(context: StateContext<FormEditStateModel>, FormEdit1: FormEditOption) {
      const state = context.getState();
      context.setState({ ...state, formedit: FormEdit1.FormEdit});
    }
}
