import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IMenulist } from './../../../models/Imenu.model';
import {CommonReportService} from './../../../../commonreport/service/commonreport.service';
import { Select, Store } from '@ngxs/store';
import { Reportid} from './../../../../commonreport/datastore/commonreport.action';
import { LayoutState } from './../../../../layout/store/layout.state'
import {ButtonVisibility , Title} from  './../../../../layout/store/layout.actions'

import { Router } from '@angular/router';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-menu-item', 
  templateUrl: './gbmenuitem.component.html',
  styleUrls: ['./gbmenuitem.component.scss']
})
export class MenuitemComponent implements OnInit {
  @Input() items!: IMenulist[];
  @ViewChild('childMenu', { static: true }) public childMenu: any;
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;


  constructor(public router: Router , private dataService: CommonReportService ,private store: Store) { }

  ngOnInit(): void {
  }

  Reportid!: number;

  public selecteddata(reportid:any){
    this.store.dispatch(new ButtonVisibility("Reports")) 
    let id = reportid.Id;
    let name = reportid.View;
    this.store.dispatch(new Title(name)) 
    console.log("datas",id);
    this.router.navigate(["/commonreport"]);
    this.store.dispatch(new Reportid(id)) 
  }

  public selected(data:any){
    this.store.dispatch(new Title(data)) 
    this.store.dispatch(new ButtonVisibility("Forms")) 
  }

}
 