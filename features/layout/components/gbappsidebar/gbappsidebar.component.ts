import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store'
import { modulestate } from './../../store/modulelist/modulelist.state';
import { LayoutState } from './../../store/layout.state';
import { GbformtitleComponent} from './../gbmaincontent/gbformheader/gbformtitle/gbformtitle.component';



@Component({
  selector: 'app-gbappsidebar',
  templateUrl: './gbappsidebar.component.html',
  styleUrls: ['./gbappsidebar.component.scss']
})
export class GbappsidebarComponent implements OnInit {
  public change!: boolean;
  formdisplay = false;
  height = 0;
  buttons = false ;
  icons = true;
  screendisplay = false;
  testdisplay = false;
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  @Select(modulestate.GetMenu) Menu$!: Observable<any>;
  @Select(LayoutState.MenuBarOpened) leftsidebarOpen$!: Observable<boolean>;
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  @ViewChild(GbformtitleComponent) title!: GbformtitleComponent;
  constructor() {
    
  }

  ngOnInit() {
    // this.leftsidebarOpen$.subscribe(res => {
    //   this.change = res;
    //   if (this.change == true){
    //     console.log("true");
    //     this.buttons = true;
    //     this.icons = false;
    //     this.formdisplay = false;
    //     this.screendisplay = false;
    //     this.testdisplay = false;
    //   }
    //   else if(this.change == false){
    //     this.buttons = false;
    //     this.icons = true;
    //     this.formdisplay = false;
    //     this.screendisplay = false;
    //     this.testdisplay = false;
    //   }
    // })
  }
 
  public extended() {

  }
  public Forms() {
    this.formdisplay = !this.formdisplay;
  }
  public Screen() {
    this.screendisplay = !this.screendisplay;
  }

  public Test() {
    this.testdisplay = !this.testdisplay;
  }
  



 }