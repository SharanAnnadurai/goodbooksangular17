import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { IMenulist } from './../../../models/Imenu.model';
import { MenuserviceService } from './../../..//services/menu/menuservice.service';
import { modulestate } from './../../../../../libs/gbui/src/lib/store/modulelist.state'
import { Observable, Subscription } from 'rxjs';
import { Select } from '@ngxs/store'
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { of as observableOf } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { CommonReportService } from './../../../../commonreport/service/commonreport.service';
import { Reportid } from './../../../../commonreport/datastore/commonreport.action';
import { LayoutState } from './../../../../layout/store/layout.state'
import { ButtonVisibility, Title,Path } from './../../../../layout/store/layout.actions'
import { Store } from '@ngxs/store';
import { NavigationExtras, Router } from '@angular/router';
import { OpenMenuBar, CloseMenuBar } from './../../../../../features/layout/store/layout.actions';
import { AfterViewChecked, ChangeDetectorRef } from '@angular/core'
import { MenuListservice } from './../../../../../features/layout/services/MenuList/MenuList.service';


export interface FileNode {
  DisplayName: string;
  ModuleName: string;
  Id: number;
  ListMenuId:any;
  WebFormSecondURL: string;
  MenuType: number;
  children?: FileNode[];
}

/** Flat node with expandable and level information */
export interface TreeNode {
  DisplayName: string;
  ModuleName:string;
  Id: number;
  WebFormSecondURL:string;
  MenuType: number;
  level: number;
  expandable: boolean;
  expanded:boolean;
  clicked:boolean;
}

@Component({
  selector: 'app-gbmenutree',
  templateUrl: './gbmenutree.component.html',
  styleUrls: ['./gbmenutree.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class GbmenutreeComponent{
  // treeControl: FlatTreeControl<TreeNode>;
  // treeFlattener: MatTreeFlattener<FileNode, TreeNode>;
  // dataSource: MatTreeFlatDataSource<FileNode, TreeNode, IMenulist>;
  // previousnode!: TreeNode;
  // navItems: any
  // selectedModuleID: any
  // hovercolor! : string;
  // public change= true;
  // @Select(modulestate.getmoduleid) selectedId: any;
  // @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  // @Select(LayoutState.MenuBarOpened) leftsidebarOpen$!: Observable<boolean>;
  // @Select(LayoutState.CurrentTheme) currenttheme: any;
  // private formSubscription: Subscription = new Subscription();
  // Module! : string;
  // themebackground: string='Default' ;
  // theme: string='Default';
  // constructor(private readonly changeDetectorRef: ChangeDetectorRef, public menuserviceService: MenuserviceService, public router: Router, private dataService: CommonReportService, private store: Store) {
  //   this.treeFlattener = new MatTreeFlattener(
  //     this.transformer,
  //     this.getLevel,
  //     this.isExpandable,
  //     this.getChildren
  //   );

  //   this.treeControl = new FlatTreeControl<TreeNode>(
  //     this.getLevel,
  //     this.isExpandable
  //   );
  //   this.dataSource = new MatTreeFlatDataSource(
  //     this.treeControl,
  //     this.treeFlattener
  //   );
  // }
  // ngAfterViewChecked(): void {
  //   this.changeDetectorRef.detectChanges();
  // }

  // ngOnInit(): void {
  //   this.leftsidebarOpen$.subscribe(res => {
  //     this.change = res;
  //   })
    
  //      this.formSubscription.add(
  //     this.selectedId.subscribe( (res:String) => {
  //       if (res) {
  //         this.selectedModuleID = res;
  //         this.MenuList(this.selectedModuleID);
  //       }
  //     })
  //   ); 

  //   this.currenttheme.subscribe( (theme:string) => {
  //     this.themebackground=theme;
  //     if (theme =='Blue') {
  //       this.theme=theme;
  //     } else {
  //       this.theme='Default'
  //     }
  //   })
   
  // }

  // public MenuList(moduleid: string) {
  //   this.menuserviceService.menudetailsservice(moduleid).subscribe(res => {
  //     let xx :any[]= []
  //     for(let data in res){
  //       xx.push(res[data]);
  //     }
  //     this.navItems=res; 
  //     this.dataSource.data = xx;
  //     this.Module=this.dataSource.data[1].ModuleName;
  //     // this.store.dispatch(new Title(this.Module))
  //     console.log("Datasource",this.dataSource.data)
  //   });
    
  // }

  // transformer(node: FileNode, level: number) {
  //   return {
  //     DisplayName: node.DisplayName,
  //     ModuleName:node.ModuleName,
  //     ListMenuId: node.ListMenuId,
  //     Id: node.Id,
  //     WebFormSecondURL: node.WebFormSecondURL,
  //     MenuType: node.MenuType,
  //     level: level,
  //     expandable: !!node.children.length,
  //     expanded:false,
  //     clicked:false
  //   };
  // }

  // over(node: TreeNode){
  //   if(node.clicked==false){
  //     node.expanded=true;
  //   }
  // }

  // out(node: TreeNode){
  //   if(node.clicked==false){
  //     node.expanded=false;
  //   }
  // }

  // over1(node: TreeNode){
  //   if(node.clicked==false){
  //     node.expanded=true;
  //     if((node.level)%2==1){
  //       this.hovercolor='colors mon-seventh'
  //     }
  //     else{
  //       this.hovercolor='colors mon-quinary'
  //     }
  //   }
  // }

  // out1(node: TreeNode){
  //   if(node.clicked==false){
  //     node.expanded=false;
  //     if((node.level)%2==1){
  //       this.hovercolor='colors mon-quinary'
  //     }
  //     else{
  //       this.hovercolor='colors mon-seventh'
  //     }
  //   }
  // }

  // changebackground1(node: TreeNode){
  //   if(this.previousnode!=undefined){
  //     this.previousnode.expanded=false
  //     this.previousnode.clicked=false
  //   }
  //   if (node.clicked==false){
  //     node.clicked=true
  //     node.expanded=true;
  //   } 
  //   else if (node.clicked==true){
  //     node.clicked=false
  //     node.expanded=false;
  //   }
  //   this.previousnode=node
  // }

  // changebackground(node: TreeNode){
  //   if (node.clicked==false){
  //     node.clicked=true
  //     node.expanded=true;
  //   } 
  //   else if (node.clicked==true){
  //     node.clicked=false
  //     node.expanded=false;
  //   }
  // }

  // /** Get the level of the node */
  // getLevel(node: TreeNode) {
  //   return node.level;
  // }

  // /** Return whether the node is expanded or not. */
  // isExpandable(node: TreeNode) {
  //   return !!node.expandable;
  // }

  // /** Get the children for the node. */
  // getChildren(node: FileNode) {
  //   return observableOf(node.children);
  // }

  // /** Get whether the node has children or not. */
  // hasChild(index: number, node: TreeNode) {
  //   //return !!node.children && node.children.length > 0;
  //   return node.expandable;
  //   //!!node.expandable && node.expandable.length > 0;
  // }
  // public selecteddata(reportid) {
  //   console.log("Report ID:", reportid);
  //   this.store.dispatch(new ButtonVisibility("Reports"))
  //   let id = reportid.Id;
  //   let name = reportid.View;
  //   this.store.dispatch(new Title(name))
  //   const queryParams: any = {};
  //   queryParams.Reportid = JSON.stringify(id);
  //   const navigationExtras: NavigationExtras = {
  //     queryParams
  //   };
  //   this.router.navigate([`commonreport/${id}`]);
  //   this.store.dispatch(new OpenMenuBar)
  //   this.store.dispatch(new Reportid(id))
  // }

  // public selected(data) {
  //   console.log("Data:", data)
  //   if (data.Id == -1) {
  //     let path = data.Path
  //     let view = data.View
  //     this.router.navigate([path]);
  //     this.store.dispatch(new Title(view))
  //     this.store.dispatch(new ButtonVisibility("Forms"))
  //   }
  //   else {
  //     let view = data.View
  //     let path = data.Path
  //     const queryParams: any = {};
  //     queryParams.Reportname = JSON.stringify(view);
  //     const navigationExtras: NavigationExtras = {
  //       queryParams
  //     };
  //     this.router.navigate(["/commonreport"], navigationExtras);
  //     this.store.dispatch(new Title(view))
  //     this.store.dispatch(new Path(path))
  //     this.store.dispatch(new ButtonVisibility("Report&forms"))
  //     this.store.dispatch(new Reportid(data.Id))
  //   }
  // }

  // public menulist(node){
  //   let menulist;
  //   for(let data of this.dataSource.data){
  //     if(node.DisplayName==data.DisplayName){
  //       menulist=data;
  //       break;
  //     }
  //   }
  //   //this.menulistservice.MenuList=menulist;
  //   const jsonString = JSON.stringify(menulist);
  //   let Menupath=this.Module+" > "+menulist.DisplayName;
  //   this.router.navigate(["GoodBooks/Menu",Menupath],{ queryParams: { data: jsonString } })
  // }

}
