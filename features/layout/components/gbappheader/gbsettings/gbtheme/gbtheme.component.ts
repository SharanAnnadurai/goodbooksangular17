import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ChangeMode, ChangeTheme, CloseContextBar, CloseMenuBar, OpenContextBar, OpenMenuBar } from '../../../../store/layout.actions';
import { LayoutState } from '../../../../store/layout.state';
import { OverlayContainer } from "@angular/cdk/overlay";
// import { GbmodulelistMainComponent } from 'features/erpmodules/samplescreens/screens/ModuleList/ModuleList.component';
// const themecolors = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/ThemeColor.json')
// const selectcolors = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/SelectedColor.json')
@Component({
  selector: 'app-gbtheme',
  templateUrl: './gbtheme.component.html',
  styleUrls: ['./gbtheme.component.scss']
})
export class GbThemeComponent implements OnInit {
  currentThemes!: string;
  currentMode!: string;
  color!:any;
  // private modulelist: GbmodulelistMainComponent
  constructor(private store: Store, private ref: ChangeDetectorRef, private overlayContainer: OverlayContainer) {
    let themevalue = '';
    let modevalue = ''
    this.theme$!.subscribe(res => themevalue = res)
    this.mode$!.subscribe(res => modevalue = res)
    this.currentThemes = themevalue;
    this.currentMode = modevalue;
  }
  ngAfterViewInit(): void {
    this.changeTheme(this.currentThemes);
    this.changeMode(this.currentMode);
  }


  ngOnInit(): void {
    // this.currenttheme.subscribe( (theme) => {
    //   if (theme =='Blue') {
    //     this.color=themecolors[theme];
    //     this.setStyle('--color', this.color);
    //     this.setStyle('--selectcolor', selectcolors[theme]);
    //   } else {
    //     this.color=themecolors.Default
    //     this.setStyle('--color', this.color);
    //     this.setStyle('--selectcolor', selectcolors.Default);
    //   }
    // })
  }

  // setStyle(name,value) {
  //   document.documentElement.style.setProperty(name, value);
  // }
  @Select(LayoutState.CurrentTheme) currenttheme: any; 
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  @Select(LayoutState.CurrentTheme) theme$!: Observable<string>;
  @Select(LayoutState.CurrentMode) mode$!: Observable<string>;
  @Select(LayoutState.MenuBarOpened) leftsidebarOpen$!: Observable<boolean>;
  @Select(LayoutState.ContextBarOpened) rightsidebarOpen$!: Observable<boolean>;

  changeTheme(newValue: string) {
    this.store.dispatch(new ChangeTheme(newValue, this.ref)); this.onThemeChange(); 
    console.log("Theme:",newValue)
  }
  changeMode(newValue: string) { 
    this.store.dispatch(new ChangeMode(newValue, this.ref)); this.onThemeChange(); 
  }

  onThemeChange() {
    this.ref.detectChanges();
    let theme = '';
    this.themeClass$.subscribe(res => theme = res);
    const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
    const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('theme--'));
    if (themeClassesToRemove.length) {
       overlayContainerClasses.remove(...themeClassesToRemove);
    }
    theme.split(' ').forEach(x => {
      overlayContainerClasses.add(x);
    });
    // overlayContainerClasses.add(theme);
  }
}
