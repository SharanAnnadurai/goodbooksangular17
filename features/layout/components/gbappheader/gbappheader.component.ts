import { ChangeDetectorRef, Component, Inject, OnInit , ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ChangeMode, ChangeTheme, CloseContextBar, CloseMenuBar, OpenContextBar, OpenMenuBar } from '../../store/layout.actions';
import { LayoutState } from '../../store/layout.state';
import { OverlayContainer } from "@angular/cdk/overlay";
import { GbformtitleComponent} from './../gbmaincontent/gbformheader/gbformtitle/gbformtitle.component';
import { Moduleservice } from './../../../../features/layout/services/modulelist/module.service';
import { ModuleID } from './../../../../features/layout/store/modulelist/modulelist.action';


@Component({
  selector: 'app-gbappheader',
  templateUrl: './gbappheader.component.html',
  styleUrls: ['./gbappheader.component.scss']
})
export class GbappHeaderComponent implements OnInit {
  modulelist:any;
  count:any;
  @ViewChild(GbformtitleComponent) title!: GbformtitleComponent;

  constructor(@Inject(Moduleservice) public moduleservice: Moduleservice, private store: Store, private ref: ChangeDetectorRef, private overlayContainer: OverlayContainer) { }

  ngOnInit(): void {
    this.viewmodulelist();
  }
  @Select(LayoutState.CurrentTheme) theme$!: Observable<string>;
  // @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
  // @Select(LayoutState.CurrentTheme) theme$: Observable<string>;
  // @Select(LayoutState.CurrentMode) mode$: Observable<string>;
  @Select(LayoutState.MenuBarOpened) leftsidebarOpen$!: Observable<boolean>;
  @Select(LayoutState.ContextBarOpened) rightsidebarOpen$!: Observable<boolean>;
  public color ='Black';
  public notification='assets/Header/Black/notification.png';
  public Dashboard='assets/Header/Black/DashBoard.png';
  public module='assets/Header/Black/ModuleMenu.png';
  toggleMenu() {
    this.leftsidebarOpen$.pipe(take(1)).subscribe(newValue => {
      if(!newValue) this.store.dispatch(new OpenMenuBar); else this.store.dispatch(new CloseMenuBar);
    });
  }
  toggleContext() {
    this.rightsidebarOpen$.pipe(take(1)).subscribe(newValue => {
      if(!newValue) this.store.dispatch(new OpenContextBar); else this.store.dispatch(new CloseContextBar);
    });
  }
  private viewmodulelist() {

    this.moduleservice.modulelistservice().subscribe((data:any) => {

      this.modulelist = data;

      this.count = this.modulelist.length;
    });


  }
  public SelectedModuleId(selectedModuleId: any) {
    this.store.dispatch(new ModuleID(selectedModuleId));
  }

  changeimageModuleWhite(){
    this.module='assets/Header/White/ModuleMenu.png';
  }

  changeimageModuleBlack(){
    this.module='assets/Header/Black/ModuleMenu.png';
  }

  changeimageDashboardWhite(){
    this.Dashboard='assets/Header/White/DashBoard.png';
  }

  changeimageDashboardBlack(){
    this.Dashboard='assets/Header/Black/DashBoard.png';
  }

  changeimagenotificationWhite(){
    this.notification='assets/Header/White/notification.png';
  }

  changeimagenotificationBlack(){
    this.notification='assets/Header/Black/notification.png';
  }
  // changeTheme(newValue: string) { this.store.dispatch(new ChangeTheme(newValue, this.ref)); this.onThemeChange(); }
  // changeMode(newValue: string) { this.store.dispatch(new ChangeMode(newValue, this.ref)); this.onThemeChange(); }
  // onThemeChange() {
  //   this.ref.detectChanges();
  //   let theme = '';
  //   this.themeClass$.subscribe(res => theme = res);
  //   const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
  //   const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('theme--'));
  //   if (themeClassesToRemove.length) {
  //      overlayContainerClasses.remove(...themeClassesToRemove);
  //   }
  //   theme.split(' ').forEach(x => {
  //     overlayContainerClasses.add(x);
  //   });
    // overlayContainerClasses.add(theme);
  // }
}
