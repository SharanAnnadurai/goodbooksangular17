import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbsearchComponent } from './gbsearch.component';

describe('GbsearchComponent', () => {
  let component: GbsearchComponent;
  let fixture: ComponentFixture<GbsearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbsearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
