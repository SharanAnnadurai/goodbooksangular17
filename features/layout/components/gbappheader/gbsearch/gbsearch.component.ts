import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { LayoutState } from '../../../store/layout.state';
@Component({
  selector: 'app-gbsearch',
  templateUrl: './gbsearch.component.html',
  styleUrls: ['./gbsearch.component.scss']
})
export class GbsearchComponent implements OnInit {
  @Select(LayoutState.CurrentTheme) theme$!: Observable<string>;
  public color! :any;
  public search! :any;
  constructor() { }

  ngOnInit(): void {
    this.theme$.subscribe( (theme) => {
      if (theme =='Blue') {
        this.search=theme;
      } else {
        this.search="Default";
      }
    })
  }

}
