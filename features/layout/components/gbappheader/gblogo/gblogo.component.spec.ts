import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GblogoComponent } from './gblogo.component';

describe('GblogoComponent', () => {
  let component: GblogoComponent;
  let fixture: ComponentFixture<GblogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GblogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GblogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
