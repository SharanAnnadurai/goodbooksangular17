import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmodulelistComponent } from './gbmodulelist.component';

describe('GbmodulelistComponent', () => {
  let component: GbmodulelistComponent;
  let fixture: ComponentFixture<GbmodulelistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbmodulelistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmodulelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
