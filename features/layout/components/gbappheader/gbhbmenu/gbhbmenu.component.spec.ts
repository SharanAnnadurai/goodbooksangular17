import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbhbmenuComponent } from './gbhbmenu.component';

describe('GbhbmenuComponent', () => {
  let component: GbhbmenuComponent;
  let fixture: ComponentFixture<GbhbmenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbhbmenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbhbmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
