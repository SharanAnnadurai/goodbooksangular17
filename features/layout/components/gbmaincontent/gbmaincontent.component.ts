import { Component, ViewChild} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GBBasePageComponentNG } from './../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { GbformactionsComponent } from './gbformheader/gbformactions/gbformactions.component';
import { GbformheaderComponent } from './gbformheader/gbformheader.component';
import {GbformtoolbarComponent} from './gbformtoolbar/gbformtoolbar.component';
@Component({
  selector: 'app-gbmaincontent',
  templateUrl: './gbmaincontent.component.html',
  styleUrls: ['./gbmaincontent.component.scss']
})
export class GbmaincontentComponent {
  public color:any;
  public curcomp:any;
  @ViewChild(RouterOutlet) page!: RouterOutlet;
  @ViewChild(GbformheaderComponent) formheader!: GbformheaderComponent;
  @ViewChild(GbformtoolbarComponent) formtoolbar!: GbformtoolbarComponent;
  @ViewChild(GbformactionsComponent) formdata!: GbformactionsComponent;
  
  public onActivate(){
    if (this.page){
      if (this.page.component){
        this.curcomp=this.page.component as GBBasePageComponentNG;
          //this.formtoolbar.form.formComponent= this.curcomp;
          this.formheader.forms.formComponent = this.curcomp;
          
      }
    }
  }
}
