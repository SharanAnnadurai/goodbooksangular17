import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformselectorComponent } from './gbformselector.component';

describe('GbformselectorComponent', () => {
  let component: GbformselectorComponent;
  let fixture: ComponentFixture<GbformselectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformselectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformselectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
