import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformstatusComponent } from './gbformstatus.component';

describe('GbformstatusComponent', () => {
  let component: GbformstatusComponent;
  let fixture: ComponentFixture<GbformstatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformstatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
