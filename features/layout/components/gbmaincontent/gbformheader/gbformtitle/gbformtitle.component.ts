import { Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state'
import { Observable } from 'rxjs';
// import { FilterState } from 'features/gbfilter/store/gbfilter.state';


@Component({
  selector: 'app-gbformtitle',
  templateUrl: './gbformtitle.component.html',
  styleUrls: ['./gbformtitle.component.scss']
})
export class GbformtitleComponent implements OnInit {
  viewvalue! : string;
  viewmode! :string;
  @Select(LayoutState.Title) view$!: Observable<any>;
  // @Select(FilterState.Filter) filtercriteria$!: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$!: Observable<string>;
  constructor() { }

  ngOnInit(): void {
    // this.filtercriteria$.subscribe(resx => {
    //   console.log("resx",resx)
    //   if(this.viewmode == "Reports" && resx){
    //     let filterfromdate : string;
    //     let filtertodate : string
    //     for(let data of resx.SectionCriteriaList[0].AttributesCriteriaList){
    //       console.log("datadata",data)
    //       if(data.FieldName == "PeriodFromDate"){
    //          filterfromdate = data.CriteriaAttributeValue
    //          console.log("PeriodFromDate",data.CriteriaAttributeValue)
    //       }
    //       if(data.FieldName == "PeriodToDate"){
    //         filtertodate = data.CriteriaAttributeValue
    //         console.log("PeriodToDate",data.CriteriaAttributeValue)
    //       }
    //     }
    //     if(filterfromdate.length > 1 && filtertodate.length > 1){
    //       // this.viewvalue = this.viewvalue+" --- Fromdate:"+filterfromdate+"Todate:"+filtertodate
    //     }
    //   }
    // })
    this.visiblebuttons$.subscribe(res => {
      if(res){
        this.viewmode = res;
      }
    })
    this.view$.subscribe(data => {
      if (data){
        this.viewvalue = data
      }
    })
  }
}
