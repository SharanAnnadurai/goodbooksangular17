import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformtitleComponent } from './gbformtitle.component';

describe('GbformtitleComponent', () => {
  let component: GbformtitleComponent;
  let fixture: ComponentFixture<GbformtitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformtitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformtitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
