
export class Columnsetting {
    headerName: string;
    field: string;
    sortable: boolean;
    filter: string;
    checkboxSelection: boolean;
    resizable: boolean;
    editable: boolean;
    pinned: string;
    flex: number;
    minWidth: number;
    enableRowGroup: boolean;
    enablePivot: boolean;
    enableValue: boolean;
    paginationAutoPageSize: boolean;
    pagination: boolean;
    routingpath: string;
    routingparams: string;
}

export class Columnmodel {
    viewValue: string;
    viewtype: string;
    Columnsetting: Columnsetting[];
}


