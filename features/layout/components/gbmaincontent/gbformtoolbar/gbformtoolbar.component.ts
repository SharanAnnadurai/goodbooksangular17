import { Component, OnInit, ViewChild } from '@angular/core';
import { GbformdataactionsComponent} from './gbformdataactions/gbformdataactions.component';

@Component({
  selector: 'app-gbformtoolbar',
  templateUrl: './gbformtoolbar.component.html',
  styleUrls: ['./gbformtoolbar.component.scss']
})
export class GbformtoolbarComponent implements OnInit {
  @ViewChild(GbformdataactionsComponent) form!: GbformdataactionsComponent;
  constructor() { }

  ngOnInit(): void {
  }

}
