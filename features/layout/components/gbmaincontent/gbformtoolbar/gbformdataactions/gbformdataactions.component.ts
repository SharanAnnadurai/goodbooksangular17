import { Component, Input, OnInit } from '@angular/core';
import { GBBasePageComponentNG } from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state';
// import { GbaboutComponent } from 'features/gbabout/components/gbabout/gbabout.component';
import { Router } from '@angular/router';
// import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';

@Component({
  selector: 'app-gbformdataactions',
  templateUrl: './gbformdataactions.component.html',
  styleUrls: ['./gbformdataactions.component.scss']
})
export class GbformdataactionsComponent implements OnInit {
  @Select(LayoutState.ThemeClass) themeClass$!: Observable<string>;
  @Input() private _formComponent!: GBBasePageComponentNG;
  @Input() private FormGroup!: GBBasePageComponentNG;

  constructor(public dialog: MatDialog, private router: Router,) {
  }

  get formComponent(): GBBasePageComponentNG {
    return this._formComponent;
  }

  get formgrp(): GBBasePageComponentNG {
    return this.FormGroup;
  }
  @Input()
  set formComponent(val: GBBasePageComponentNG) {
    this._formComponent = val;
  }
  public cc = [];
  public rr = [];

  ngOnInit(): void {

  }

  public addRecord() {
    const add = this.formComponent.form as any;
    add.clear();
  }

  public pdf() {
    const add = this.formComponent as any;
    var doc = new jsPDF('p', 'pt');

    add.columnData.forEach((element:any) => {
      var temp:any[] = element.headerName;
      this.cc.push(temp as never);
    });

    add.rowData.forEach((element:any) => {
      var temp:any[] = [element.Code, element.Name];
      this.rr.push(temp as never);
    });
    autoTable(doc, {
      columns: this.cc,
      body: this.rr,
    })
    doc.save("table.pdf");
  }

  public saveRecord() {
    const add = this.formComponent.form as any;
    add.save();
  }
  public clearRecord() {
    const add = this.formComponent.form as any;
    add.clear();
  }
  public deleteRecord() {
    const add = this.formComponent.form as any;
    add.delete();
  }

  public movePrev() {
    const prev = this.formComponent.form as any;
    prev.movePrev();
  }
  public moveNext() {
    const next = this.formComponent.form as any;
    next.moveNext();
  }
  public moveFirst() {
    const first = this.formComponent.form as any;
    first.moveFirst();
  }

  public moveLast() {
    const Last = this.formComponent.form as any;
    Last.moveLast();
  }

  aboutclick() {
    // const dialogRef = this.dialog.open(GbaboutComponent, { position: { right: '0' } });
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
    // this.router.navigate(['/gbabout']);
  }
  gblogclick() {
    // const dialogRef = this.dialog.open(GblogComponent, { position: { right: '0' } });
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
    // this.router.navigate(['/gblog']);
  }
}
