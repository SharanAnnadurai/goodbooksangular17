import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformdatatitleComponent } from './gbformdatatitle.component';

describe('GbformdatatitleComponent', () => {
  let component: GbformdatatitleComponent;
  let fixture: ComponentFixture<GbformdatatitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformdatatitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformdatatitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
