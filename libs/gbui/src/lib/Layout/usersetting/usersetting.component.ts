import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ThemeOption } from './../../../../../../features/common/shared/stores/auth.action';
import { IThemeDto } from './../../../../../../features/common/shared/stores/auth.model';
import { Store } from '@ngxs/store';


@Component({
  selector: 'app-usersetting',
  templateUrl: './usersetting.component.html',
  styleUrls: ['./usersetting.component.scss']
})
export class usersettingComponent implements OnInit {
  themeColor: string = "";
  constructor(public store: Store) { }

  ngOnInit(): void {
  }
  public themechange() {
    const change = 'primary'
    this.store.dispatch(new ThemeOption(change));
  }
  public themechanges() {
    const change = 'accent'
    this.store.dispatch(new ThemeOption(change));
  }
  public themechangess() {
    const change = 'warn'
    this.store.dispatch(new ThemeOption(change));
  }
}
