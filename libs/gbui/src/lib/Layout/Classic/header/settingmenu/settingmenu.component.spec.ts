import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { settingmenuComponent } from './settingmenu.component';



describe('MenuBarComponent', () => {
  let component: settingmenuComponent ;
  let fixture: ComponentFixture<settingmenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ settingmenuComponent  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(settingmenuComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
