import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SidebarService } from './../sidebar/sidebar.service';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store'
import { modulestate } from './../../store/modulelist.state';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  public change!: string;
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  @Select(modulestate.GetMenu) Menu$!: Observable<any>;
  private formSubscription: Subscription = new Subscription();
  constructor(private sideNavService: SidebarService) {
  }

  ngOnInit() {
    this.formSubscription.add(
      this.Menu$.subscribe(res => {
        this.change = res
        console.log("sidenavebar", this.change);
        if (this.change) {
          this.sidenav.toggle();
        }
      })
    )
  }
}
