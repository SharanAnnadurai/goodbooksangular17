import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GBBasePageComponentNG } from './../../../../../uicore/src/lib/components/Base/GBBasePage';
import { FormHeaderComponent } from './../FormHeader/formheader.component';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent  {
  @ViewChild(RouterOutlet) page!: RouterOutlet;
  @ViewChild(FormHeaderComponent) title!: FormHeaderComponent;
  
  public onActivate(){
    if (this.page){
      if (this.page.component){
      this.title.formComponent = this.page.component as GBBasePageComponentNG;
      }
    }
  }
}

