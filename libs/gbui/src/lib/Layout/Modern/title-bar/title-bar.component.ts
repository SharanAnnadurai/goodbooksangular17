import { Component, Input, OnInit } from '@angular/core';

import {
  GBBasePageComponentNG,
} from './../../../../../../uicore/src/lib/components/Base/GBBasePage';

@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.scss'],
})
export class TitleBarComponent implements OnInit {
  @Input() public formComponent!: GBBasePageComponentNG;

  constructor() {}

  ngOnInit(): void {
    // this.formComponent.form
  }

  public addRecord() {
    const add = this.formComponent.form as any;
    add.clear();
  }
  public deleteRecord() {
    const dlt = this.formComponent.form as any;
    dlt.delete();
  }
  public movePrev() {
    const prev = this.formComponent.form as any;
    prev.movePrev();
  }
  public moveNext() {
    const next = this.formComponent.form as any;
    next.moveNext();
  }
  public moveFirst() {
    const first = this.formComponent.form as any;
    first.moveFirst();
  }

  public moveLast() {
    const Last = this.formComponent.form as any;
    Last.moveLast();
  }

  listView() {}
}
