import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
// import { IMenulist } from './../../models/Imenu.model';
import { GBHttpService } from '@goodbooks/gbcommon';
import { GbUIURLS } from './../../URLS/urls';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class attachdbService {

  constructor(private http: GBHttpService, private httpp: HttpClient) { }
 /* public MenulistService(ModuleId:string): Observable<IMenulist> {
    const param = new HttpParams({fromObject: {UserId:'-1499998941',ModuleId: ModuleId}});
  
    //return this.http.get(GbUIURLS.MENULIST, { params: param})

   // return this.http.httpget(GbUIURLS.MENULIST+ModuleId);
   return this.http.httpgetparams(GbUIURLS.MENULIST,param)

  }*/
  public uploadservice1(formdata:any,objecttypeid:any)
  {

    var rangen = Math.floor(Math.random() * 1499999999) + 1;
    let userObjectId = rangen;

    return this.httpp.post('api/fws/Entity.svc/FileUploadWebWithMetadata/?ObjectId='+userObjectId+'&ObjectTypeId='+objecttypeid, formdata)

     
 

  }

  public uploadservice2(alfrescodata:any,objecttypeid:any)
  {

    var rangen = Math.floor(Math.random() * 1499999999) + 1;
    let userObjectId = rangen;

    return this.httpp.post('api/fws/Entity.svc/UpdateAttachment/?ObjectId='+userObjectId+'&ObjectTypeId='+objecttypeid, alfrescodata)

     
   
  }

  public entityfiledownloadservice(id:string)
  {
    return this.http.httpget(GbUIURLS.entityfileupload+id);
  }

}





