import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { Router, RouterModule } from '@angular/router';

import { GbComboboxModule } from './../gb-combobox/gb-combobox.module';
import {GbNumericModule} from './../gb-numeric/gb-numeric.module';
import {GbPhoneComponent} from './gb-phone.component';



@NgModule({
  declarations: [
    GbPhoneComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    GbNumericModule,
    MatFormFieldModule,
    GbComboboxModule
  ],
  exports: [
    GbPhoneComponent
  ],

})
export class GbPhoneModule { }
