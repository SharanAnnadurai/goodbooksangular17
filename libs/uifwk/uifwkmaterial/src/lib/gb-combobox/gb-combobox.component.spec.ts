import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbComboboxComponent } from './gb-combobox.component';

describe('GbComboboxComponent', () => {
  let component: GbComboboxComponent;
  let fixture: ComponentFixture<GbComboboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbComboboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbComboboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
