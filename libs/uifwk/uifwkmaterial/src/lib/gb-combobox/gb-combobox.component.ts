import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
// import { LayoutState } from 'features/layout/store/layout.state';
@Component({
  selector: 'app-gb-combobox',
  templateUrl: './gb-combobox.component.html',
  styleUrls: ['./gb-combobox.component.scss']
})
export class GbComboboxComponent extends GBBaseUIFCComponent implements OnChanges {
  // @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
  // @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Input() picklistid:any;
  @Input() picklistdata:any;
  @Output() childEvent : EventEmitter<any> = new EventEmitter();
  check = 1
  isreadable = false;
  defaultvalue : any ={key:"",value:""};
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['picklistid'] || changes['picklistdata']){
      if(this.picklistid || this.picklistid==0){
      this.defaultvalue = this.picklistdata.find((c:any) => c.key == this.picklistid )
    }
    if(this.picklistid == undefined){
      this.picklistid = 0
      this.defaultvalue = this.picklistdata.find((c:any) => c.key == this.picklistid )
    }
    }
    // this.edit$.subscribe(res => {
    //   this.isreadable = res;
    // })
  }

  change(value:any){
    this.picklistid = value
    this.defaultvalue = this.picklistdata.find((c: any) => c.key == this.picklistid )
    this.childEvent.emit(value);
  }
}
