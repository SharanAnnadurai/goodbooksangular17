import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule,FormsModule   } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { Router, RouterModule } from '@angular/router';
import { GbComboboxComponent } from './gb-combobox.component';

@NgModule({
  declarations: [GbComboboxComponent],
  imports: [CommonModule, FormsModule  ,RouterModule, MatSelectModule, ReactiveFormsModule],
  exports: [GbComboboxComponent],
})
export class GbComboboxModule {}
