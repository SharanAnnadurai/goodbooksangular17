import { Component, OnInit } from '@angular/core';

import { GbNumericComponent } from '../gb-numeric/gb-numeric.component';

@Component({
  selector: 'app-gb-currency',
  templateUrl: './gb-currency.component.html',
  styleUrls: ['./gb-currency.component.scss']
})
export class GbCurrencyComponent extends GbNumericComponent {

}
