import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbCurrencyComponent } from './gb-currency.component';

describe('GbCurrencyComponent', () => {
  let component: GbCurrencyComponent;
  let fixture: ComponentFixture<GbCurrencyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
