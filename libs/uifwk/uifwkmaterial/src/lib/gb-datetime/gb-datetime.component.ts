import { Component, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

import {GBBaseUIFCComponent} from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent'
@Component({
  selector: 'app-gb-datetime',
  templateUrl: './gb-datetime.component.html',
  styleUrls: ['./gb-datetime.component.scss']
})
export class GbDateTimeComponent extends GBBaseUIFCComponent {
  // @Input() floatLabel; // 'always' | 'never' | 'auto' = 'always';
  // @Input() appearance; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
  // @ViewChild('picker') picker: any;

  // public disabled = false;
  // public showSpinners = true;
  // public showSeconds = false;
  // public touchUi = false;
  // public enableMeridian = false;
  // public minDate!: Date;
  // public maxDate!: Date;
  // public stepHour = 1;
  // public stepMinute = 1;
  // public stepSecond = 1;

  // public dateControl = new FormControl(new Date());

  // public options = [
  //   { value: true, label: 'True' },
  //   { value: false, label: 'False' }
  // ];
  // public stepHours = [1, 2, 3, 4, 5];
  // public stepMinutes = [1, 5, 10, 15, 20, 25];
  // public stepSeconds = [1, 5, 10, 15, 20, 25];
}

