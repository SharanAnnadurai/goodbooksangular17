import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbTextareaComponent } from './gb-textarea.component';

describe('GbTextareaComponent', () => {
  let component: GbTextareaComponent;
  let fixture: ComponentFixture<GbTextareaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
