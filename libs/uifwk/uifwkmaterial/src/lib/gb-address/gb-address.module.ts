import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { Router, RouterModule } from '@angular/router';

import { GbInputModule } from './../gb-input/gb-input.module';
import { GbNumericModule } from './../gb-numeric/gb-numeric.module';
import { GbAddressComponent } from './gb-address.component';
@NgModule({
  declarations: [
    GbAddressComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    GbInputModule,
    MatFormFieldModule,
    GbNumericModule,
  ],
  exports: [
    GbAddressComponent
  ],
})
export class GbAddressModule {}
