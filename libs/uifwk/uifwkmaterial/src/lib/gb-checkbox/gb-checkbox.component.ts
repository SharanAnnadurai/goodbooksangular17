import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

import { GBBaseUIFCComponent } from './../../../../../uicore/src/lib/components/Base/GBBaseUIFCComponent';

@Component({
  selector: 'app-gb-checkbox',
  templateUrl: './gb-checkbox.component.html',
  styleUrls: ['./gb-checkbox.component.scss']
})
export class GbCheckboxComponent extends GBBaseUIFCComponent implements  OnChanges {
  @Input() isChecked:any;
  @Output() childEvent : EventEmitter<any> = new EventEmitter();
  check : boolean = false
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['isChecked']){
      this.check = this.isChecked == 0 ? true : false;
      if(this.isChecked == undefined){
        this.check = false
      }
    }
  }

  chkChanged(evt:any) {
    let value = evt.checked ? 1 : 0;
    this.childEvent.emit(value);

  }
}
