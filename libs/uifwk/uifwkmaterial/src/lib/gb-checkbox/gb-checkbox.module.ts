import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { GbCheckboxComponent } from './gb-checkbox.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GbCheckboxComponent],
  imports: [CommonModule, RouterModule, MatCheckboxModule, ReactiveFormsModule],
  exports: [GbCheckboxComponent],
})
export class GbCheckboxModule {}
