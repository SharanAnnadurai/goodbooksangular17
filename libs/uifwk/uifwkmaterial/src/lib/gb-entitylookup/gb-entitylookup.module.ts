import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GbEntitylookupComponent } from './gb-entitylookup.component';


@NgModule({
  declarations: [GbEntitylookupComponent],
  imports: [
     CommonModule,
     RouterModule,
     ReactiveFormsModule
    ],
  exports: [
    GbEntitylookupComponent
  ],
})
export class GbEntitylookupModule {}
