import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbMobileComponent } from './gb-mobile.component';

describe('GbMobileComponent', () => {
  let component: GbMobileComponent;
  let fixture: ComponentFixture<GbMobileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
