import { Component, Input } from '@angular/core';

import { GbInputComponent } from '../gb-input/gb-input.component';

@Component({
  selector: 'app-gb-numeric',
  templateUrl: './gb-numeric.component.html',
  styleUrls: ['./gb-numeric.component.scss']
})
export class GbNumericComponent extends GbInputComponent {
  @Input() precision!: number;
  // @ViewChild('input') gbinput: GbInputComponent;

  get Precision(): number {
    if (!this.precision && this.Field !== null && this.Field !== undefined) {
      return this.Field.Precision;
    }
    else {
      return this.precision;
    }
  }

  override onKeyDown(e: any): boolean {
    if (super.onKeyDown(e)) {
      return true;
    }
    // Check Precision to allow decimal
    if (this.Precision === 0 && (e.keyCode === 190 || e.keyCode === 46)) {
      e.preventDefault();
      return true;
    }
    // Restrict decimal if already present
    if (this.value != null && this.value.toString().includes('.') && (e.keyCode === 190 || e.keyCode === 46)) {
      e.preventDefault();
      return true;
    }
    // Restrict everything after precision
    if (this.value != null && this.value.toString().includes('.') && this.Precision > 0 && this.value.toString().length - this.value.toString().indexOf('.') > this.Precision) {
      e.preventDefault();
      return true;
    }
    // Restrict numbers
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && e.keyCode !== 190 && e.keyCode !== 46) {
      e.preventDefault();
      return true;
    }
    return true;
// return false;
  }
}
