import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-textarea',
  template: '{{Field?.Label}}:<textarea  [formControl]="gbControl" rows="4" cols="50"></textarea><br/>'
})
export class GbTextareaComponent extends GBBaseUIFCComponent {
}
