import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-radiobox',
  template: `{{Field?.Label}}:
  <span *ngFor="let val of Field.PickLists">
  <input type="radio" [formControl]="gbControl"id={{Field?.Name}} value={{val}}>{{val}}
  </span>
  <br/>`
})
export class GbRadioboxComponent extends GBBaseUIFCComponent {
}
