import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IScreen } from '../interfaces/ibasefield';

@Injectable()
export class GBUIMetadataService {
  constructor(
    private http: HttpClient) {

  }

  public getFormData(id: string): Observable<IScreen> {
    return this.http.get<IScreen[]>('/assets/mockdata/_general/screens.json')
      .pipe(
        map(res => {
          return res.filter(f => f.ID === id)[0];
        })
      );
  }

  public getAccessControls() {
    //this is to avoid an empty function
  }
}
