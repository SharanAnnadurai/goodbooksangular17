import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormTemplateDirective } from './directives/formtemplate.directive';

export { GBFormGroup, GBDataFormGroup, GBDataFormGroupWN, GBDataFormGroupStore, 
  GBDataFormGroupStoreWN, GBBaseFormGroup } from './classes/GBFormGroup';
export { GBFormControl } from './classes/GBFormControl';

export { GBBaseComponent } from './components/Base/GBBaseComponent';
export { GBBaseUIComponent } from './components/Base/GBBaseUIComponent';
export { GBBaseUIFCComponent } from './components/Base/GBBaseUIFCComponent';
export { GBBaseUIFGComponent } from './components/Base/GBBaseUIFGComponent';
export { GBBasePageComponent, GBBasePageComponentNG, GBBaseDataPageComponent, GBBaseDataPageComponentWN, GBBaseDataPageStoreComponent, GBBaseDataPageStoreComponentWN,
         GBBaseListPageComponent, GBBaseListDataStoreComponentWN,GBBaseListDataPageStoreComponent} from './components/Base/GBBasePage';
export { GBDataPageService, GBPageService, GBDataPageServiceStore, GBDataPageServiceStoreWN, GBDataPageServiceWN, GBDataPageServiceNG, } from './services/gbpage.service';
export { IAddressData } from './interfaces/iaddressdata';
export { IField, IUIField, IPickListValue, IScreen, IDataType, IModule, IEntity, IRelationship, IView } from './interfaces/ibasefield';
export { FormTemplateDirective } from './directives/formtemplate.directive';

@NgModule({
  declarations: [FormTemplateDirective],
  imports: [CommonModule],
  exports: [FormTemplateDirective]
})
export class UicoreModule {}
