import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[gbFormtemplate]'
})
export class FormTemplateDirective {
  @Input() name!: string;
  @Input() description!: string;

  constructor(public template: TemplateRef<any>, public viewContainerRef: ViewContainerRef, public el: ElementRef) {
   }

}
