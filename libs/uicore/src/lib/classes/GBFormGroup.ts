import { HttpClient } from '@angular/common/http';
import { AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

// import { GBBaseDataService, GBBaseDataServiceWN, GBDataStateActionFactory, GBDataStateActionFactoryWN } from '@goodbooks/gbdata';
import {GBBaseDataService, GBBaseDataServiceWN} from './../../../../gbdata/src/lib/services/gbbasedata.service';
import {GBDataStateActionFactory, GBDataStateActionFactoryWN } from './../../../../gbdata/src/lib/store/gbdata.actions'
import { Select, Store } from '@ngxs/store'; 

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IField, IScreen } from '../interfaces/ibasefield';

import { GBFormControl } from './GBFormControl';
import { GBFormService } from './../services/gbform.service'
// import * as screenjson from './../../../../uicore/src/lib/assets/screens.json';
// const screenjson = require('./../assets/screens.json');

export class GBBaseFormGroup extends FormGroup {
  myvalidatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions;
  public ErrMessages: { [key: string]: string } = {};

  constructor(
    controls: { [key: string]: AbstractControl; },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  public addCustomvalidator(
    name: string,
    customValidatorOrOpts: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    errMessage?: string
  ) {
    let newValidFns: ValidatorFn[] = [];
    if (Array.isArray(this.myvalidatorOrOpts)) {
      newValidFns = newValidFns.concat(this.myvalidatorOrOpts);
    }
    if (Array.isArray(customValidatorOrOpts)) {
      newValidFns = newValidFns.concat(customValidatorOrOpts);
    }
    else if (typeof customValidatorOrOpts === 'function') {
      newValidFns.push(customValidatorOrOpts);
    }
    this.ErrMessages[name] = errMessage || '';
    this.setValidators(newValidFns);
    this.updateValueAndValidity();
  }

  public get hasCustomError(): boolean {
    let retValue = false;
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retValue = true;
        }
      });
    }
    return retValue;
  }

  public get customErrorMessages(): string {
    let retMsg = '';
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retMsg += this.ErrMessages[element] + '; ';
        }
      });
    }
    return retMsg;
  }

  public override reset() {
    super.reset();
    Object.keys(this.controls).forEach(key => {
      const fc = this.controls[key] as GBFormControl;
      fc.setValue(fc.field.DefaultValue);
    });
  }
}

export class GBFormSubGroup extends GBBaseFormGroup {

  constructor(public field: IField, controls: { [key: string]: AbstractControl; }, validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions, asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]) {
    super(controls, validatorOrOpts, asyncValidator);
  }

}

export class GBFormGroup extends GBBaseFormGroup {
  // fs: GBFormService;
  public get bFormReady(): boolean {
    return (Object.keys(this.controls).length > 0);
  }
  constructor(
    private http: HttpClient,
    public formid: string,
    // public formservice: GBFormService,
    controls: { [key: number]: AbstractControl; },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(controls, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    this.setFormGroup();
  }

  private _getFormData(id: string): Observable<IScreen> {
    let screenjson : any;
    this.http.get('/assets/screens.json').subscribe((res:any)=>{
      screenjson = res
    })
    return this.http.get(screenjson[id])
    .pipe(
      map(res => {
        return (res as IScreen);
      })
    );
  }

  setFormGroup() {
    this.Initialize().subscribe();
  }

  public Initialize(): Observable<boolean> {
    return this._getFormData(this.formid).pipe(map(res => {
      const form: IScreen = res;
      const formFields = form.ObjectFields;
      for (const itm of formFields) {
        const ibf = itm; // formFields[i] as IField;
        switch (ibf.Type.toLowerCase()) {
          case 'address': {
            this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
              Address1: new GBFormControl({ Name: 'addr1Field', Type: 'string', Label: 'Address 1', Required: false } as IField, ''),
              Address2: new GBFormControl({ Name: 'addr2Field', Type: 'string', Label: 'Address 2', Required: false } as IField, ''),
              City: new GBFormControl({ Name: 'cityField', Type: 'string', Label: 'City', Required: false } as IField, ''),
              State: new GBFormControl({ Name: 'stateField', Type: 'string', Label: 'State', Required: false } as IField, ''),
              Country: new GBFormControl({ Name: 'ctryField', Type: 'string', Label: 'Country', Required: false } as IField, ''),
              PostCode: new GBFormControl({ Name: 'postField', Type: 'string', Label: 'Post Code', Required: false } as IField, ''),
            }));
            break;
          }
          case 'datetime': {
            break;
          }
          case 'mobile': {
            this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
              Code: new GBFormControl({ Name: 'Code', Type: 'string', Label: 'Title', Tooltip: '', Placeholder: '--Select--',
              Required: false, ValidRegEx: '', PickLists: [
                '+91', '+92', '+93']} as IField, ''),
              Mobileno: new GBFormControl({ Name: 'mobile', Type: 'string', Label: 'Mobile', Tooltip: '', Placeholder: 'MobileNo', Required: false,
              ValidRegEx: ''} as IField, '')
            }));
            break;
          }
          case 'phone': {
            this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
              Code: new GBFormControl({ Name: 'Code', Type: 'string', Label: 'Title', Tooltip: '', Placeholder: '--Select--',
              Required: false, ValidRegEx: '', PickLists: [
                '+91', '+92', '+93']} as IField, ''),
              DisCode: new GBFormControl({ Name: 'DisCode', Type: 'string', Label: 'Code', Tooltip: '', Placeholder: 'Code', Required: false,
                ValidRegEx: ''} as IField, ''),
              Phoneno: new GBFormControl({ Name: 'Phoneno', Type: 'string', Label: 'phone', Tooltip: '', Placeholder: 'Phone', Required: false,
              ValidRegEx: ''} as IField, '')
            }));
            break;
          }
          default: {
            this.addControl(ibf.Name, new GBFormControl(ibf, ibf.DefaultValue));
            break;
          }
        }
      }
      return true;
    }));
  }
}

export class GBDataFormGroup<T extends { [key: string]: any }> extends GBFormGroup {
  constructor(
    http: HttpClient,
    public override formid: string,
    controls: { [key: string]: AbstractControl; },
    public ds: GBBaseDataService<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    // this.fs.setFormGroup(this);
  }
public getData(id: string) {
this.ds.getData(id);
}
  public clear() {
    this.reset();
  }

  public delete() {
    const pdata: T = this.value as T;
    if (typeof pdata === 'object' && pdata !== null) {
      this.ds.deleteData(pdata[this.ds.IdField]);
    }
    this.reset();
  }
  public save() {

    const pdata: T = this.value as T;
    this.ds.saveData(pdata);

  }
}

export class GBDataFormGroupWN<T extends { [key: string]: any }> extends GBDataFormGroup<T> {
  constructor(
    http: HttpClient,
    public override formid: string,
    controls: { [key: string]: AbstractControl; },
    public override ds: GBBaseDataServiceWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, ds, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    // this.fs.setFormGroup(this);

    this.ds.getAll().subscribe(res => {
      this.ds.moveFirst().subscribe(r => {
        const pk: T = r;
        this.patchValue(pk);
      });
    });
  }
  public override clear() {
    this.reset();
  }
  public moveFirst() {
    this.ds.moveFirst().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public movePrev() {
    this.ds.movePrev().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public moveNext() {
    this.ds.moveNext().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public moveLast() {
    this.ds.moveLast().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

}

export class GBDataFormGroupStore<T> extends GBFormGroup {
  // currentData$;//: Observable<T>;
  constructor(
    http: HttpClient,
    public override formid: string,
    controls: { [key: string]: AbstractControl; },
    public store: Store,
    public af: GBDataStateActionFactory<T>,
    //public ds: GBBaseDataServiceWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, validatorOrOpts, asyncValidator);
  }
  public getData(id: number) {
    this.store.dispatch(this.af.GetData(id));
    }
  public clear() {
    this.store.dispatch(this.af.ClearData())
    this.reset();
  }

  public delete() {
    const pdata: T = this.value as T;
    this.store.dispatch(this.af.DeleteData())
    // this.ds.deleteData(pdata[this.ds.IdField]);
    //this.reset();
  }
  public save() {
    const pdata: T = this.value as T;
    this.store.dispatch(this.af.SaveData(pdata));
  }
}

export class GBDataFormGroupStoreWN<T> extends GBDataFormGroupStore<T> {

  constructor(
    http: HttpClient,
    public override formid: string,
    controls: { [key: string]: AbstractControl; },
    public override store: Store,
    public override af: GBDataStateActionFactoryWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, store, af, validatorOrOpts, asyncValidator);
  }

  public getAll(scl:any) {
    this.store.dispatch(this.af.GetAll(scl));
  }

  public moveFirst() {
    this.store.dispatch(this.af.MoveFirst());
  }

  public movePrev() {
    this.store.dispatch(this.af.MovePrev());
  }

  public moveNext() {
    this.store.dispatch(this.af.MoveNext());
  }

  public moveLast() {
    this.store.dispatch(this.af.MoveLast());
  }

}

export class GBDataListFormGroupStore<T> extends GBDataFormGroupStore<T> {
  constructor(
    http: HttpClient,
    public override formid: string,
    controls: { [key: string]: AbstractControl; },
    public override store: Store,
    public override af: GBDataStateActionFactoryWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, store, af, validatorOrOpts, asyncValidator);
  }

  public getAll(scl:any){
    this.store.dispatch(this.af.GetAll(scl));
  }
}