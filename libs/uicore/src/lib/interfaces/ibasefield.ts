import { AbstractControl, FormArray, FormGroup } from '@angular/forms';

export interface IUIField {
  field?: IField;
  readonly form?: FormGroup | FormArray;
  readonly formControl?: AbstractControl;
  readonly data?: any;
}

export interface IField {
  Name: string;
  Description: string;
  IsSystem: boolean;
  // EntityID: IBaseEntity;
  Formula: string;
  EnableAnalysis: boolean;
  Length: number;
  Precision: number;

  Type: string;
  width:string;
  Label: string;
  Tooltip?: string;
  Placeholder?: string;

  Required: boolean;
  ValidRegEx: string;
  MinValue?: any;
  MaxValue?: any;
  DefaultValue: any;
  PickLists: string[] | number[] | IPickListValue[];

  // Events
  BeforeChange: string;
  AfterChange: string;

  Rules: IRule[];
}

export interface IPickListValue {
  key: string | number;
  value: string;
}

export interface IScreen {
  ID: string;
  Name: string;
  Description: string;
  EntityID: IEntity;
  IsSystem: boolean;
  ScreenType: string;
  ViewID: IView;

  // Events
  AfterLoad: string;
  BeforeSave: string;
  AfterSave: string;
  BeforeDelete: string;
  AfterValidation: string;

  // Misc
  HelpURL: string;
  MultimediaURL: string;
  PrecompliledScreenPage: string;
  OverrideScreenPage: string;
  ObjectFields: IField[];

  Rules: IRule[];
}

export interface IDataType {
  Name: string;
  RendererClass: string;
}

export interface IModule {
  Name: string;
  Description: string;
  IsSystem: string;
  HelpURL: string;
  MultimediaURL: string;
  PartnerID: string;
}

export interface IEntity {
  ID: string;
  ModuleID: IModule;
  Name: string;
  Description: string;
  ImageField: string;
  IDField: IField;
  BIZTransactionTypes: string;
  IsSystem: boolean;
  RestEndpoint: string;
  HelpURL: string;
  MultimediaURL: string;
  Attachments: boolean;
  Activities: boolean;
  Communication: boolean;
  DefaultDocumentLayout: string;
  LookupFields: IField[];
}

export interface IRelationship {
  Name: string;
  FromEntityID: IEntity;
  ToEntityID: IEntity;
  RelationshipType: string;
  IsSystem: boolean;
}

export interface IView {
  Name: string;
  Description: string;
  IsSystem: boolean;
  EntityID: IEntity;
  ImageField: IField;
  Header1Field: IField;
  Header2Field: IField;
  Editable: boolean;
}

export interface IRule {
  ID: string;
  Name: string;
  Condition: ICondition;
}

export interface ICondition {
  Fact: string;
  Operator: EnumComparers;
  ValueType: EnumValueType;
  Value: unknown;
  Truthy: IAction | ICondition;
  Falsy: IAction | ICondition;
}

export interface IAction {
  Name: string;
}

export interface IActionShowError extends IAction {
  Field: string;
  ErrorMessage: string;
}

export interface IActionSetVisible extends IAction {
  Field: string;
  Visible: boolean;
}

export interface IActionSetValue extends IAction {
  Field: string;
  ValueType: EnumValueType;
  Value: unknown | IFormula;
}

export interface IActionSetRequired extends IAction {
  Field: string;
  Required: boolean;
}

export interface IActionSetLock extends IAction {
  Field: string;
  Locked: boolean;
}

export interface IFormula {
  Field: string;
  Operator: EnumOperators
}

export const enum EnumComparers {
  AreEqual = 0,
  AreNotEqual = 1,
  IsLessThan = 2,
  IsLessThanOrEqual  = 3,
  IsGreaterThan = 4,
  IsGreaterThanEqual = 5,
  ContainsData = 6,
  DoesNotContainData = 7,
  BeginsWith = 8,
  DoesNotBeginWith = 9,
  EndsWith = 10,
  DoesNotEndWith = 11
}

export const enum  EnumOperators {
  Add = 0,
  Subtract = 1,
  Multiply = 2,
  Divide = 3
}

export const enum EnumValueType {
  Constant = 0,
  Field = 1,
  Formula = 2
}
