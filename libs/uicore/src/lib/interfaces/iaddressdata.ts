
export class IAddressData {
  public Address1: string='';
  public Address2: string ='';
  public City: string ='';
  public State: string ='';
  public Country: string ='';
  public PostCode: any = '';
}
