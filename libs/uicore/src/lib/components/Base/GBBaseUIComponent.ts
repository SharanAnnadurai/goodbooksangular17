import { Component, Input} from '@angular/core';

import { GBBaseComponent } from './GBBaseComponent';

@Component({
  template: '',
})
export abstract class GBBaseUIComponent extends GBBaseComponent {
  @Input() ReadOnly = false;
}
