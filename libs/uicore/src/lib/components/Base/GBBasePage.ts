// import { GBFormService } from '@goodbooks/uicore';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Inject, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { Subscription } from 'rxjs';

import { GBBaseFormGroup, GBDataFormGroup, GBDataFormGroupStore, GBDataFormGroupStoreWN, GBDataFormGroupWN, GBFormGroup } from '../../classes/GBFormGroup';
import { GBDataPageService, GBDataPageServiceNG, GBDataPageServiceWN, GBPageService } from '../../services/gbpage.service';
import { FormTemplateDirective } from '../../directives/formtemplate.directive';
// import { GBFormService } from '../services/gbform.service';

@Component({
  template: 'app-gb-page',
})
export abstract class GBBasePageComponent {
  abstract title: string;
  abstract isDataPage: boolean;
  abstract hasNavigation: boolean;
  // abstract form: GBBaseFormGroup;
  // constructor(protected gbps: GBPageService) {

  // }
  constructor(@Inject('GBPageService') protected gbps: GBPageService) {
    this.thisConstructor();
  }
  abstract thisConstructor(): void;
}
@Component({
  template: 'app-gb-page',
})
export abstract class GBBasePageComponentNG extends GBBasePageComponent {
  isDataPage = true;
  hasNavigation = false;
  abstract form: GBBaseFormGroup;
  constructor(@Inject('PageService') protected override gbps: GBDataPageServiceNG) {
    super(gbps);
  }

  // template support...
  @ViewChildren(FormTemplateDirective) formTemplates!: QueryList<FormTemplateDirective>;
  _currentFormName: string = 'default';
  public get currentFormName(): string {
    return this._currentFormName;
  }
  public set currentFormName(newVal: string) {
    this._currentFormName = newVal;
  }
  public get templateNames(): string[] {
    if (this.formTemplates) {
      return this.formTemplates.map(e => e.name).filter(e => e && e != '');
    }
    else {
      return [];
    }
  }
  public get currentForm() {
    if (this.currentFormName === null || this.currentFormName === '') return null;
    return this.formTemplates?.toArray().find(x => x.name.toLowerCase() == this.currentFormName.toLowerCase())?.template;
  }
}
@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageComponent<T extends { [key: string]: any; }> extends GBBasePageComponentNG {
  abstract override form: GBDataFormGroup<T>;
  override isDataPage = true;
  override hasNavigation = false;
  constructor(@Inject('PageService') protected override gbps: GBDataPageService<T>) {
    super(gbps);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageComponentWN<T extends { [key: string]: any; }> extends GBBaseDataPageComponent<T> {
  abstract override form: GBDataFormGroupWN<T>;
  override hasNavigation = true;
  constructor(@Inject('PageService') protected override gbps: GBDataPageServiceWN<T>,private route: ActivatedRoute, private router: Router) {
    super(gbps);
  }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageStoreComponent<T> extends GBBasePageComponentNG {
  abstract override form: GBDataFormGroupStore<T>;
  override isDataPage = true;
  protected formSubscription: Subscription = new Subscription();
  constructor(@Inject('PageService') protected override gbps: GBDataPageService<T>) {
    super(gbps);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageStoreComponentWN<T> extends GBBaseDataPageStoreComponent<T> {
  abstract override  form: GBDataFormGroupStoreWN<T>;
  override hasNavigation = true;
  constructor(@Inject('PageService') protected override gbps: GBDataPageServiceWN<T>) {
    super(gbps);
  }
}
//List component base class's
@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListPageComponent {
  abstract title: string;
  abstract isDataPage: boolean;
  abstract hasNavigation: boolean;
  // abstract formurl:string;
  // abstract rowdata:[];
  // abstract colmdata:[];
  // abstract defaultcolm:[];

  constructor(@Inject('GBPageService') protected gbps: GBPageService) {
    this.thisConstructor();
  }
  abstract thisConstructor(): void;
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListPageComponentNG extends GBBaseListPageComponent {
  isDataPage = true;
  hasNavigation = false;
  constructor(@Inject('PageService') protected override gbps: GBDataPageServiceNG) {
    super(gbps);
  }
}

export abstract class GBBaseListDataPageStoreComponent<T> extends GBBaseListPageComponentNG {
  override isDataPage = true;
  protected formSubscription: Subscription = new Subscription();
  constructor(@Inject('PageService') protected override gbps: GBDataPageService<T>) {
    super(gbps);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}


@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListDataStoreComponentWN<T> extends GBBaseListDataPageStoreComponent<T> {
  override hasNavigation = false;
  constructor(@Inject('PageService') protected override gbps: GBDataPageServiceWN<T>) {
    super(gbps);
  }
}