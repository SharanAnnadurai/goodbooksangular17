import { Component, ElementRef, Self } from '@angular/core';
import { ControlContainer } from '@angular/forms';

import { GBFormGroup, GBFormSubGroup } from '../../classes/GBFormGroup';
import { IField } from '../../interfaces/ibasefield';
import { GBBaseUIComponent } from './GBBaseUIComponent';

@Component({
  template: ''
})
export abstract class GBBaseUIFGComponent extends GBBaseUIComponent {

  constructor(@Self() public ngControl?: ControlContainer, el?: ElementRef) {
    super();
  }

  get gbFormGroup(): GBFormSubGroup {
    return this.ngControl?.control as GBFormSubGroup;
  }

  get angControl(): ControlContainer {
    return this.ngControl as ControlContainer;
  }

  get gbControlName(): string {
    return 'addField';
  }

  get Field(): IField | null {
    if (!this.gbFormGroup) {
      return null;
    }
    return this.gbFormGroup?.field as IField;
  }

  get value(): any {
    return this.gbFormGroup?.value;
  }

  set value(val: any) {
    this.gbFormGroup?.patchValue(val);
  }
}
