import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
  HttpResponse
} from '@angular/common/http';
import { Inject, Injectable , isDevMode } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GbToasterService } from './../toaster/gbtoaster.service'
import { Translation } from '@ngneat/transloco';
// import {LayoutState} from './../../../../../../features/layout/store/layout.state'
// import { IReport } from './../../../../../../features/commonreport/model/report.model'
import * as utf8 from "utf8";
import { Select, Store } from '@ngxs/store';
// import { CriteriaConfigArray, Drilldownsetting, ListofViewtypes, Reportid, RowDataPassing, SelectedViewtype } from 'features/commonreport/datastore/commonreport.action';
import { DatePipe } from '@angular/common';
// import { InnerExceptionError } from './../../../../../../features/layout/store/layout.actions';
// import { InnerExceptionError } from 'features/layout/store/layout.actions';
// import {SharedService} from './../../../../../../features/commonreport/service/datapassing.service'
// import { ReportState } from 'features/commonreport/datastore/commonreport.state';

declare let require: any;
const globalbase64js = require("base-64");
const globalpako = require('pako');

@Injectable({
  providedIn: 'root'
})
export class GBHttpService {
  urlfromversionapi: any;
  // @Select(LayoutState.Versionservicurl) view$: Observable<any> | any;
  // @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any> | any;

  protected loginDTO: any;
  isprod = this.env.gbsettings.isprod
  proxvalue = this.env.gbsettings.apiprox
  constructor(private readonly http: HttpClient,  public toaster: GbToasterService, private store: Store, @Inject('env') private env:any) {

  }

  responsehandler(encodeddata: any) {
    const getresponse = JSON.stringify(encodeddata);
    const responseModel = JSON.parse(getresponse);
    let apiBody = responseModel.Body;
    const bytes = utf8.decode(apiBody);
    const decodedData = globalbase64js.decode(bytes);
    const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
    // this.store.dispatch(new InnerExceptionError(Responsebodydecoded.InnerException))
    const con = Responsebodydecoded.Body;
    let converter = Responsebodydecoded;
    try {
      converter = JSON.parse(con)
    } catch (error) {
      converter = con
    }
    return converter;
  }

  public versionhttpget(url: string): Observable<any> {//gbapihttpget
    let versiondata: any
    const jsonFile = `assets/config/GBconfig.json`;

    const versionprox = this.env.gbsettings.versionprox;

    console.log("isprod",versionprox)
    localStorage.setItem('urlformatted', versionprox);

    let targeturl
    if (isDevMode() == true) {
      let configuration: any = localStorage.getItem("VersionServiceDomain");
      targeturl = configuration + encodeURIComponent(url)
      console.log("KKKK",targeturl)
    }
    else {
      targeturl = versionprox + url
      console.log("XXXX",targeturl)
    }
    let headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
      }),
    };
    return this.http.get(targeturl).pipe(
      map((r) => {
        let responsetoJSON;
        if (isDevMode() == true) {
          const pd: any = r;
          responsetoJSON = JSON.stringify(pd.contents);
        }
        else {
          responsetoJSON = JSON.stringify(r);
        }
        const parsedbody = JSON.parse(responsetoJSON);
        const bodyparsed = JSON.parse(parsedbody.Body);
        // this.store.dispatch(new InnerExceptionError(parsedbody.InnerException))
        if (parsedbody.Status == 200 || parsedbody.Status == 0) {
          if (bodyparsed.length != 0) {
            versiondata = bodyparsed[0].ServiceBaseUri

            if (isDevMode() == false) {
              this.urlfromversionapi = versiondata
            }
            else {
              this.urlfromversionapi = ""
            }
            localStorage.setItem('urlformatted', this.urlfromversionapi);


          }
          const pd: any = r;
          return pd;
        }
        else {
          this.toaster.showerror(bodyparsed.Body, "ERROR");
        }

      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  httppost(url: string, data: any) {
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == false) {
      finalurl = encodeURIComponent(urlformatted + url)
    }
    else {
      finalurl = urlformatted + url
    }
    let testurl = this.proxvalue + finalurl//encodeuricomponent(this.urlfromversionapi + url)
    let headers = {
      headers: new HttpHeaders({

        'Login': this.getLoginDTO()
      }),
    };
    return this.http.post(testurl, data, headers).pipe(
      map((r) => {
        let responsetoJSONPostdata; 
        const pd: any = r;     
        if (isDevMode() == false) {
          const pd: any = r;
          if(url.includes('Common.svc')){
            localStorage.setItem('TotalRecord',pd.contents.Total)
            }
          responsetoJSONPostdata = pd.contents;
        }
        else {
          const pd: any = r;
          if(url.includes('Common.svc')){  
            localStorage.setItem('TotalRecord',pd.Total)
            }
          responsetoJSONPostdata = r;
        }
        if(JSON.stringify(responsetoJSONPostdata).includes('HTTP')){
          responsetoJSONPostdata = JSON.parse(responsetoJSONPostdata.substring(responsetoJSONPostdata.indexOf('Body')-2,responsetoJSONPostdata.length))
        }
        return this.responsehandler(responsetoJSONPostdata)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  public httpgetwithoutheader(url: string): Observable<any> {//gbapihttpget
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == true) {
      finalurl = encodeURIComponent(urlformatted + url)
      this.proxvalue = localStorage.getItem("ProxyLocation");
    }
    else {
      finalurl = url
    }
    let targeturl = this.proxvalue + finalurl
    console.log("Raviii",targeturl)
    //let targeturl = "prox" + url;  //  http://169.56.148.10:82/gb4/fws/Server.svc/AuthenticationKey

    return this.http.get(targeturl).pipe(
      map((r) => {
        const pd: any = r;
        //return pd;
        let rdata: any;
        if (isDevMode() == true) {
          rdata = pd.contents
        }
        else {
          rdata = pd;
        }
        console.log("rdata",rdata);
        return this.responsehandler(rdata)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  public httpget(url: string): Observable<any> {//gbapihttpget
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == true) {
      finalurl = encodeURIComponent(urlformatted + url)
    }
    else {
      finalurl = url
    }


    let targeturl = this.proxvalue + finalurl
    // let targeturl = "prox" + url;  //  http://169.56.148.10:82/gb4/fws/Server.svc/AuthenticationKey

    let headers = {
      headers: new HttpHeaders({

        'Login': this.getLoginDTO()
      }),
    };
    return this.http.get(targeturl, headers).pipe(
      map((r) => {
        const pd: any = r;
        let responsedata;
        if (isDevMode() == true) {
          let responsetoJSON = JSON.stringify(pd.contents);
          const parsedbody = JSON.parse(responsetoJSON);
          responsedata = parsedbody;
        }
        else {
          responsedata = r;
        }
        //return pd;
        return this.responsehandler(responsedata)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }
  httpgetasset(url: string): Observable<any> {
    return this.http.get(url).pipe(
      map((r) => {
        const pd: any = r;
        return pd;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  httpgettransloco(url: string): Observable<any> {
    return this.http.get<Translation>(url).pipe(
      map((r) => {
        const pd: any = r;
        return pd;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  httpgetparams(url: string, param: any): Observable<any> {

    let urlformatted = localStorage.getItem('urlformatted');
    let data = JSON.stringify(param)
    let urlwithparam = url + param.UserId + "&ModuleId=" + param.ModuleId
    let finalurl = urlformatted + urlwithparam;
    if (isDevMode() == false) {
      finalurl = encodeURIComponent(finalurl)
    }
    let targeturl = this.proxvalue + finalurl
    const headers = new HttpHeaders().set('Login', this.getLoginDTO());
    return this.http.get(targeturl, { headers: headers }).pipe(
      map((r) => {
        const pd: any = r;
        let responsedataparams;
        if (isDevMode() == false) {
          let responsetoJSONparms = JSON.stringify(pd.contents);
          const parsedbodyparms = JSON.parse(responsetoJSONparms);
          responsedataparams = parsedbodyparms;
        }
        else {
          responsedataparams = r;
        }
        //return pd;
        return this.responsehandler(responsedataparams)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }
  httppostwithheader(url: any, authdto: any, headers: any) {
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == false) {
      finalurl = encodeURIComponent(urlformatted + url)
    }
    else {
      finalurl = urlformatted + url
    }

    let testurl = this.proxvalue + finalurl
    /*let testurl = this.urltranformation(url)
    alert("testurl from gbhttp")
    alert(testurl)*/
    //let testurl = "prox" + url
    return this.http.post(
      testurl,
      authdto,
      headers
    ).pipe(
      map((r) => {
        const pd: any = r; //this.decodejson(r);
        //return pd;
        return this.responsehandler(r)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }

  /* httptest(url:string,body:any,headers:inamevalue[],params:httpparams)
   {
 
   }*/
  httppostwithauthheader(url: any, authdto: any, headers: any) {
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == true) {
      finalurl = encodeURIComponent(urlformatted + url)
    }
    else {
      finalurl = url
    }
    let testurl = this.proxvalue + finalurl
    /*let testurl = this.urltranformation(url)
    alert("testurl from gbhttp")
    alert(testurl)*/
    //let testurl = "prox" + url
    return this.http.post(
      testurl,
      authdto,
      headers
    ).pipe(
      map((r) => {
        const pd: any = r; //this.decodejson(r);
        //return pd;
        let responsedata;
        if (isDevMode() == true) {
          let authresponsetoJSON = JSON.stringify(pd.contents);
          const authparsedbody = JSON.parse(authresponsetoJSON);
          responsedata = authparsedbody;
        }
        else {
          responsedata = r;
        }
        return this.responsehandler(responsedata)
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err.message || 'Server Error');
      })
    );
  }
  httpdelete(url: string) {
    let urlformatted = localStorage.getItem('urlformatted');
    let finalurl
    if (isDevMode() == false) {
      finalurl = encodeURIComponent(urlformatted + url)
    }
    else {
      finalurl = urlformatted + url
    }
    let headers = {
      headers: new HttpHeaders({

        'Login': this.getLoginDTO()
      }),
    };
    let targeturl = this.proxvalue + finalurl
    return this.http.delete(targeturl, headers);
  }

  httpput(url: string, data: any) {
    return this.http.put(url, data);
  }
  public getLoginDTO(): string {
    let retvalue = '';
    this.store.selectSnapshot((state) => {
      if (state.AuthStore?.DLoginDTO != null)
        retvalue = JSON.stringify(state.AuthStore.DLoginDTO);
      if (state.AppStore?.DLoginDTO != null)
        retvalue = JSON.stringify(state.AppStore.DLoginDTO);

    });
    return retvalue;
  }

  Drilldownsetting(drillmenuid: any, selectedrowdataid: any ,fieldname:any): Observable<any> {
    let userid: any;
    return userid
    // this.store.selectSnapshot((state) => {
    //   userid = state.AuthStore.DLoginDTO.UserId 
    // });
    // const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=' + userid + '&MenuId=' + drillmenuid;
    // return this.httpget(url).pipe(
    //   map((menudetails) => {
    //     this.store.dispatch(new Reportid(drillmenuid))
    //     this.store.dispatch(new CriteriaConfigArray(menudetails))
    //     this.store.dispatch(new ListofViewtypes(menudetails[0].ReportVsViewsArray))
    //     for (var data of menudetails[0].ReportVsViewsArray){ 
    //       if(data.ReportViewId == menudetails[0].DefaultReportViewId){
    //         let selectedview =({Id: data.ReportViewId, View: data.ReportViewType , ViewName: data.ReportViewName , TemplateLocation: data.TemplateLocation, SecondTemplateLocation: data.SecondTemplateLocation})
    //         this.store.dispatch(new SelectedViewtype(selectedview)) 
    //       }
    //     }
    //     let destioncriteraiaform: any = []
    //     let WebServiceSettingArray = menudetails[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
    //     let CriteriaConfifgsettingArray = menudetails[0].CriteriaConfigArray[0].ConfigSection[0].ConfigAttribute
    //     for (let item of WebServiceSettingArray) {
    //       for (let critem of CriteriaConfifgsettingArray) {
    //         if (critem.CriteriaAttributeId == item.CriteriaAttributeId) {
    //           let criFieldValue = ""
    //           if (item.WebServiceSettingDetailCriteriaFieldValue == "NONE" && critem.CriteriaConfigAttributeFieldValue != "NONE") {
    //             criFieldValue = critem.CriteriaConfigAttributeFieldValue
    //           }
    //           else {
    //             if (critem.CriteriaAttributeName == "PeriodFrom" && item.WebServiceCriteriaFieldName == "PeriodFrom") {
    //               criFieldValue = critem.CriteriaConfigAttributeFieldValue
    //             }
    //             else {
    //               criFieldValue = item.WebServiceSettingDetailCriteriaFieldValue
    //             }
    //           }
    //           destioncriteraiaform.push(
    //             {
    //               FieldName: item.WebServiceCriteriaFieldName,
    //               OperationType: critem.CriteriaConfigAttributeOperationType,
    //               FieldValue: criFieldValue,
    //               JoinType: critem.CriteriaConfigAttributeJoin,
    //               CriteriaAttributeName: item.WebServiceSettingDetailDisplayName,
    //               CriteriaAttributeValue: critem.CriteriaConfigAttributeFieldDisplayValue,
    //               IsHeader: item.WebServiceSettingDetailIsHeader,
    //               IsCompulsory: item.WebServiceSettingDetailIsCompulsory,
    //               CriteriaAttributeId: item.CriteriaAttributeId,
    //               CriteriaAttributeType: item.CriteriaAttributeType,
    //               FilterType: "1"
    //             }
    //           )
    //         }
    //       }
    //     }
    //     let finalcriteriadto = {

    //       "SectionCriteriaList": [
    //         {
    //           "SectionId": 0,
    //           "AttributesCriteriaList": [] as any,
    //           "OperationType": 1
    //         }
    //       ],

    //     }
    //     this.sourcerowcriteria$.subscribe((criteria: { SectionCriteriaList: { AttributesCriteriaList: any; }[]; })=> {
    //       for (let srcdata of criteria.SectionCriteriaList[0].AttributesCriteriaList) {
    //         for (let dstdata of destioncriteraiaform) {
    //           if (srcdata.CriteriaAttributeId == dstdata.CriteriaAttributeId) {
    //             dstdata.FieldValue = srcdata.FieldValue
    //             finalcriteriadto.SectionCriteriaList[0].AttributesCriteriaList.push({
    //               FieldValue: dstdata.FieldValue,
    //               FieldName: dstdata.FieldName,
    //               JoinType: dstdata.JoinType,
    //               OperationType: dstdata.OperationType
    //             })
    //           }
    //         }
    //       }

    //       var uniqueResultdata = criteria.SectionCriteriaList[0]
    //       .AttributesCriteriaList.filter((item1: { CriteriaAttributeId: any; }) =>
    //         !destioncriteraiaform.some((item2: { CriteriaAttributeId: any; }) => (item2.CriteriaAttributeId === item1.CriteriaAttributeId)))

    //     for (let data of uniqueResultdata) {
    //       finalcriteriadto.SectionCriteriaList[0].AttributesCriteriaList.push({
    //         FieldValue: data.FieldValue,
    //         FieldName: data.FieldName,
    //         JoinType: data.JoinType,
    //         OperationType: data.OperationType
    //       })
    //     }
    //     })

    //     finalcriteriadto.SectionCriteriaList[0].AttributesCriteriaList.push({
    //       FieldValue: selectedrowdataid,
    //       FieldName:fieldname,
    //       JoinType: "2",
    //       OperationType: 5
    //     })
    //     let criteria = {
    //       "ReportUri": menudetails[0].WebServiceUriTemplate,
    //       "Method": menudetails[0].MethodType,
    //       "ReportTitle": "",
    //       "CriteriaDTO": finalcriteriadto,
    //     }
    //     let fullurl = '/cos/Common.svc/Report';
    //     this.httppost(fullurl, criteria).subscribe(res => {
    //       this.shareddataservice.setRowInfo(res.ReportDetail);
    //       this.store.dispatch(new Drilldownsetting("RRRRRRRRRRRRRRRRRRRRRRRRRRRRAAAAAAAAAAAAAAAAAAAVVVVVVVVV"));
    //     });
    //   })
    // );
    
  }


}

