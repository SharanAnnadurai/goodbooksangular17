import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RetryRequestService } from './RetryreqInterceptor.service';
import { NotifyErrorService } from './NotifyerrorInterceptor.service';
import { APIinterceptor } from './APIInterceptor.service';
import { SpinnerHttpInterceptor } from './SpinnerInterceptor.service';
import { NotificationService} from './notificationinterceptor.service';

export const interceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: SpinnerHttpInterceptor, multi: true },
  //{ provide: HTTP_INTERCEPTORS, useClass: NotificationService, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: NotifyErrorService, multi: true },
//  { provide: HTTP_INTERCEPTORS, useClass: APIinterceptor, multi: true },
];
