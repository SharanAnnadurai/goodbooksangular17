import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class DashboardFunction {
    constructor() { }
    Random(number:number){
        let randomarray =[]
        for(let i=0;i<number;i++){
            randomarray[i] = Math.floor(Math.random() * 10) + 1;
        }
        return randomarray;
    }
}