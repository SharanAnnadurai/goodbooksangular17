import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

export { RuleEngine } from './core/rule-engine';
export { EnumComparers, EnumOperators, EnumValueType } from './interfaces/RulesModel';
export { IActionCheckCondition, IActionSetValue, IActionSetError, IActionSetLock, IActionSetRequired, IActionSetVisible } from './interfaces/RulesModel';
export { IAction, ICondition, IFormula, IRule } from './interfaces/RulesModel';

@NgModule({
  imports: [CommonModule],
  exports: []
})
export class NgBreModule {}
