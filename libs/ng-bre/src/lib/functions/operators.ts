import { IRuleContext } from "../interfaces/RulesModel";

export function add(context: IRuleContext, leftValue: unknown, rightValue: unknown): unknown {
  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 + val2;
  }
  if (typeof leftValue === 'bigint' && typeof rightValue === 'bigint') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 + val2;
  }
  if (typeof leftValue === 'string' && typeof rightValue === 'string') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 + val2;
  }
  if (leftValue instanceof Date && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return new Date(val1.getTime() + val2);
  }
  if (typeof leftValue === 'string' && Date.parse(leftValue) && typeof rightValue === 'number') {
    const val1: Date = new Date(leftValue);
    const val2 = rightValue;
    return new Date(val1.getTime() + val2);
  }
  throw "Operator add not supported for this operation!";
}
export function subtract(context: IRuleContext, leftValue: unknown, rightValue: unknown): unknown {
  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 - val2;
  }
  if (typeof leftValue === 'bigint' && typeof rightValue === 'bigint') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 - val2;
  }
  if (leftValue instanceof Date && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return new Date(val1.getTime() - val2);
  }
  throw "Operator subtract not supported for this operation!";
}
export function multiply(context: IRuleContext, leftValue: unknown, rightValue: unknown): unknown {
  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 * val2;
  }
  if (typeof leftValue === 'bigint' && typeof rightValue === 'bigint') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 * val2;
  }
  throw "Operator multiply not supported for this operation!";
}
export function divide(context: IRuleContext, leftValue: unknown, rightValue: unknown): unknown {
  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 / val2;
  }
  if (typeof leftValue === 'bigint' && typeof rightValue === 'bigint') {
    const val1 = leftValue;
    const val2 = rightValue;
    return val1 / val2;
  }
  throw "Operator divide not supported for this operation!";
}
export function diffDays(context: IRuleContext, leftValue: unknown, rightValue: unknown) {
  let lValue: Date | null = null;
  let rValue: Date | null = null;

  if(typeof leftValue === "string") lValue = new Date(Date.parse(leftValue));
  if(typeof leftValue === "number") lValue = new Date(leftValue);
  if(leftValue instanceof Date) lValue = leftValue;

  if(typeof rightValue === "string") rValue = new Date(Date.parse(rightValue));
  if(typeof rightValue === "number") rValue = new Date(rightValue);
  if(rightValue instanceof Date) rValue = rightValue;

  if (lValue && rValue) {
    const diff = Math.abs(lValue.getTime() - rValue.getTime());
    const diffInDays = Math.ceil(diff / (1000 * 3600 * 24));
    return diffInDays;
  }
  throw "Operator diffDays not supported for this operation!";
}
export function diffYears(context: IRuleContext, leftValue: unknown, rightValue: unknown) {
  return Math.round(diffDays(context, leftValue, rightValue) / 365);
}
