import { HttpClient } from '@angular/common/http';
import { Inject, isDevMode } from '@angular/core';

import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ISelectListCriteria } from '../interfaces/ISectionCriteriaList';
import { GBBaseDBDataService } from './gbbasedbdata.service';
import { EnumIntervalType, GBLocalStorageService, GBSessionStorageService } from './storage.service';

export abstract class AbstractDS { }
export class GBBaseService implements AbstractDS {}

export class GBBaseDataService<T> implements GBBaseService {
  protected currentData!: T;

  constructor(@Inject('DBDataService') protected dbDataService: GBBaseDBDataService<T>, public IdField: string) {
  }

  saveData(data: T): Observable<any> {
    return this.dbDataService.postData(data);
  }

  getData(id: string): Observable<T> {
    return this.dbDataService.getData(this.IdField, id);
  }
  deleteData(id: any): Observable<any> {
    return this.dbDataService.deleteData(this.IdField, id);
  }
}

export class GBBaseDataServiceWN<T> extends GBBaseDataService<T> {
  // pickListId: string;
  protected scl!: ISelectListCriteria;
  protected plst: any;
  protected dataList: any[] = [];
  curPos = 0;

  protected ls: GBLocalStorageService = new GBLocalStorageService();
  protected ss: GBSessionStorageService = new GBSessionStorageService();

  constructor(
    @Inject('DBDataService') public override dbDataService: GBBaseDBDataService<T>,
    public override IdField: string,
    private getAllCacheName: string = '',
    private bStoreInLocal: boolean = false,
    private expires: number | Date = 10,
    private numberIn: EnumIntervalType = EnumIntervalType.Seconds
  ) {
    super(dbDataService, IdField);

    if (this.getAllCacheName === '') {
      this.getAllCacheName = IdField.replace('Id', '');
      this.expires = 10;
    }
  }

  getAll(): Observable<any> {
    return this.dbDataService.getList(this.scl).pipe(map(r => {
      this.dataList = r as any[];
      return this.dataList
    }));
  }
  override deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  override saveData(data: T): Observable<any> {
    return super.saveData(data);
  }

  moveFirst(): Observable<T> {
    if (this.dataList === undefined) {
      this.getAll().subscribe();
    }
    this.curPos = 0;
    return this.getData(this.dataList[this.curPos].Id);
  }

  movePrev(): Observable<T> {
    if (this.dataList === null) {
      this.getAll().subscribe();
    }
    this.curPos = this.curPos - 1;
    if (this.curPos < 0) {
      this.curPos = 0;
    }
    return this.getData(this.dataList[this.curPos].Id);
  }

  moveNext(): Observable<T> {
    if (this.dataList === null) {
      this.getAll().subscribe();
    }
    this.curPos = this.curPos + 1;
    if (this.curPos >= this.dataList.length) {
      this.curPos = this.dataList.length - 1;
    }
    return this.getData(this.dataList[this.curPos].Id);
  }

  moveLast(): Observable<T> {
    if (this.dataList === null) {
      this.getAll().subscribe();
    }
    this.curPos = this.dataList.length - 1;
    return this.getData(this.dataList[this.curPos].Id);
  }
}

export class GBBaseListDataService<T> extends GBBaseDataService<T> {
  protected scl!: ISelectListCriteria;
  protected plst: any;
  protected dataList!: any[];
  constructor(
    @Inject('DBDataService') public override dbDataService: GBBaseDBDataService<T>,
    public override IdField: string,
  ) {
    super(dbDataService, IdField);
  }
  getAll(): Observable<any> {
    return this.dbDataService.getList(this.scl).pipe(map(r => {
      this.dataList = r as any[];
      return this.dataList
    }));
  }
}