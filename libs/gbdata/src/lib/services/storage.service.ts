
export enum EnumIntervalType {
  Seconds = 0,
  Minutes = 1,
  Hours = 2,
  Days = 3,
  Weeks = 4,
  // Months = 5,
  // Years = 6
}
abstract class AbstractWebStorageService {
  /*  removeStorage: removes a key from localStorage and its sibling expiracy key
      params:
          key <string>     : localStorage key to remove
      returns:
          <boolean> : telling if operation succeeded
   */
  abstract removeItem(key: string): void;
  /*  setStorage: writes a key into localStorage setting a expire time
    params:
        key <string>     : localStorage key
        value <string>   : localStorage value
        expires <number> : number of seconds from now to expire the key
    returns:
        <boolean> : telling if operation succeeded
  */
  abstract setItemInternal(key: string, value: string, expires: number): void;
  public setItem(key: string, value: string, expires: number | Date, numberIn: EnumIntervalType = EnumIntervalType.Seconds) {
    let expsecs = 0;
    if (expires instanceof Date) {
      const eo = Math.floor(expires.getTime() / 1000);
      const now = Date.now();
      expsecs = eo - now;
    }
    else {
      switch (numberIn) {
        case EnumIntervalType.Seconds: {
          expsecs = expires;
          break;
        }
        case EnumIntervalType.Minutes: {
          expsecs = expires * 60;
          break;
        }
        case EnumIntervalType.Hours: {
          expsecs = expires * 60 * 60;
          break;
        }
        case EnumIntervalType.Days: {
          expsecs = expires * 60 * 60 * 24;
          break;
        }
        case EnumIntervalType.Weeks: {
          expsecs = expires * 60 * 60 * 24 * 7;
          break;
        }
        // case EnumIntervalType.Months: {
        //   break;
        // }
        // case EnumIntervalType.Years: {
        //   break;
        // }
        default: {
          break;
        }
      }
      this.setItemInternal(key, value, expsecs);
    }
  }
  /*  getStorage: retrieves a key from localStorage previously set with setStorage().
    params:
        key <string> : localStorage key
    returns:
        <string> : value of localStorage key
        null : in case of expired key or failure
  */
  abstract getItem(key: string): any;
}
export class GBLocalStorageService extends AbstractWebStorageService {
  removeItem(key: string) {
    try {
      localStorage.removeItem(key);
      localStorage.removeItem(key + '_expiresIn');
    } catch (e) {
      return false;
    }
    return true;
  }
  getItem(key: string) {

    const now = Date.now();  // epoch time, lets deal only with integer
    // set expiration for storage
    let expiresIn = Date.parse(localStorage.getItem(key + '_expiresIn') || '');
    if (expiresIn === undefined || expiresIn === null) { expiresIn = 0; }

    if (expiresIn < now) { // Expired
      this.removeItem(key);
      return null;
    } else {
      try {
        const value = localStorage.getItem(key);
        return value;
      } catch (e) {
        return null;
      }
    }
  }
  setItemInternal(key: string, value: string, expires: number) {

    if (expires === undefined || expires === null) {
      expires = (24 * 60 * 60);  // default: seconds for 1 day
    } else {
      expires = Math.abs(expires); // make sure it's positive
    }

    const now = Date.now();  // millisecs since epoch time, lets deal only with integer
    const schedule = now + expires * 1000;
    try {
      localStorage.setItem(key, value);
      localStorage.setItem(key + '_expiresIn', schedule.toString());
    } catch (e) {
      return false;
    }
    return true;
  }
}
export class GBSessionStorageService extends AbstractWebStorageService {
  removeItem(key: string) {
    try {
      sessionStorage.removeItem(key);
      sessionStorage.removeItem(key + '_expiresIn');
    } catch (e) {
      return false;
    }
    return true;
  }
  getItem(key: string) {

    const now = Date.now();  // epoch time, lets deal only with integer
    // set expiration for storage
    // let ei: Date = sessionStorage.getItem(key + '_expiresIn');
    let expiresIn = parseInt(sessionStorage.getItem(key + '_expiresIn') || '', 10);
    if (expiresIn === undefined || expiresIn === null) { expiresIn = 0; }

    if (expiresIn < now) { // Expired
      this.removeItem(key);
      return null;
    } else {
      try {
        const value = sessionStorage.getItem(key);
        return value;
      } catch (e) {
        return null;
      }
    }
  }
  setItemInternal(key: string, value :any, expires :any) {

    if (expires === undefined || expires === null) {
      expires = (24 * 60 * 60);  // default: seconds for 1 day
    } else {
      expires = Math.abs(expires); // make sure it's positive
    }

    const now = Date.now();  // millisecs since epoch time, lets deal only with integer
    const schedule = now + expires * 1000;
    try {
      sessionStorage.setItem(key, value);
      sessionStorage.setItem(key + '_expiresIn', schedule.toString());
    } catch (e) {
      return false;
    }
    return true;
  }
}
