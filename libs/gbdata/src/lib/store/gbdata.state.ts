import { Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// import { LibInjector, Notify } from '@goodbooks/gbcommon';
import {LibInjector} from './../../../../gbcommon/src/lib/lib-injector';
import {Notify} from './../../../../gbcommon/src/lib/store/event.actions';
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GBBaseDataService, GBBaseDataServiceWN } from '../services/gbbasedata.service';

export abstract class FormStateAbstract {
  abstract model: unknown;
  abstract dirty: boolean;
  abstract status: string;
  abstract errors: string | null;
}
export abstract class FormState<T> extends FormStateAbstract {
  abstract override model: T | undefined;
  abstract override dirty: boolean;
  abstract override status: string;
  abstract override errors: string | null;
}
export abstract class GBDataStateModelAbstract {
  abstract data: unknown[];
  abstract areDataLoaded: boolean;
  abstract currentData: FormStateAbstract;
  abstract currentIndex: number;
}
export class GBDataStateModel<T> extends GBDataStateModelAbstract
{
    areDataLoaded = false;
    currentIndex = 0;
    data: T[] = [];
    currentData: FormState<T> = {
      model: undefined,
      dirty: false,
      status: '',
      errors: ''
    };
}

export abstract class GBDataState<T> {

  constructor(@Inject('DataService') protected dataService: GBBaseDataServiceWN<T>, protected router?: Router, protected store?: Store) {
    const inj = LibInjector;
    if (inj) {
      if (router === null || router === undefined) {
        router = inj.get(Router);
      }
      if (store === null || store === undefined) {
        store = inj.get(Store);
      }
    }
    this.thisConstructor();
  }
  // ngOnDestroy(): void {
  // }

  protected thisConstructor(): void {}

  abstract GetData(context: StateContext<GBDataStateModel<T>>, { id }: { id: any }): Observable<T>;
  abstract AddData(context: StateContext<GBDataStateModel<T>>, {payload}: { payload: any }): Observable<T>;
  abstract SaveData(context: StateContext<GBDataStateModel<T>>, {payload}: { payload: any }): Observable<T>;
  abstract ClearData(context: StateContext<GBDataStateModel<T>>): Observable<T>;
  abstract DeleteData(context: StateContext<GBDataStateModel<T>>): Observable<T>;

  abstract GetAll(context: StateContext<GBDataStateModel<T>>, {scl}: { scl: any }): Observable<T>;
  abstract MoveFirst(context: StateContext<GBDataStateModel<T>>): Observable<T>;
  abstract MovePrev(context: StateContext<GBDataStateModel<T>>): Observable<T>;
  abstract MoveNext(context: StateContext<GBDataStateModel<T>>): Observable<T>;
  abstract MoveLast(context: StateContext<GBDataStateModel<T>>): Observable<T>;

  protected Super_GetData(context: StateContext<GBDataStateModel<T>>,
    id: string
  ): Observable<T> {
    return this.dataService.getData(id.toString()).pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("get data", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_AddData(context: StateContext<GBDataStateModel<T>>,
    { payload }: { payload: any }
  ): Observable<any> {
    return this.dataService.saveData(payload).pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("add data", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_SaveData(context: StateContext<GBDataStateModel<T>>,
    { payload, id }: { payload: any; id: any }
  ): Observable<any>{
    return this.dataService.saveData(payload).pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("save data", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_ClearData(context: StateContext<GBDataStateModel<T>>): void {
    const state = context.getState();
    context.setState({
      ...state,
      currentData: { model: {} as T, dirty: false, errors: null, status: ''}
    });
    context.dispatch(new Notify("clear data"));
  }

  protected Super_DeleteData(context: StateContext<GBDataStateModel<T>>): Observable<any> {
    const state = context.getState();
    const idfld = this.dataService.IdField;
    const currentData = state.currentData?.model as T;
    const id = (currentData as any)[idfld];
    return this.dataService.deleteData(id).pipe(
      tap(result => {
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: '', status: ''}
        });
        context.dispatch(new Notify("delete data", (result as any)[this.dataService.IdField]));
      })
    );
  }




  protected Super_GetAll(context: StateContext<GBDataStateModel<T>>, scl: {scl:any}): Observable<any> {
    return this.dataService.getAll().pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          data: result,
          areDataLoaded: true
        });
        context.dispatch(new Notify("get all", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_MoveFirst(context: StateContext<GBDataStateModel<T>>): Observable<T> {
    return this.dataService.moveFirst().pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("Move first", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_MovePrev(context: StateContext<GBDataStateModel<T>>): Observable<T> {
    return this.dataService.movePrev().pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("Move prev", (result as any)[this.dataService.IdField]));
      })
    );
  }

  public Super_MoveNext(context: StateContext<GBDataStateModel<T>>): Observable<T> {
    return this.dataService.moveNext().pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("Move next", (result as any)[this.dataService.IdField]));
      })
    );
  }

  protected Super_MoveLast(context: StateContext<GBDataStateModel<T>>): Observable<T> {
    return this.dataService.moveLast().pipe(
      tap(result => {
        const state = context.getState();
        context.setState({
          ...state,
          currentData: { model: result, dirty: false, errors: null, status: ''}
        });
        context.dispatch(new Notify("Move last", (result as any)[this.dataService.IdField]));
      })
    );
  }

}
