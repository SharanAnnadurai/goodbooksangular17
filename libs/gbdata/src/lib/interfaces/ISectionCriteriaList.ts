export interface ISelectListCriteria {
  SectionCriteriaList: ISectionCriteriaList[];
}

export interface ISectionCriteriaList {
  SectionId: number;
  AttributesCriteriaList: IAttributesCriteriaList[];
  OperationType: number;
}

export interface IAttributesCriteriaList {
  FieldName: string;
  OperationType: number;
  FieldValue: unknown;
  InArray: boolean;
  JoinType: number;
}
