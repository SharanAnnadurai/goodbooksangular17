// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseurl: "",
  gbsettings: {
    apiprox: "prox",
    versionprox: "versionapi",
    isprod:false ,
    versionurl:"http://183.82.250.223:98/vs",
    apiurl:"http://183.82.250.223:98",
    devproxy:"devproxy"
    // apiurl:"",
    // proxylocation: "versionapi/",//"http://169.56.148.10:90/gb4version/proxy.php?url="
  
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
