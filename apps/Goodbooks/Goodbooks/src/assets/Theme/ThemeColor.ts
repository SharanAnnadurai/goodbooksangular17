export class ThemeColor{
  "Default": "#6E3AB6";
  "Blue": "#2D6ADF";
  "Pink": "#fd92a4";
  "Red": "#f11d16";
  "Aqua": "#16f1df";
  "Yellow": "#ecf00c";
  "Teal": "#016f6f";
  "Green": "#008000"
}