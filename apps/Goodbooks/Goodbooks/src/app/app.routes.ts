import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './AuthGuard/authguard.service';
import { LoginComponent } from './../../../../../features/common/shared/screens/Login/login.component';



const routes: Routes = [
  { path: 'Login', component: LoginComponent },
  { path: 'main', redirectTo: 'GoodBooks', pathMatch: 'full' },
  // { path: '', canActivate: [AuthGuard], redirectTo: 'main', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
