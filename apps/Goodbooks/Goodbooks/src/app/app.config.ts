import { HttpClientModule } from '@angular/common/http';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app.routes';
import { ServiceWorkerModule } from '@angular/service-worker';

import { GbcommonModule } from './../../../../../libs/gbcommon/src/lib/gbcommon.module';
import { UicoreModule } from './../../../../../libs/uicore/src/lib/uicore.module';
import { UifwkUifwkmaterialModule } from './../../../../../libs/uifwk/uifwkmaterial/src/lib/uifwk-uifwkmaterial.module';
import { GbuiModule } from './../../../../../libs/gbui/src/lib/gbui.module';

import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppState } from './Datastore/app.state';
import { GbSharedModule } from './sharedscreens/Shared/gbshared.module';
import { TranslocoRootModule } from './transloco/transloco-root.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { FeatureCommonmodule} from './../../../../../features/common/featurecommon.module';
import { GBLayoutModule } from './../../../../../features/layout/gblayout.module';

import {GBConfigService } from './../../../../../libs/gbcommon/src/lib/services/config/gbconfig.service';

export function initializeApp(appConfig: GBConfigService) {
  return () => appConfig.load();
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    UifwkUifwkmaterialModule,
    UicoreModule,
    ReactiveFormsModule,
    GbcommonModule.forRoot(environment),
    GbuiModule,
    GbSharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TranslocoRootModule,
    NgxsModule.forRoot([AppState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot( {disabled: environment.production }),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({storage: StorageOption.SessionStorage}),
    NgxSpinnerModule,
    FeatureCommonmodule,
    GBLayoutModule,
  ],
  providers: [
    GBConfigService,
  { provide: APP_INITIALIZER,
      useFactory: initializeApp,
   deps: [GBConfigService], multi: true }],
  bootstrap: [AppComponent],
})

export class AppModule { }
