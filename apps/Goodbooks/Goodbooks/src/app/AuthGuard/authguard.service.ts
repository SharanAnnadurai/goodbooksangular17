import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import {Router} from '@angular/router';
import { Store, Select } from '@ngxs/store';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private myRoute: Router, private store: Store){
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUserSubject = this.getLoginDTO();
    if (currentUserSubject) {
        return true;
    }
    this.myRoute.navigate(['/Login'], { queryParams: { returnUrl: state.url } });
    return false;
}
public getLoginDTO(): string {
  let retvalue :any =null;
  let authdata= '';
  this.store.selectSnapshot((state) => {
    if (state.AuthStore.DLoginDTO != null)
      retvalue = state.AuthStore.DLoginDTO;
      authdata = retvalue.UserCode;
  });
  return authdata;
}
}
