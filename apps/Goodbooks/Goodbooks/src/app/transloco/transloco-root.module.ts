import { HttpClient } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';

import { translocoConfig, TranslocoLoader, TranslocoModule, TRANSLOCO_CONFIG, TRANSLOCO_LOADER} from '@ngneat/transloco';
import { environment } from '../../environments/environment';
 import {GBHttpService} from "./../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service"
@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
  constructor(private http: HttpClient,private GBHttpService:GBHttpService) {
  }

  getTranslation(lang: string) {
    return this.GBHttpService.httpgettransloco( `assets/i18n/${lang}.json`);
   
  }
}

@NgModule({
  exports: [ TranslocoModule ],
  providers: [
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        availableLangs: ['English', 'French', 'Hindi','Tamil'],
        defaultLang: 'English',
        reRenderOnLangChange: true,
        fallbackLang: 'English',
        prodMode: environment.production,
      })
    },
    { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader }
  ]
})
export class TranslocoRootModule {}
